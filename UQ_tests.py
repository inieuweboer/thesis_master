import matplotlib.pyplot as plt
import numpy as np

from scipy import stats
from scipy.integrate import quad

from helper_functions import (
    colors,
    make_affine_transf,
    )
from UQ_bm_drift_rm import exact_prob_bm_rm
from UQ_methods import *

from tabulate import tabulate
from text_render_latex import *
latexify(columns=2)


def runge(x):
    return 1 / (1 + 25*x**2)


def gpc_plot_precision(
        model_fn=runge,
        T=30, L=26, sp_mult=20, printerrors=False
        ):
    # Watch out with indexing against L!
    sp_mult_L = 4
    Lspace = np.linspace(0, L, int(np.ceil(L*sp_mult_L + 1)))

    Tspace = np.linspace(0, T)
    Targ = -1

    a, b = -1, 1
    assert a < b, 'a not smaller than b'

    #  KM_arr = [
    #            (5, 5),
    #            (5, 10),
    #            (10, 5),
    #            (10, 10),
    #            (10, 20),
    #            (50, 20),
    #            (50, 50),
    #            (50, 100),
    #           ]
    KK = np.array([5, 10, 20, 40, 80])
    MM = KK + 1

    table_arr = []

    true_mean = quad(model_fn, a, b)[0] / (b - a)
    true_var = quad(lambda z: (model_fn(z) - true_mean)**2, a, b)[0] / (b - a)

    table_arr.append(['True (quadrature)', '', '', '', str(true_mean)])

    nsamples = np.power(10, 5)

    cmc_samples = model_fn(np.random.uniform(a, b, size=nsamples))
    cmc_mean = np.mean(cmc_samples)

    bias = np.abs(cmc_mean - true_mean)
    rel_bias = bias / true_mean
    table_arr.append(['CMC {}'.format(nsamples), '', '',
                      '{:.4g}'.format(cmc_mean),
                      '{:.4g}'.format(bias),
                      '{:.4g}'.format(rel_bias),
                      ])

    gpc_interp_means = []
    gpc_dp_means = []

    gpc_interp_vars = []
    gpc_dp_vars = []

    for K in KK:
        _, fcs_interp, norms_sq = gpc_approx_interp(
            model_fn, a, b,
            gpc_deg=K
            )
        gpc_interp_mean = fcs_interp[0]
        gpc_interp_var = np.nansum(fcs_interp[1:]**2 * norms_sq[1:])
        gpc_interp_means.append(gpc_interp_mean)
        gpc_interp_vars.append(gpc_interp_var)

        #  bias = np.abs(gpc_interp_mean - true_mean)
        #  rel_bias = bias / true_mean
        #  table_arr.append(['gPC interp.',
        #                    str(K),
        #                    '',
        #                    '{:.4g}'.format(gpc_interp_mean),
        #                    '{:.4g}'.format(bias),
        #                    '{:.4g}'.format(rel_bias),
        #                    ])

    for K, M in zip(KK, MM):
        _, fcs_dp, norms_sq = gpc_approx_discrete_proj_uniform(
            model_fn, a, b,
            gauss_quad_deg=M,
            gpc_deg=K
            )

        gpc_dp_mean = fcs_dp[0]
        gpc_dp_var = np.nansum(fcs_dp[1:]**2 * norms_sq[1:])
        gpc_dp_means.append(gpc_dp_mean)
        gpc_dp_vars.append(gpc_dp_var)

        bias = np.abs(gpc_dp_mean - true_mean)
        rel_bias = bias / true_mean
        table_arr.append(['gPC disc. proj.',
                          str(K),
                          str(M),
                          '{:.4g}'.format(gpc_dp_mean),
                          '{:.4g}'.format(bias),
                          '{:.4g}'.format(rel_bias),
                          ])

    first_row = [['Method',
                  'K',
                  'M',
                  'Est',
                  'Bias',
                  'Relative bias',
                  ]]
    print(tabulate(first_row, tablefmt="latex_raw"), file=None)
    print(tabulate(table_arr, tablefmt="latex_raw", floatfmt=".4g"), file=None)

    gpc_interp_means = np.array(gpc_interp_means)
    gpc_dp_means = np.array(gpc_dp_means)
    gpc_interp_vars = np.array(gpc_interp_vars)
    gpc_dp_vars = np.array(gpc_dp_vars)

    fig, axes = plt.subplots(nrows=1, ncols=2)
    fig.set_figheight(fig.get_figheight() / 2)

    dp_label = "Disc. proj."
    interp_label = "Interp."
    xlabel = "K"
    rv = 'f(U)'

    #  axes[0, 0].plot(KK, np.abs(gpc_dp_means - true_mean),
    #                  'bs--', lw=0.5,
    #                  markersize=4,
    #                  label=dp_label)
    #  axes[0, 0].plot(KK, np.abs(gpc_interp_means - true_mean),
    #                  'r.--', lw=0.5, label=interp_label)
    #  axes[0, 0].set_yscale('log')
    #  set_axis_labels_endaxes(xlabel, 'Bias est. of ${}$'.format(xe(rv)), axes[0, 0])

    #  axes[0, 1].plot(KK, np.abs(gpc_dp_vars - true_var),
    #                  'bs--', lw=0.5,
    #                  markersize=4,
    #                  label=dp_label)
    #  axes[0, 1].plot(KK, np.abs(gpc_interp_vars - true_var),
    #                  'r.--', lw=0.5, label=interp_label)
    #  axes[0, 1].set_yscale('log')
    #  set_axis_labels_endaxes(xlabel, 'Bias est. of ${}$'.format(xv(rv)), axes[0, 1])

    axes[0].plot(KK, np.abs((gpc_dp_means - true_mean) / true_mean),
                 'bs--', lw=0.5,
                 markersize=4,
                 label=dp_label)
    axes[0].plot(KK, np.abs((gpc_interp_means - true_mean) / true_mean),
                 'r.--', lw=0.5, label=interp_label)
    axes[0].set_yscale('log')
    set_axis_labels_endaxes(xlabel, 'Rel. bias est. of ${}$'.format(xe(rv)), axes[0])

    axes[1].plot(KK, np.abs((gpc_dp_vars - true_var) / true_var),
                 'bs--', lw=0.5,
                 markersize=4,
                 label=dp_label)
    axes[1].plot(KK, np.abs((gpc_interp_vars - true_var) / true_var),
                 'r.--', lw=0.5, label=interp_label)
    axes[1].set_yscale('log')
    axes[1].legend()
    set_axis_labels_endaxes(xlabel, 'Rel. bias est. of ${}$'.format(xv(rv)), axes[1])

    fig.savefig(
        './figs/gpc_compare_K_{}-{}'
        '.pdf'.format(
            KK[0], KK[-1],
            ),
        format='pdf',
        )


def gpc_approx_error(
        model_fn=runge,
        a=-1, b=1,
        #  a=-3, b=3,
        KK=None, MM=None,
        sp_mult=100,
        ):

    fig, ax = plt.subplots()
    fig.set_figheight(fig.get_figheight() * figfactor)
    fig.set_figwidth(fig.get_figwidth() * figfactor)

    x = np.linspace(a, b, int(np.ceil(b - a))*sp_mult + 1)
    true = model_fn(x)
    ax.plot(x, true, 'k--', label='True')

    #  KM_arr = [
                 #  (5, 5),
                 #  (10, 5),
                 #  (10, 10),
                 #  (20, 10),
                 #  (50, 50),
                 #  (100, 50),
    #           ]
    if KK is None:
         KK = np.array([5, 10, 20, 40, 80])

    if MM is None:
        MM = KK + 1

    #  for K, M in KM_arr::
    for K, M in zip(KK, MM):
        gpc_fn_disc_proj, _, _ = gpc_approx_discrete_proj_uniform(
            model_fn,
            a=a, b=b,
            gpc_deg=K,
            gauss_quad_deg=M,
            )

        gpc_fn_interp, _, _ = gpc_approx_interp(
            model_fn,
            a=a, b=b,
            gpc_deg=K,
            nnodes=M,
            )

        ax.plot(x, gpc_fn_disc_proj(x), lw=.65,
                 #  c=np.random.choice(colors),
                 #  label='Disc. proj. K = {}, M = {}'.format(K, M),
                 label='Disc. proj. K = {}'.format(K),
                 )
        #  ax.plot(x, gpc_fn_interp(x), lw=.4,
        #           #  c=np.random.choice(colors),
        #           #  label='Interp. K = {}, M = {}'.format(K, M),
        #           label='Interp. K = {}'.format(K),
        #           )

        #  axes[1].plot(x, true - gpc_fn_disc_proj(x), lw=.65,
        #           #  c=np.random.choice(colors),
        #           #  label='Disc. proj. K = {}, M = {}'.format(K, M),
        #           label='Disc. proj. K = {}'.format(K),
        #           ls='-',
        #           )
        #  axes[1].plot(x, true - gpc_fn_interp(x), lw=.4,
        #           #  c=np.random.choice(colors),
        #           #  label='Interp. K = {}, M = {}'.format(K, M),
        #           label='Interp. K = {}'.format(K),
        #           ls='-',
        #           )

    figname = 'runge_approx_error'

    ax.legend()
    fig.savefig(
        './figs/fit_gpc_{}_'
        'a_{}_b_{}.pdf'.format(
            figname,
            a, b),
        format='pdf',
        )


def test_gpc_mean_std(T=30, L=26, sp_mult=20, printerrors=False):
    gauss_quad_deg = 10  # M
    gpc_deg = 5  # K

    # Watch out with indexing against L!
    sp_mult_L = 4
    Lspace = np.linspace(0, L, int(np.ceil(L*sp_mult_L + 1)))

    Tspace = np.linspace(0, T)
    Targ = -1

    #  a = -2; b = 0
    #  a = 0; b = 2

    #  a = -3; b = -1
    #  a = 1; b = 3

    #  a = -0.01; b = -a
    #  a = -0.1; b = -a

    #  a = -10; b = -a
    a = -1
    b = -a
    assert a < b, 'a not smaller than b'

    model_fn_param_arr = [
                         lambda L, T, alpha:   alpha**2 + L*T*1e-16,
                         lambda L, T, alpha: 2*alpha**2 + L*T*1e-16,
                         lambda L, T, alpha: 3*alpha**2 + L*T*1e-16,
                         #  lambda L, T, alpha: alpha    + L*T*1e-16,
                         #  lambda L, T, alpha: alpha**2 + L*T*1e-16,
                         #  lambda L, T, alpha: alpha**3 + L*T*1e-16,
                         #  lambda L, T, alpha: alpha**4 + L*T*1e-16,
                        ]

    for model_fn_param in model_fn_param_arr:
        def model_fn(alpha): return model_fn_param(L, T, alpha)

        gpc_approx_interp_fn_L, fcs_interp_L, _ = gpc_approx_interp(
            model_fn, a, b,
            gpc_deg=gpc_deg
            )
        gpc_approx_discrete_proj_fn_L, fcs_dp_L, _ = gpc_approx_discrete_proj_uniform(
            model_fn, a, b,
            gauss_quad_deg=gauss_quad_deg,
            gpc_deg=gpc_deg
            )

        fcs_interp, norms_sq = gpc_approx_interp_process(
            model_fn_param, Lspace, T, a, b,
            gpc_deg=gpc_deg
            )
        fcs_dp, _ = gpc_approx_discrete_proj_process(
            model_fn_param, Lspace, T, a, b,
            gauss_quad_deg=gauss_quad_deg,
            gpc_deg=gpc_deg
            )

        cmc_samples = cmc_approx_process(model_fn_param, Lspace, T, a, b,
                                         nsamples=10000)
        cmc_mean = np.mean(cmc_samples[:, :, Targ], axis=0)
        cmc_std = np.std(cmc_samples[:, :, Targ], axis=0, ddof=1.5)

        gpc_interp_mean = fcs_interp[:, Targ, 0]
        gpc_interp_std = np.sqrt(np.nansum(
            fcs_interp[:, Targ, 1:]**2 * norms_sq[1:], axis=1))

        gpc_dp_mean = fcs_dp[:, Targ, 0]
        gpc_dp_std = np.sqrt(np.nansum(
            fcs_dp[:, Targ, 1:]**2 * norms_sq[1:], axis=1))

        k = -1

        true_mean = quad(lambda alpha: model_fn(alpha), a, b)[0] / (b - a)
        true_var = quad(lambda alpha: (model_fn(alpha) - true_mean)**2,
                        a, b)[0] / (b - a)
        gpc_var_interp_L = np.nansum(fcs_interp_L[1:]**2 * norms_sq[1:])
        gpc_var_dp_L = np.nansum(fcs_dp_L[1:]**2 * norms_sq[1:])

        print('true mean', true_mean)
        print('gpc interp mean in L', fcs_interp_L[0])
        print('gpc disc. proj. mean in L', fcs_dp_L[0])
        print('gpc interp mean from proc', gpc_interp_mean[k])
        print('gpc disc. proj. mean from proc', gpc_dp_mean[k])

        print('true var', true_var)
        print('cmc var', cmc_std[k]**2)
        print('gpc interp var in L', gpc_var_interp_L)
        print('gpc disc. proj. var in L', gpc_var_dp_L)
        print('gpc interp var from proc', gpc_interp_std[k]**2)
        print('gpc disc. proj. var from proc', gpc_dp_std[k]**2)

        print('factor', gpc_var_dp_L / true_var)
        print('factor', gpc_var_interp_L / true_var)

        print()
        print()


def test_gpc_beta_higher_moments(
        model_fn=runge,
        #  a=-1, b=1,
        #  a=0, b=2,
        a=0, b=10,
        #  a=-3, b=3,
        M=6, K=6,
        sp_mult=100,
        plot=False,
        ):

    print('uniform gpc, cmc')
    g_uniform, fcs_uniform, hsq_uniform = gpc_approx_discrete_proj_uniform_tree(
        model_fn,
        a=a, b=b,
        gpc_deg=K, gauss_quad_deg=M,
        )

    cmc_uniform_samples = model_fn(np.random.uniform(a, b, size=np.power(10, 5)))

    cmc_uniform_mean, cmc_uniform_std = np.mean(cmc_uniform_samples), np.std(cmc_uniform_samples, ddof=1.5)
    cmc_uniform_skewness = stats.skew(cmc_uniform_samples)
    cmc_uniform_kurtosis = stats.kurtosis(cmc_uniform_samples, fisher=False)  # Pearson definition

    gpc_uniform_mean = fcs_uniform[0]
    gpc_uniform_var = np.nansum(fcs_uniform[1:]**2 * hsq_uniform[1:])
    gpc_uniform_std = np.sqrt(gpc_uniform_var)

    three_tensor_legendre = make_three_tensor_legendre(K)
    gpc_uniform_skewness = gpc_skewness(fcs=fcs_uniform, three_tensor=three_tensor_legendre, std=gpc_uniform_std)

    four_tensor_legendre = make_four_tensor_legendre(K=K)
    gpc_uniform_kurtosis = gpc_kurtosis(fcs=fcs_uniform, four_tensor=four_tensor_legendre, std=gpc_uniform_std)

    fourth_centr_moment_fn = lambda z: (z - gpc_uniform_mean)**4

    _, fcs_transformed, _ = gpc_approx_discrete_proj_uniform_tree(
        lambda y: fourth_centr_moment_fn(model_fn(y)),
        a=a, b=b,
        gpc_deg=K, gauss_quad_deg=M,
        )

    print('mean comparison ', gpc_uniform_mean, cmc_uniform_mean)
    print('std comparison ', gpc_uniform_std, cmc_uniform_std)
    print('skewness comparison ', gpc_uniform_skewness, cmc_uniform_skewness)
    print('kurtosis comparison ', gpc_uniform_kurtosis, fcs_transformed[0] / gpc_uniform_var**2, cmc_uniform_kurtosis)
    print()

    #####
    #  calpha, cbeta = 2, 2
    #  calpha, cbeta = 1, 1  # Should give approximately the same as uniform gPC expansion
    calpha, cbeta = 0.5, 0.5

    print('beta gpc, cmc')

    g_beta, fcs_beta, hsq_beta = gpc_approx_discrete_proj_beta(
        model_fn,
        a=a, b=b,
        calpha=calpha, cbeta=cbeta,
        gpc_deg=K, gauss_quad_deg=M,
        )

    cmc_beta_samples = model_fn(make_affine_transf(0, 1, a, b)(np.random.beta(a=calpha, b=cbeta, size=np.power(10, 5))))

    cmc_beta_mean, cmc_beta_std = np.mean(cmc_beta_samples), np.std(cmc_beta_samples, ddof=1.5)
    cmc_beta_skewness = stats.skew(cmc_beta_samples)
    cmc_beta_kurtosis = stats.kurtosis(cmc_beta_samples, fisher=False)  # Pearson definition

    gpc_beta_mean = fcs_beta[0]
    gpc_beta_std = np.sqrt(np.nansum(fcs_beta[1:]**2 * hsq_beta[1:]))

    # TODO or not
    #  three_tensor_jacobi = make_three_tensor_jacobi(K, calpha, cbeta)
    #  gpc_uniform_skewness = gpc_skewness(fcs=fcs_uniform, three_tensor=three_tensor_jacobi, std=gpc_uniform_std)

    #  four_tensor_jacobi = make_four_tensor_jacobi(K=K, calpha, cbeta)
    #  gpc_uniform_kurtosis = gpc_kurtosis(fcs=fcs_uniform, four_tensor=four_tensor_jacobi, std=gpc_uniform_std)

    print('mean comparison ', gpc_beta_mean, cmc_beta_mean)
    print('std comparison ', gpc_beta_std, cmc_beta_std)
    #  print('skewness comparison ', gpc_beta_skewness, cmc_beta_skewness)
    #  print('kurtosis comparison ', gpc_beta_kurtosis, cmc_beta_kurtosis)
    print()

    if plot:
        x = np.linspace(a, b, int(np.ceil(b - a))*sp_mult + 1)
        plt.plot(x, model_fn(x), 'r--')
        plt.plot(x, g_uniform(x), 'k', lw=.5)
        plt.plot(x, g_beta(x), 'b', lw=.5)
        plt.show()


def test_gpc_uniform_transform_kurt(
        func=None,
        model_fn=None,
        npoints=15,
        plot=False,
        nticks=3,
        ):

    #####
    mean_quad_uniform = quad(lambda x: model_fn(x), -1, 1)[0] / 2
    var_quad_uniform = quad(
        lambda x: (model_fn(x) - mean_quad_uniform)**2, -1, 1)[0] / 2
    kurt_quad_uniform = quad(
        lambda x: (model_fn(x) - mean_quad_uniform)**4,
        -1, 1)[0] / (2 * var_quad_uniform**2)

    kurt_approx_uniform = []
    kurt_approx_transformed = []

    K = 1
    KK = np.arange(K, K + npoints)
    MM = KK + 1

    four_tensor_legendre = make_four_tensor_legendre(K=KK[-1])

    for K, M in zip(KK, MM):
        ##### Strong uniform gPC
        _, fcs_uniform, hsq_uniform = gpc_approx_discrete_proj_uniform_tree(
            model_fn,
            a=-1, b=1,
            gpc_deg=K, gauss_quad_deg=M,
            )

        var = np.nansum(fcs_uniform[1:]**2 * hsq_uniform[1:])
        kurt = gpc_kurtosis(
            fcs=fcs_uniform,
            four_tensor=four_tensor_legendre[:K+1, :K+1, :K+1, :K+1], var=var)

        kurt_approx_uniform.append(kurt)

        # Weak uniform gPC for kurtosis
        fourth_central_moment = lambda z: (z - fcs_uniform[0])**4
        transformed_model_fn = lambda y: fourth_central_moment(model_fn(y))

        _, fcs_transformed, _ = gpc_approx_discrete_proj_uniform_tree(
            transformed_model_fn,
            a=-1, b=1,
            gpc_deg=K, gauss_quad_deg=M,
            )

        kurt_approx_transformed.append(fcs_transformed[0] / var**2)

    kurt_approx_uniform = np.array(kurt_approx_uniform)
    kurt_approx_transformed = np.array(kurt_approx_transformed)

    #  print('Kurts:')
    #  print(kurt_quad_uniform)
    #  print(kurt_approx_uniform)
    #  print(kurt_approx_transformed)

    fig, axes = plt.subplots(nrows=1, ncols=1)
    fig.set_figheight(fig.get_figheight() * figfactor)
    fig.set_figwidth(fig.get_figwidth() * figfactor)

    uniform_label = 'Weak uniform app.'
    transformed_label = 'Weak transf. uniform app.'
    xlabel = 'K'
    other = 'M'
    rv = 'f(U)'

    figname = 'uniform_transformed'
    #  figname = 'uniform_transformed_logscale'

    #  axes[0].plot(KK, kurt_quad_uniform - kurt_approx_uniform,
    #                  'b.--', lw=0.5, label=uniform_label)
    #  axes[0].plot(KK, kurt_quad_uniform - kurt_approx_transformed,
    #                  'g.--', lw=0.5, label=transformed_label)
    #  #  axes[0].set_yscale('log')
    #  axes[0].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    #  axes[0].set_xticks([k * npoints // nticks for k in range(1, 1 + nticks)])
    #  set_axis_labels_endaxes(xlabel, 'Bias est. of $\\mathrm{{Kurt}}[{}]$'.format(rv), axes[0])

    axes.plot(KK, np.abs((kurt_quad_uniform - kurt_approx_uniform) / kurt_quad_uniform),
                 'b.--', lw=0.5, label=uniform_label)
    axes.plot(KK, np.abs((kurt_quad_uniform - kurt_approx_transformed) / kurt_quad_uniform),
                 'g.--', lw=0.5, label=transformed_label)
    axes.set_yscale('log')
    axes.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    axes.set_xticks([k * npoints // nticks for k in range(1, 1 + nticks)])
    axes.legend()
    set_axis_labels_endaxes(xlabel, 'Rel. bias est. of $\\mathrm{{Kurt}}[{}]$'.format(rv), axes)

    fig.savefig(
        './figs/UQ_test_{}_kurtosis_{}_{}_{}_{}_{}-{}'
        '.pdf'.format(
            figname,
            func,
            other, '=K+1',
            xlabel, KK[0], KK[-1],
            ),
        format='pdf',
        )


def test_gpc_beta_transform_mean_var(
        func=None,
        model_fn=None,
        calpha=2, cbeta=5,
        npoints=15,
        plot=False,
        ):

    #####
    #  calpha, cbeta = 0.5, 0.5
    #  calpha, cbeta = 1, 1  # Should give approximately the same as uniform gPC expansion
    #  calpha, cbeta = 2, 2

    mean_quad_beta = quad(lambda x: model_fn(x) * stats.beta.pdf(x, a=calpha, b=cbeta), 0, 1)[0]
    var_quad_beta = quad(lambda x: (model_fn(x) - mean_quad_beta)**2 * stats.beta.pdf(x, a=calpha, b=cbeta), 0, 1)[0]
    #  print(mean_quad_beta, var_quad_beta)
    #  samples = model_fn(np.random.beta(a=calpha, b=cbeta, size=np.power(10, 5)))
    #  print(np.mean(samples), np.var(samples, ddof=1))

    mean_approx_beta = []
    var_approx_beta = []

    mean_approx_uniform = []
    var_approx_uniform = []

    var_approx_transformed = []

    K = 1
    KK = np.arange(K, K + npoints)
    MM = KK + 1

    uniform_model_fn = lambda y: model_fn(
        stats.beta.ppf(make_affine_transf(-1, 1, 0, 1)(y), calpha, cbeta))
    uniform_model_inv_transf = lambda z: make_affine_transf(0, 1, -1, 1)(
        stats.beta.cdf(z, calpha, cbeta))

    for K, M in zip(KK, MM):
        ##### Strong beta gPC
        gpc_beta, fcs_beta, hsq_beta = gpc_approx_discrete_proj_beta(
            model_fn,
            a=0, b=1,
            calpha=calpha, cbeta=cbeta,
            gpc_deg=K, gauss_quad_deg=M,
            )

        mean_approx_beta.append(fcs_beta[0])
        var_approx_beta.append(np.nansum(fcs_beta[1:]**2 * hsq_beta[1:]))

        ##### Weak uniform gPC
        gpc_uniform, fcs_uniform, hsq_uniform = gpc_approx_discrete_proj_uniform_tree(
            uniform_model_fn,
            a=-1, b=1,
            gpc_deg=K, gauss_quad_deg=M,
            )

        mean_approx_uniform.append(fcs_uniform[0])
        var_approx_uniform.append(np.nansum(fcs_uniform[1:]**2 * hsq_uniform[1:]))

        # Weak uniform gPC for variance
        var_fn = lambda z: (z - fcs_uniform[0])**2
        transformed_model_fn = lambda y: var_fn(uniform_model_fn(y))

        _, fcs_transformed, _ = gpc_approx_discrete_proj_uniform_tree(
            transformed_model_fn,
            a=-1, b=1,
            gpc_deg=K, gauss_quad_deg=M,
            )

        var_approx_transformed.append(fcs_transformed[0])

        # For presentation, strong and weak app. plotted
        if K == KK[-1]:
            fig_app, ax_app = plt.subplots(nrows=1, ncols=1)
            fig_app.set_figheight(fig_app.get_figheight() * figfactor)
            fig_app.set_figwidth(fig_app.get_figwidth() * figfactor)

            figname_app = 'strong_weak_app'

            xspace = np.linspace(0, 1, )

            ax_app.plot(xspace, model_fn(xspace),
                        'k--', label='True')
            ax_app.plot(xspace, gpc_beta(xspace),
                        'r--', linewidth=.5, label='Strong app.')
            ax_app.plot(xspace, gpc_uniform(uniform_model_inv_transf(xspace)),
                        'b--', linewidth=.5, label='Weak app.')

            xlabel = '$z$'
            ylabel = '$f(z); K = {}, M = {}$'.format(K, M)
            set_axis_labels_endaxes(xlabel, ylabel, ax_app)
            ax_app.legend()

            fig_app.savefig(
                './figs/fit_gpc_{}_{}_'
                'K_{}_M_{}.pdf'.format(figname_app, func,
                                       K, M),
                format='pdf', transparent=True,
                )


    mean_approx_beta = np.array(mean_approx_beta)
    var_approx_beta = np.array(var_approx_beta)
    mean_approx_uniform = np.array(mean_approx_uniform)
    var_approx_uniform = np.array(var_approx_uniform)
    var_approx_transformed = np.array(var_approx_transformed)

    fig, axes = plt.subplots(nrows=1, ncols=2)
    fig.set_figheight(fig.get_figheight() / 2)

    figname = 'est_mean_var_beta_vs_uniform'

    beta_label = 'Strong beta app.'
    uniform_label = 'Weak uniform app.'
    transformed_label = 'Weak transf. uniform app.'
    xlabel = 'K'
    other = 'M'
    rv = 'f(B)'

    #  axes[0, 0].plot(KK, mean_quad_beta - mean_approx_beta,
    #                  'r.--', lw=0.5, label=beta_label)
    #  axes[0, 0].plot(KK, mean_quad_beta - mean_approx_uniform,
    #                  'b.--', lw=0.5, label=uniform_label)
    #  axes[0, 0].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    #  #  axes[0, 0].set_yscale('log')
    #  set_axis_labels_endaxes(xlabel, 'Bias est. of ${}$'.format(xe(rv)), axes[0, 0])

    #  axes[0, 1].plot(KK, var_quad_beta - var_approx_beta,
    #                  'r.--', lw=0.5, label=beta_label)
    #  axes[0, 1].plot(KK, var_quad_beta - var_approx_uniform,
    #                  'b.--', lw=0.5, label=uniform_label)
    #  axes[0, 1].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    #  #  axes[0, 1].set_yscale('log')
    #  set_axis_labels_endaxes(xlabel, 'Bias est. of ${}$'.format(xv(rv)), axes[0, 1])

    axes[0].plot(KK, np.abs((mean_quad_beta - mean_approx_beta) / mean_quad_beta),
                 'r.--', lw=0.5, label=beta_label)
    axes[0].plot(KK, np.abs((mean_quad_beta - mean_approx_uniform) / mean_quad_beta),
                 'b.--', lw=0.5, label=uniform_label)
    axes[0].set_yscale('log')
    set_axis_labels_endaxes(xlabel, 'Rel. bias est. of ${}$'.format(xe(rv)), axes[0])

    axes[1].plot(KK, np.abs((var_quad_beta - var_approx_beta) / var_quad_beta),
              'r.--', lw=0.5, label=beta_label)
    axes[1].plot(KK, np.abs((var_quad_beta - var_approx_uniform) / var_quad_beta),
              'b.--', lw=0.5, label=uniform_label)
    axes[1].set_yscale('log')
    axes[1].legend()
    set_axis_labels_endaxes(xlabel, 'Rel. bias est. of ${}$'.format(xv(rv)), axes[1])

    fig.savefig(
        './figs/UQ_test_{}_alpha_{}_beta_{}_{}_{}_{}_{}_{}-{}'
        '.pdf'.format(
            figname,
            calpha, cbeta,
            func,
            other, '=K+1',
            xlabel, KK[0], KK[-1],
            ),
        format='pdf', transparent=True,
        )

    # Only need this for a specific function
    if func != 'runge':
        return

    fig2, axes2 = plt.subplots(nrows=1, ncols=1)
    fig2.set_figheight(fig2.get_figheight() * figfactor)
    fig2.set_figwidth(fig2.get_figwidth() * figfactor)

    figname = 'uniform_vs_transf_func'

    #  axes2[0].plot(KK, var_quad_beta - var_approx_uniform,
    #                  'b.--', lw=0.5, label=uniform_label)
    #  axes2[0].plot(KK, var_quad_beta - var_approx_transformed,
    #                  'g.--', lw=0.5, label=transformed_label)
    #  axes2[0].set_yscale('log')
    #  set_axis_labels_endaxes(xlabel, 'Bias est. of ${}$'.format(xv(rv)), axes2[0])

    axes2.plot(KK, np.abs((var_quad_beta - var_approx_uniform) / var_quad_beta),
                 'b.--', lw=0.5, label=uniform_label)
    axes2.plot(KK, np.abs((var_quad_beta - var_approx_transformed) / var_quad_beta),
                 'g.--', lw=0.5, label=transformed_label)
    axes2.set_yscale('log')
    axes2.legend()
    set_axis_labels_endaxes(xlabel, 'Rel. bias est. of ${}$'.format(xv(rv)), axes2)

    fig2.savefig(
        './figs/UQ_test_{}_alpha_{}_beta_{}_{}_{}_{}_{}_{}-{}'
        '.pdf'.format(
            figname,
            calpha, cbeta,
            func,
            other, '=K+1',
            xlabel, KK[0], KK[-1],
            ),
        format='pdf', transparent=True,
        )

    #  print('Means')
    #  print(mean_approx_beta)
    #  print(mean_approx_uniform)
    #  print('Variances')
    #  print(var_approx_beta)
    #  print(var_approx_uniform)


def test_gpc_beta_transform_mean_var_multiple(
        model_fns=None,
        funcnames=None,
        calpha=2, cbeta=5,
        npoints=15,
        plot=False,
        figname='est_mean_var_all',
        ):

    def pdf(x):
        return stats.beta.pdf(x, a=calpha, b=cbeta)

    mean_approxes_beta = []
    var_approxes_beta = []

    mean_approxes_uniform = []
    var_approxes_uniform = []

    for model_fn in model_fns:
        mean_approx_beta = []
        var_approx_beta = []

        mean_approx_uniform = []
        var_approx_uniform = []

        K = 1
        KK = np.arange(K, K + npoints)
        MM = KK + 1

        for K, M in zip(KK, MM):
            ##### Strong beta gPC
            _, fcs_beta, hsq_beta = gpc_approx_discrete_proj_beta(
                model_fn,
                a=0, b=1,
                calpha=calpha, cbeta=cbeta,
                gpc_deg=K, gauss_quad_deg=M,
                )

            mean_approx_beta.append(fcs_beta[0])
            var_approx_beta.append(np.nansum(fcs_beta[1:]**2 * hsq_beta[1:]))

            ##### Weak uniform gPC
            uniform_model_fn = lambda y: model_fn(
                stats.beta.ppf(make_affine_transf(-1, 1, 0, 1)(y), calpha, cbeta))

            _, fcs_uniform, hsq_uniform = gpc_approx_discrete_proj_uniform_tree(
                uniform_model_fn,
                a=-1, b=1,
                gpc_deg=K, gauss_quad_deg=M,
                )

            mean_approx_uniform.append(fcs_uniform[0])
            var_approx_uniform.append(np.nansum(fcs_uniform[1:]**2 * hsq_uniform[1:]))

        mean_approxes_beta.append(mean_approx_beta)
        var_approxes_beta.append(var_approx_beta)
        mean_approxes_uniform.append(mean_approx_uniform)
        var_approxes_uniform.append(var_approx_uniform)

    mean_approxes_beta = np.array(mean_approxes_beta)
    var_approxes_beta = np.array(var_approxes_beta)
    mean_approxes_uniform = np.array(mean_approxes_uniform)
    var_approxes_uniform = np.array(var_approxes_uniform)

    fig, axes = plt.subplots(nrows=1, ncols=2)
    fig.set_figheight(fig.get_figheight() / 2)

    xlabel = 'K'
    other = 'M'
    rv = 'f(B)'

    it = enumerate(zip(
        model_fns, funcnames,
        mean_approxes_beta, var_approxes_beta,
        mean_approxes_uniform, var_approxes_uniform,
        ))
    for i, (model_fn, func,
            mean_approx_beta, var_approx_beta,
            mean_approx_uniform, var_approx_uniform,
            ) in it:

        # Baseline
        mean_quad_beta = quad(lambda x: model_fn(x) * pdf(x), 0, 1)[0]
        var_quad_beta = quad(
            lambda x: (model_fn(x) - mean_quad_beta)**2 * pdf(x), 0, 1)[0]

        #  axes[0, 0].plot(
        #      KK, np.abs((mean_quad_beta - mean_approx_beta) / mean_quad_beta),
        #      '.--', lw=0.5,
        #      )
        axes[0].plot(
            KK, np.abs((var_quad_beta - var_approx_beta) / var_quad_beta),
            '.--', lw=0.5,
            )
        #  axes[1, 0].plot(
        #      KK, np.abs((mean_quad_beta - mean_approx_uniform) / mean_quad_beta),
        #      '.--', lw=0.5,
        #      )
        axes[1].plot(
            KK, np.abs((var_quad_beta - var_approx_uniform) / var_quad_beta),
            '.--', lw=0.5,
            label=func,
            )

    #  set_axis_labels_endaxes(
    #      xlabel, 'Rel. bias strong app. of ${}$'.format(xe(rv)), axes[0, 0])
    set_axis_labels_endaxes(
        xlabel, 'Rel. bias strong app. of ${}$'.format(xv(rv)), axes[0])
    #  set_axis_labels_endaxes(
    #      xlabel, 'Rel. bias weak app. of ${}$'.format(xe(rv)), axes[1, 0])
    set_axis_labels_endaxes(
        xlabel, 'Rel. bias weak app. of ${}$'.format(xv(rv)), axes[1])
    #  axes[0, 0].set_yscale('log')
    axes[0].set_yscale('log')
    #  axes[1, 0].set_yscale('log')
    axes[1].set_yscale('log')
    set_legend_under(fig, axes)

    fig.savefig(
        './figs/UQ_test_{}_alpha_{}_beta_{}_{}_{}_{}_{}_{}-{}'
        '.pdf'.format(
            figname,
            calpha, cbeta,
            'multiple',
            other, '=K+1',
            xlabel, KK[0], KK[-1],
            ),
        format='pdf', transparent=True,
        bbox_inches='tight',
        )


def test_gpc_beta_transform_mean_var_multiple_twoplots(
        model_fns=None,
        funcnames=None,
        calpha=2, cbeta=5,
        npoints=15,
        plot=False,
        ):

    def pdf(x):
        return stats.beta.pdf(x, a=calpha, b=cbeta)

    mean_approxes_beta = []
    var_approxes_beta = []

    mean_approxes_uniform = []
    var_approxes_uniform = []

    for model_fn in model_fns:
        mean_approx_beta = []
        var_approx_beta = []

        mean_approx_uniform = []
        var_approx_uniform = []

        K = 1
        KK = np.arange(K, K + npoints)
        MM = KK + 1

        for K, M in zip(KK, MM):
            ##### Strong beta gPC
            _, fcs_beta, hsq_beta = gpc_approx_discrete_proj_beta(
                model_fn,
                a=0, b=1,
                calpha=calpha, cbeta=cbeta,
                gpc_deg=K, gauss_quad_deg=M,
                )

            mean_approx_beta.append(fcs_beta[0])
            var_approx_beta.append(np.nansum(fcs_beta[1:]**2 * hsq_beta[1:]))

            ##### Weak uniform gPC
            uniform_model_fn = lambda y: model_fn(
                stats.beta.ppf(make_affine_transf(-1, 1, 0, 1)(y), calpha, cbeta))

            _, fcs_uniform, hsq_uniform = gpc_approx_discrete_proj_uniform_tree(
                uniform_model_fn,
                a=-1, b=1,
                gpc_deg=K, gauss_quad_deg=M,
                )

            mean_approx_uniform.append(fcs_uniform[0])
            var_approx_uniform.append(np.nansum(fcs_uniform[1:]**2 * hsq_uniform[1:]))

        mean_approxes_beta.append(mean_approx_beta)
        var_approxes_beta.append(var_approx_beta)
        mean_approxes_uniform.append(mean_approx_uniform)
        var_approxes_uniform.append(var_approx_uniform)

    mean_approxes_beta = np.array(mean_approxes_beta)
    var_approxes_beta = np.array(var_approxes_beta)
    mean_approxes_uniform = np.array(mean_approxes_uniform)
    var_approxes_uniform = np.array(var_approxes_uniform)

    fig, axes = plt.subplots(nrows=1, ncols=2)
    #  fig.set_figheight(fig.get_figheight() / 2)

    figname = 'est_mean_var_strong'

    beta_label = 'Strong approximation'
    xlabel = 'K'
    other = 'M'
    rv = 'f(B)'

    it = enumerate(zip(
        model_fns, funcnames, mean_approxes_beta, var_approxes_beta))
    for i, (model_fn, func, mean_approx_beta, var_approx_beta) in it:
        # Baseline
        mean_quad_beta = quad(lambda x: model_fn(x) * pdf(x), 0, 1)[0]
        var_quad_beta = quad(
            lambda x: (model_fn(x) - mean_quad_beta)**2 * pdf(x), 0, 1)[0]

        axes[0].plot(
            KK, np.abs((mean_quad_beta - mean_approx_beta) / mean_quad_beta),
            '.--', lw=0.5,
            )
        axes[1].plot(
            KK, np.abs((var_quad_beta - var_approx_beta) / var_quad_beta),
            '.--', lw=0.5,
            label='{} of {}'.format(beta_label, func),
            )

    set_axis_labels_endaxes(
        xlabel, 'Rel. bias app. of ${}$'.format(xe(rv)), axes[0])
    set_axis_labels_endaxes(
        xlabel, 'Rel. bias app. of ${}$'.format(xv(rv)), axes[1])
    axes[0].set_yscale('log')
    axes[1].set_yscale('log')
    set_legend_under(fig, axes, 2)

    fig.savefig(
        './figs/UQ_test_{}_alpha_{}_beta_{}_{}_{}_{}_{}_{}-{}'
        '.pdf'.format(
            figname,
            calpha, cbeta,
            'multiple',
            other, '=K+1',
            xlabel, KK[0], KK[-1],
            ),
        format='pdf', transparent=True,
        bbox_inches='tight',
        )

    #####

    fig, axes = plt.subplots(nrows=1, ncols=2)

    figname = 'est_mean_var_weak'
    uniform_label = 'Weak approximation'

    it = enumerate(zip(
        model_fns, funcnames, mean_approxes_uniform, var_approxes_uniform))
    for i, (model_fn, func, mean_approx_uniform, var_approx_uniform) in it:
        # Baseline
        mean_quad_beta = quad(lambda x: model_fn(x) * pdf(x), 0, 1)[0]
        var_quad_beta = quad(
            lambda x: (model_fn(x) - mean_quad_beta)**2 * pdf(x), 0, 1)[0]

        axes[0].plot(
            KK, np.abs((mean_quad_beta - mean_approx_uniform) / mean_quad_beta),
            '.--', lw=0.5,
            )
        axes[1].plot(
            KK, np.abs((var_quad_beta - var_approx_uniform) / var_quad_beta),
            '.--', lw=0.5,
            label='{} of {}'.format(uniform_label, func),
            )

    set_axis_labels_endaxes(
        xlabel, 'Rel. bias app. of ${}$'.format(xe(rv)), axes[0])
    set_axis_labels_endaxes(
        xlabel, 'Rel. bias app. of ${}$'.format(xv(rv)), axes[1])
    axes[0].set_yscale('log')
    axes[1].set_yscale('log')
    set_legend_under(fig, axes, 2)

    fig.savefig(
        './figs/UQ_test_{}_alpha_{}_beta_{}_{}_{}_{}_{}_{}-{}'
        '.pdf'.format(
            figname,
            calpha, cbeta,
            'multiple',
            other, '=K+1',
            xlabel, KK[0], KK[-1],
            ),
        format='pdf', transparent=True,
        bbox_inches='tight',
        )


def gpc_plot_over_underdetermination(
        model_fn=None,
        KM_arr=None,
        #  a=-1, b=1,
        a=-2, b=4,
        sp_mult=50,
        ):

    assert a < b, 'a not smaller than b'

    if model_fn is None:
        model_fn = lambda x: (x + 1) * x * (x - 2)

    if KM_arr is None:
        KM_arr = [
                  #  (2, 3),
                  (3, 3),
                  (3, 4),
                  (8, 5),
                  (8, 6),
                  ]

    #####
    drift_space = np.linspace(a, b, int(np.ceil(b - a))*sp_mult + 1)
    true_model_drift_space = model_fn(drift_space)

    # Plot true and approx using both naive gPC and func gPC approx
    fig, axes = plt.subplots(nrows=2, ncols=2)

    figname = 'under_overdetermination'

    for i, (K, M) in enumerate(KM_arr):
        ax = axes[int(i / 2), i % 2]

        gpc_fn, _, _, nodes = gpc_approx_interp(
            model_fn,
            a, b,
            gpc_deg=K,
            nnodes=M,
            returnnodes=True,
            )

        gpc_approx_drift_space = gpc_fn(drift_space)

        ax.plot(drift_space, true_model_drift_space, 'k--', label='True')
        ax.plot(drift_space, gpc_approx_drift_space, 'r',
                linewidth=.5, label='gPC')

        ax.scatter(nodes, model_fn(nodes))

        xlabel = '$z$'
        ylabel = '$f(z); K = {}, M = {}$'.format(K, M)
        set_axis_labels_endaxes(xlabel, ylabel, ax)

        if i == 0:
            ax.legend()

    fig.savefig(
        './figs/fit_gpc_{}_'
        #  'K_{}_M_{}-{}_a_{}_b_{}.pdf'.format(figname,
        '_a_{}_b_{}.pdf'.format(figname,
                                a, b),
        format='pdf',
        )


def gpc_plot_aliasing(
        model_fn=None,
        K=5, MM=None,
        #  a=-1, b=1,
        a=-2, b=4,
        sp_mult=50,
        ):

    assert a < b, 'a not smaller than b'

    deg = 5
    #  deg = 3
    if model_fn is None:
        #  model_fn = lambda z: z**deg + 10*z - 1
        model_fn = lambda x: (x + 1.5) * (x + 1) * x * (x - 2) * (x - 3)

    if MM is None:
        M = int((deg + K + 1) / 2) - 2
        MM = list(range(M, M + 4))

    #####
    drift_space = np.linspace(a, b, int(np.ceil(b - a))*sp_mult + 1)
    true_model_drift_space = model_fn(drift_space)

    # Plot true and approx using both naive gPC and func gPC approx
    fig, axes= plt.subplots(nrows=2, ncols=2)

    figname = 'aliasing_multiple_{}b'.format(deg)

    for i, M in enumerate(MM):
        ax = axes[int(i / 2), i % 2]

        gpc_fn, _, _ = gpc_approx_discrete_proj_uniform(
            model_fn,
            a, b,
            gauss_quad_deg=M,
            gpc_deg=K,
            #  returnnodes=True,  # makes no sense, are quad nodes
            )

        gpc_approx_drift_space = gpc_fn(drift_space)

        ax.plot(drift_space, true_model_drift_space, 'k--', label='True')
        ax.plot(drift_space, gpc_approx_drift_space, 'r',
                linewidth=.5, label='gPC')

        xlabel = '$z$'
        ylabel = '$f(z); M = {}$'.format(M)
        set_axis_labels_endaxes(xlabel, ylabel, ax)

        if i == 0:
            ax.legend()

    fig.savefig(
        './figs/fit_gpc_{}_'
        'K_{}_M_{}-{}_a_{}_b_{}.pdf'.format(figname,
                                         K, MM[0], MM[-1], a, b),
        format='pdf',
        )


def gpc_plot_equivalence(
        model_fn=None,
        K=4,
        MM=None,
        KM_arr=None,
        #  a=-1, b=1,
        a=-2, b=4,
        sp_mult=50,
        ):

    assert a < b, 'a not smaller than b'

    deg = 5
    #  deg = 7
    if model_fn is None:
        model_fn = lambda x: (x + 1.5) * (x + 1) * x * (x - 2) * (x - 3)
        #  model_fn = lambda x: (x + 1.5) * (x + 1) * x * (x - 1) * (x - 2) * (x - 3) * (x - 3.5)

    if MM is None:
        M = K + 1
        MM = list(range(M, M + 4))

    if KM_arr is None:
        k = np.arange(deg - 3, deg + 1)
        m = k + 1
        KM_arr = zip(k, m)
        #  KM_arr = [
        #            (2, 3),
        #            (3, 4),
        #            (4, 5),
        #            (5, 6),
        #            ]

    #####
    drift_space = np.linspace(a, b, int(np.ceil(b - a))*sp_mult + 1)
    true_model_drift_space = model_fn(drift_space)

    # Plot true and approx using both naive gPC and func gPC approx
    fig, axes= plt.subplots(nrows=2, ncols=2)

    figname = 'equivalence_{}'.format(deg)

    #  for i, M in enumerate(MM):
    for i, (K, M) in enumerate(KM_arr):
        ax = axes[int(i / 2), i % 2]

        gpc_fn_interp, _, _ = gpc_approx_interp(
            model_fn,
            a, b,
            nnodes=M,
            gpc_deg=K,
            weights=True,
            )

        gpc_fn_discproj, _, _ = gpc_approx_discrete_proj_uniform(
            model_fn,
            a, b,
            gauss_quad_deg=M,
            gpc_deg=K,
            )

        ax.plot(drift_space, true_model_drift_space, 'k--', label='True')
        ax.plot(drift_space, gpc_fn_interp(drift_space), 'r',
                linewidth=.5, label='gPC weighted least sq/interp')
        ax.plot(drift_space, gpc_fn_discproj(drift_space), 'b',
                linewidth=.5, label='gPC discproj')

        xlabel = '$z$'
        ylabel = '$f(z); K = {}, M = {}$'.format(K, M)
        set_axis_labels_endaxes(xlabel, ylabel, ax)
        #  ax.set_title('$$'.format(K, M))

        if i == 0:
            ax.legend()

    #  plt.show()
    fig.savefig(
        './figs/fit_gpc_{}_'
        'a_{}_b_{}.pdf'.format(figname,
                               a, b),
        format='pdf',
        )


if __name__ == "__main__":
    #  test_gpc_mean_std()

    #  model_fn = lambda x: x**2
    #  model_fn = lambda x: np.sin(x) * np.exp(x)
    #  model_fn = lambda x: np.sin(np.exp(-x))
    #  test_gpc_beta_higher_moments()
    #  test_gpc_beta_higher_moments(model_fn=model_fn)

    # For theory chapter
    #  gpc_plot_precision()
    #  gpc_plot_over_underdetermination()
    #  gpc_plot_aliasing()
    #  gpc_plot_equivalence()

    # Applications
    #  gpc_approx_error()

    #  deg = 5
    #  test_gpc_uniform_transform_kurt(
    #      func='pow_x_{}'.format(deg),
    #      model_fn=lambda x: x**deg
    #      )
    #  test_gpc_uniform_transform_kurt(func='runge', model_fn=runge, npoints=25)

    #  for deg in [2, 3, 4, 5]:
    #      model_fn = lambda x: x**deg
    #      test_gpc_beta_transform_mean_var(
    #          func='pow_x_{}'.format(deg),
    #          model_fn=model_fn
    #          )

    #  test_gpc_beta_transform_mean_var(func='tan', model_fn=np.tan)
    #  test_gpc_beta_transform_mean_var(func='runge', model_fn=runge, npoints=25)

    L = 26
    T = 30

    model_fns = [
                 #  lambda z: 1 / (1 + 25*z**2),
                 #  np.cos,
                 #  lambda z: z / np.sqrt(1 - z**2),
                 #  lambda z: np.tan(np.pi*z - np.pi / 2),
                 lambda z: np.sin(z) / z,
                 lambda alpha: exact_prob_bm_rm(L, T, alpha),
                 np.log,
                 ]
    funcnames = [
                 #  'Runge',
                 #  'cos',
                 #  '$z\\mapsto \\frac{{z}}{{\\sqrt[]{{1 - z^2}}}}$',
                 #  'adjusted\_tan',
                 '$z \\mapsto \\frac{{\\sin(z)}}{{z}}$',
                 '$\\gamma$ from BM with drift',
                 'log',
                 ]

    #  test_gpc_beta_transform_mean_var(
    #      func=funcnames[-1], model_fn=model_fns[-1])

    test_gpc_beta_transform_mean_var_multiple(
        model_fns=model_fns,
        funcnames=funcnames,
        npoints=25,
        figname='est_mean_presentation',
        )
    #  test_gpc_beta_transform_mean_var_multiple_twoplots(
    #      model_fns=model_fns,
    #      funcnames=funcnames,
    #      npoints=25,)

#!/bin/bash
#SBATCH --job-name=RFGENLOTS
#SBATCH --output=%x-%j.out
#SBATCH -N 1
#SBATCH --tasks-per-node 15
#
#SBATCH --time=120:00:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: RFGENLOTS"

#Determining the number of processors in the system
NPROC=`nproc --all`
let lim=$NPROC-1

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

case $HOSTNAME in
  (antergos-sopropo) work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl) echo "should not run on ${HOSTNAME} without sbatch, exit"; exit 1;;
  (*) module load 2019; module unload Python; module load Python/3.6.6-intel-2018b; work_dir="$HOME/thesis_rainfall";;
esac
echo `which python3`

gg_deg=8
# method='FE'
method='FNS'
methodparam=100
# method='FO'
# methodparam="10 3"
nloops=100
nsimsperrun=10

for i in `seq $nloops`; do
  for ((node_index=0; node_index < gg_deg; node_index++)); do
    params="--nsims ${nsimsperrun} --gauss_quad_deg ${gg_deg} --node_index ${node_index} --${method} ${methodparam}";

    # python3 "${work_dir}/rainfall_model_uq_gendata_one.py" --datetime "$dt" $params &
    # python3 "${work_dir}/rainfall_model_uq_gendata_one.py" $params &
    python3 "${work_dir}/rainfall_model_uq_gendata_nonparallel.py" $params &

    while (( `jobs | wc -l` >= $lim )); do
      sleep 5
    done
  done
done

wait

echo end of job

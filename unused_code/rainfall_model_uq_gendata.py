import multiprocessing
import os
import shutil
import time

from rf_cumulative_splitting import *
from UQ_fitters import gpc_approx_discrete_proj_uniform_tree_nodes

from helper_functions import (
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    )


def worker(work_args):
    (outfile, nsims, alg_dict, method, rfparams, rmax, ndays,
     alg_params, args_key) = work_args

    with open(outfile, 'a+') as fp_meth:
        t = time.process_time()

        for _ in range(nsims):
            print(alg_dict[method](params=rfparams, rmax=rmax, ndays=ndays,
                                   alg_params=alg_params,
                  ),
                file=fp_meth)

        elapsed = time.process_time() - t

    print('name:')
    print(multiprocessing.current_process().name)
    print('Method:')
    print(method)
    #  print('nsims:')
    #  print('{:,}'.format(nsims).replace(',', ' '))
    print('Alg params:')
    print(alg_params)
    print('Process time:')
    print(elapsed)
    print('Dictionary entry:')
    print({args_key: elapsed})
    print()
    print()


def main():
    # Prepare multiprocessing
    manager = multiprocessing.Manager()
    jobs = []

    # Get args
    args = rf_argparser()
    (datetimestr, nsims, L, T, nlevels, rfparams_def,
     gauss_quad_deg, gpc_deg
     ) = (args.datetime, args.nsims,
          args.L, args.T, args.nlevels, list(args.rfparams),
          args.gauss_quad_deg, args.gpc_deg,
          )

    a, b = 0.5, 1.5
    work_nodes = gpc_approx_discrete_proj_uniform_tree_nodes(
        a=a, b=b,
        gauss_quad_deg=gauss_quad_deg, gpc_deg=gpc_deg,  # M, K
        )
    Ucov_arr = np.multiply.outer(work_nodes, rfparams_def[4])

    rmax = L
    ndays = T
    importance_fn_distance = make_importance_fn_distance(ndays, rmax)

    # c = 0.6275; gamma = 0.001; int(np.round(c * np.abs(np.log(gamma))))

    print('nsims = ', '{:,}'.format(nsims).replace(',', ' '))
    print('nlevels = ', '{:,}'.format(nlevels).replace(',', ' '))
    print()
    print('L = ', L)
    print('T = ', T)
    print()

    alg_dict = {
                'CMC': cumulative_rainfall_cmc,
                'FE': cumulative_rainfall_splitting_fixed_effort,
                'FNS': cumulative_rainfall_splitting_fixed_successes,
                'FO': cumulative_rainfall_splitting_fixed_offspring,
                }

    # Prepare filenames
    outfolder = tofolder = 'out/rf/'
    if args.test:
        outfolder = outfolder + 'test/'
        tofolder = tofolder + 'test/'
    if not args.local:
        outfolder = '{}/multi_splitting_inb_{}/{}'.format(os.getenv("TMPDIR"),
                                                          datetimestr,
                                                          outfolder)
        tofolder = '{}/{}'.format(os.getenv("HOME"), tofolder)

    os.makedirs(outfolder, exist_ok=True)
    os.makedirs(tofolder, exist_ok=True)

    name = '_rf_'
    filename_out_pre = "".join([outfolder, datetimestr, name])
    filename_to_pre = "".join([tofolder, datetimestr, name])

    for Ucov in Ucov_arr:
        rfparams = np.copy(rfparams_def)
        rfparams[4] = Ucov

        for method in ['CMC', 'FE', 'FNS', 'FO', ]:
            for method_params in getattr(args, method):
                if method_params[0] < 0:
                    print('Skipping ', method)
                    break
                print('New job')

                args_key = args_str_maker(
                    method, method_params,
                    nsims=nsims, L=L, T=T, nlevels=nlevels,
                    rfparams=rf_params_tostring(rfparams),
                    )
                outfile = filename_out_pre + args_key

                work_args = outfile, nsims, alg_dict, method, \
                    rfparams, rmax, ndays, \
                    (nlevels, *method_params, importance_fn_distance), \
                    args_key

                p = multiprocessing.Process(
                        target=worker, args=(work_args, ))
                p.name = args_key
                jobs.append(p)
                p.start()

    for p in jobs:
        args_key = p.name
        p.join()

        outfile = filename_out_pre + args_key
        tofile = filename_to_pre + args_key

        # Move from scratch to local
        shutil.move(outfile, tofile)

        print(args_key, 'joined!')
        print()


if __name__ == "__main__":
    np.random.seed()
    np.seterr(divide='ignore')

    main()

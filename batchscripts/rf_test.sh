#!/bin/bash
#SBATCH --job-name=RFTEST
#SBATCH --output=%x-%j.out
#SBATCH -N 1
#SBATCH --tasks-per-node 16
#SBATCH -p short
#SBATCH --time=00:05:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: RFTEST"

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

case $HOSTNAME in
  (antergos-sopropo) work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl) echo "should not run on ${HOSTNAME} without sbatch, exit"; exit 1;;
  # (*) module load 2019; module unload Python; work_dir="$HOME/thesis_rainfall";;
  (*) module load 2019; module unload Python; module load Python/3.6.6-intel-2018b; work_dir="$HOME/thesis_rainfall";;
  # (*) module load 2019; module unload Python; module load Python/3.7.5-foss-2018b; work_dir="$HOME/thesis_rainfall";;  # seems slower
esac
echo `which python3`

# params="--nsims 20 --L 800 --CMC 10 --FO 5 2 --FE 10 --FNS 10 --test"
# params="--nsims 40 --nlevels 10 --L 800 --CMC 100 --FE 100 --test"
# params="--nsims 40 --nlevels 10 --L 800 --FNS 100 --FO 50 2 --test"
# params="--nsims 40 --nlevels 10 --L 800 --FO 50 2 --test"
# params="--nsims 40 --nlevels 10 --L 800 --FE 100 --test"

# params="--nsims 20 --nlevels 10 --L 800 --CMC 20 --FE 20 --FNS 20 --test"
params="--nsims 10 --nlevels 3 --L 1000 --FNS 20 --outfolder out/rf/ --test"

time python3 ${work_dir}/rainfall_model_run.py      --datetime "$dt" $params && \
time python3 ${work_dir}/rainfall_model_validate.py --datetime "$dt" $params

echo end of job

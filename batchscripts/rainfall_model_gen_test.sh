#!/bin/bash
#SBATCH --job-name=RFGENTEST
#SBATCH --output=%x-%j.out
#SBATCH -N 1
#SBATCH --tasks-per-node 15
#SBATCH -p short
#SBATCH --time=00:05:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: RFGENTEST"

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

# outfolder="${TMPDIR}/multi_splitting_${dt}/out/rf/test"
# outfolder="${TMPDIR}"
# tofolder="${HOME}/out/rf/test"
# mkdir -p "${outfolder}"
# mkdir -p "${tofolder}"

case $HOSTNAME in
  (antergos-sopropo) work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl) echo "should not run on ${HOSTNAME} without sbatch, exit"; exit 1;;
  (*) module load 2019; module unload Python; module load Python/3.6.6-intel-2018b; work_dir="$HOME/thesis_rainfall";;
esac
echo `which python3`

# params="--nsims 20 --FNS 30 --test --gauss_quad_deg 5 --gpc_deg 5"
# params="--nsims 2 --FE 5 --gauss_quad_deg 3 --gpc_deg 3 --test"
# time python3 "${work_dir}/rainfall_model_uq_gendata_nonparallel.py" --datetime "$dt" $params


# params="--nsims 1 --FNS 100"
# time python3 "${work_dir}/rainfall_model_uq_gendata.py" --datetime "$dt" $params

# time python3 "${work_dir}/rainfall_model_validate.py" $params  # Do this manually afterwards!

gg_deg=5
# method='FNS'
method='FE'
methodparam=5
# method='FO'
# methodparam="10 3"

for i in `seq $nsims`; do
  for ((node_index=0; node_index < gg_deg; node_index++)); do
    params="--gauss_quad_deg ${gg_deg} --node_index ${node_index} --${method} ${methodparam}";
    params="--gauss_quad_deg ${gg_deg} --node_index ${node_index} --${method} ${methodparam} --test";
    # params="--gauss_quad_deg ${gg_deg} --node_index ${node_index} --${method} ${methodparam} --test --local";

    python3 "${work_dir}/rainfall_model_uq_gendata_one.py" --datetime "$dt" $params &

    while (( `jobs | wc -l` >= $lim )); do
      sleep 5
    done

    # outfile="${FILEN}_${method}_${FNS}_${node_index}_${gg_deg}_${dt}"
    # python3 "${work_dir}/rainfall_model_uq_gendata_one.py" --datetime "$dt" $params > ~/$outfile 2>&1 &
    # python3 "${work_dir}/rainfall_model_uq_gendata_one.py" --datetime "$dt" $params > "${outfolder}/${outfile}" 2>&1 &
  done
done


wait

# Already do this in Python
# cp "$TMPDIR"/multi_splitting/out/rf/* "$HOME"/out/rf/ -v
# cp "${TMPDIR}"/multi_splitting_${dt}/out/rf/test/* "${HOME}"/out/rf/test/ -v
# cp "${outfolder}/*" "${tofolder}" -v

# time python3 "${work_dir}/rainfall_model_validate.py" $params  # Do this manually afterwards!

echo end of job

#!/bin/bash
#SBATCH --job-name=BMCMC
#SBATCH --output=%x-%j.out
#SBATCH -N 1
#SBATCH --time=72:00:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: BMCMC"

mkdir -p "$TMPDIR"/multi_splitting/out/bm_cmc

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

case $HOSTNAME in
  (antergos-sopropo) pyth="python"; work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl) echo "should not run on ${HOSTNAME} without sbatch, exit"; exit 1;;
  (*) pyth="$HOME/.virtualenvs/math/bin/python"; work_dir="$HOME/thesis_rainfall";;  # py3 and latest versions of numpy, scipy
esac

# time "${pyth}" "${work_dir}/bm_cmc.py"

# time ~/.virtualenvs/math/bin/python ~/thesis_rainfall/bm_cmc.py

time python3 "${work_dir}/bm_cmc.py"

cp "$TMPDIR"/multi_splitting/out/* "$HOME"/out/ -rv

echo end of job

### Lanczos
    if lanczos > 0:
        def gpc_approx_fn(z):
            return (fourier_coefs * np.sinc((2*np.arange(gauss_quad_deg+1) + 1) * np.pi / (2*gauss_quad_deg))) @ orthopolval(affine_fn_inv(z))
    else:
        def gpc_approx_fn(z):
            return fourier_coefs @ orthopolval(affine_fn_inv(z))




    ## relative error plot

    fig_re, ax_re = plt.subplots()

    figname = 'filter_re'

    ax_re.plot(drift_space,
                 (true_model_drift_space - gpc_approx_drift_space) / true_model_drift_space,
                 'r--', linewidth=.75, label='gPC log RE')
    ax_re.plot(drift_space,
                 (true_model_drift_space - gpc_approx_raised_cosine_drift_space) / true_model_drift_space,
                 'g--', linewidth=.75, label='Raised cosine gPC log RE')
    ax_re.plot(drift_space,
                 (true_model_drift_space - gpc_approx_lanczos_drift_space) / true_model_drift_space,
                 'b--', linewidth=.75, label='Lanczos filtered gPC log RE')

    xlabel = '$\\alpha$'
    ylabel = '$\\gamma_{{\\scriptscriptstyle T, L}}' \
        '(\\alpha); T = {}, L = {}$'.format(T, L)
    set_axis_labels_endaxes(xlabel, ylabel, ax_re)

    ax_re.legend()
    ax_re.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    ax_re.set_ylim([-10, 10])

    fig_re.savefig(
        './figs/fit_alpha_gamma_gpc_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf',
        #  bbox_inches='tight',
        )

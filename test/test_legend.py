import matplotlib.pyplot as plt
import numpy as np

### https://riptutorial.com/matplotlib/example/10473/single-legend-shared-across-multiple-subplots
### https://stackoverflow.com/a/10154763
### https://matplotlib.org/3.1.3/tutorials/intermediate/constrainedlayout_guide.html#legends

fig, axs = plt.subplots(1, 2, constrained_layout=True)
axs[0].plot(np.arange(10), label='First plot')
axs[1].plot(np.arange(10), label='Second plot')

handles = []
labels = []

for ax in axs.flatten():
    h, l = ax.get_legend_handles_labels()
    handles.extend(h)
    labels.extend(l)

fig.legend(
    #  *axs[1].get_legend_handles_labels(),
    handles, labels,
    loc='upper center',
    bbox_to_anchor=(0.5, 0),
    bbox_transform=fig.transFigure
    )

fig.savefig('figs/testlegend.png',
            bbox_inches='tight',
            #  dpi=100
            )

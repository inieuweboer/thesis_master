#!/bin/bash
#SBATCH --job-name=RFGENAMTS
#SBATCH --output=%x-%j.out
#SBATCH -N 1
#SBATCH --tasks-per-node 15
#
#SBATCH --time=48:00:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: RFGENAMTS"

#Determining the number of processors in the system
NPROC=`nproc --all`
let lim=$NPROC-1

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

case $HOSTNAME in
  (antergos-sopropo) work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl) echo "should not run on ${HOSTNAME} without sbatch, exit"; exit 1;;
  (*) module load 2019; module unload Python; module load Python/3.6.6-intel-2018b; work_dir="$HOME/thesis_rainfall";;
esac
echo `which python3`

gg_deg=8

nloops=100
nsimsperrun=100000
outfolder='out/rf/ucov_amts/'

for i in `seq $nloops`; do
  for ((node_index=0; node_index < gg_deg; node_index++)); do
    params="--nsims ${nsimsperrun} --outfolder ${outfolder} --gauss_quad_deg ${gg_deg} --node_index ${node_index}";

    python3 ${work_dir}/rainfall_model_uq_genamts.py $params &

    while (( `jobs | wc -l` >= $lim )); do
      sleep 5
    done
  done
done

wait

echo end of job

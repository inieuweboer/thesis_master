import numpy as np
from scipy import stats

from helper_functions import calc_chars


from tabulate import tabulate


def exact_prob_normal(sigma, level):
    return stats.norm.cdf(-level / sigma)


def cmc_normal(sigma, level, N):
    hits = (np.random.normal(0, sigma, size=N) >= level).astype(int)

    return hits


def importance_sampler_normal(sigma, level, N):
    C = level / (2 * sigma * sigma)

    samples = np.random.normal(level, sigma, size=N)
    hits = (samples >= level)*np.exp(C*(level - 2*samples))

    return hits


def importance_sampler_adv_normal(sigma, level, N, delta=1):
    C = 1 / (2 * delta * delta * sigma * sigma)
    D = delta*np.exp(level*level*C)
    d = 1 - delta*delta

    samples = np.random.normal(level, delta * sigma, size=N)
    hits = (samples >= level)*D*np.exp(C*samples*(d*samples - 2*level))

    return hits


if __name__ == "__main__":
    import time
    import datetime

    now = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    np.random.seed(42)

    sigma = 2
    level = 6
    #  N = 10**5

    exps = [3, 5, 7]

    #  print(importance_sampler_adv_normal(sigma, level, N, delta=0.825))

    filename_out = './out/importance_normal_' + now
    with open(filename_out, 'a+') as fp_out:
        print('Exact probability:', file=fp_out)
        exact_val = exact_prob_normal(sigma, level)
        print(exact_val, file=fp_out)
        print(file=fp_out)

        #  first_col = ['Estimate', 'Process time', 'Sample var. of mean', 'Sample RE of mean', 'PTN-svar', 'PTN-sRE', 'CI length', 'CI left', 'CI right', 'Bias']
        first_col = ['', '', 'Estimate', 'Bias', 'Process time', 'Sample var. of mean', 'Sample RE of mean', 'PTN-svar', 'PTN-sRE']
        first_rows = [['CMC', 'IS' ] * len(exps), np.array([('10^' + str(i), '') for i in exps]).flatten()]
        #  first_rows = [np.array([(10**i, '') for i in range(low, high)]).flatten()]
        table = []

        for i in exps:
            N = 10**i

            t = time.process_time()
            hits = cmc_normal(sigma, level, N)
            proc_time = time.process_time() - t

            ests_cmc = calc_chars(hits, proc_time)
            bias_cmc = np.abs(exact_val - ests_cmc[0])
            ests_cmc = np.insert(ests_cmc, 1, bias_cmc)

            #  row = ['CMC', '10^' + str(i)]
            #  row.extend(ests_cmc)
            table.append(ests_cmc)

            t = time.process_time()
            hits = importance_sampler_normal(sigma, level, N)
            proc_time = time.process_time() - t

            ests_is = calc_chars(hits, proc_time)
            bias_is = np.abs(exact_val - ests_is[0])
            ests_is = np.insert(ests_is, 1, bias_is)

            #  row = ['IS', '']
            #  row.extend(ests_is)
            table.append(ests_is)
            #  table.append(np.array([ests_cmc, ests_is], dtype=object))

        # Workaround! Cannot format floats when having a full array of strings or transposing
        print(tabulate([first_col], tablefmt="latex"), file=fp_out)
        print(tabulate(first_rows, tablefmt="latex"), file=fp_out)
        print(tabulate(np.transpose(table), tablefmt="latex", floatfmt=".4g"), file=fp_out)


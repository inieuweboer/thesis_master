#!/bin/bash
#SBATCH --job-name=UQRFFIG
#SBATCH --output=%x-%j.out
#SBATCH -N 1
#SBATCH --tasks-per-node 15
#SBATCH --time=0:10:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: UQRFFIG"

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

case $HOSTNAME in
  (antergos-sopropo) work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl) echo "should not run on ${HOSTNAME} without sbatch, exit"; exit 1;;
  (*) module load 2019; module unload Python; module load Python/3.6.6-intel-2018b; work_dir="$HOME/thesis_rainfall";;
esac
echo `which python3`

gq_deg=8
gpc_deg=4
outfolder="$HOME/out/rf/ucov_amts"

params="--outfolder ${outfolder} --gauss_quad_deg ${gq_deg} --gpc_deg ${gpc_deg}";

# python "${work_dir}/UQ_rf_amts.py" $params --Lmin 1000 --Lmax 1200 &
# python "${work_dir}/UQ_rf_amts.py" $params --Lmin 1100 --Lmax 1200 &
# python "${work_dir}/UQ_rf_amts.py" $params --Lmin 1200 --Lmax 1300 &
# python "${work_dir}/UQ_rf_amts.py" $params --Lmin 1150 --Lmax 1250 &
# python "${work_dir}/UQ_rf_amts.py" $params --Lmin 1160 --Lmax 1240 &
# python "${work_dir}/UQ_rf_amts.py" $params --Lmin 1175 --Lmax 1225 &
time python "${work_dir}/UQ_rf_amts.py" $params --Lmin 1000 1180 --Lmax 1200 1220 &

wait

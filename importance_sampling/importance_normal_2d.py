def cmc_normal_2d(cov, a, N):
    tot = 0
    for _ in range(N):
        sample = np.random.multivariate_normal(np.zeros(2), cov)

        if sample[0] >= a and sample[1] >= a:
            tot += 1

    return tot / N


def importance_sampler_normal_2d(cov, a, N):
    aa = np.full(shape=2, fill_value=a)
    precision = cov.I

    tot = 0
    for _ in range(N):
        sample = np.random.multivariate_normal(aa, cov)
        modifier = np.asscalar(np.exp((aa/2 - sample) @ precision @ aa.T))

        if sample[0] >= a and sample[1] >= a:
            # eq 9.3 in
            # https://statweb.stanford.edu/~owen/mc/Ch-var-is.pdf
            tot += modifier

    return tot / N


def importance_sampler_adv_normal_2d(cov, a, N, delta=1):
    # FIXME werkt niet helemaal lijkt het, maar ik snap wat ik moet snappen
    aa = np.full(shape=2, fill_value=a)
    precision = cov.I

    tot = 0
    for _ in range(N):
        sample = np.random.multivariate_normal(aa, delta*cov)
        modifier = np.asscalar(np.exp((1/delta - 1) * \
                sample @ precision @ sample.T/2 + \
                1/delta * (-sample + aa/2) @ precision @ aa.T))

        if sample[0] >= a and sample[1] >= a:
            # eq 9.3 in
            # https://statweb.stanford.edu/~owen/mc/Ch-var-is.pdf
            tot += modifier

    return tot / N


if __name__ == "__main__":
    np.random.seed(42)

    cov = np.matrix([[4, -1], [-1, 4]])
    level = 1

    print(cmc_normal_2d(cov, level, N))
    print(importance_sampler_normal_2d(cov, level, N))

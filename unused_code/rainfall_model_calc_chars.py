import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

from helper_functions import mvec
from text_render_latex import set_size


def calc(N=10**7, filename='figs/cmc_hist_new', size=452.96796):
    filename += '_' + str(N)

    with open('out/rainfall_cmc_data', 'r') as fp:
        data = np.array([float(next(fp).strip()) for _ in range(N)])

    width = 452.96796

    plt.figure(figsize=set_size(width))

    ### Do stuff here

    print('N = ', N)

    # First moment/mean, variance, relative error
    print('mvec = ', mvec(data))

    # Skewness
    print('skewness = ', stats.skew(data))

    # Excess kurtosis
    print('excess kurtosis = ', stats.kurtosis(data, fisher=True))

    # probability of exceeding rmax
    for rmax in [1000, 1100, 1200]:
        print('prob of exceeding ', rmax, ' = ', np.sum(data > rmax) / N)



    # Save and remove excess whitespace
    #  plt.savefig(filename + '.pdf', format='pdf', bbox_inches='tight')




if __name__ == "__main__":
    for i in range(4, 8):
        calc(10**i)

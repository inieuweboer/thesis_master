import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

from text_render_latex import *
latexify(columns=2)


def optimize_levels(probs, levels, plot=False):
    """
    Optimization of levels as described in Wadman (2015) paragraph 6.3.1,
    originally Amrein & Kunsch 2011
    """
    popt = 0.2032
    log_popt = np.log(popt)
    new_nlevels = 6  # 20

    cumul_probs = np.flip(np.cumprod(np.flip(probs)))
    log_cumul_probs = np.log(cumul_probs)

    log_popts = [k*log_popt for k in range(1, new_nlevels+1)]
    popts = np.exp(log_popts)

    new_levels = np.interp(log_popts, log_cumul_probs, levels)

    if plot:
        fig1, ax = plt.subplots()
        fig1.set_figheight(fig1.get_figheight() * figfactor)
        fig1.set_figwidth(fig1.get_figwidth() * figfactor)

        ax.plot(levels, cumul_probs, color='k', lw=0.8)

        ax.scatter(new_levels, popts, color='r', s=10)
        ax.scatter(levels, cumul_probs, color='k', s=3)

        ax.hlines(popts, 0, new_levels, linestyle="dashed", linewidth=0.5)
        ax.vlines(new_levels, 0, popts, linestyle="dashed", linewidth=0.5)

        ax.set_yscale('log')
        ax.set_xlabel('Levels')
        ax.set_ylabel('Cumulative probabilities')

        ax.annotate('$p_{{\\text{{opt}}}}$',
                    (new_levels[0], popts[0]),
                    xytext=(-25, 5), textcoords='offset points',
                    **{'fontsize': 'xx-small'})

        for i in range(1, new_nlevels):
            ax.annotate('$p_{{\\text{{opt}}}}^{}$'.format(i+1),
                        (new_levels[i], popts[i]),
                        xytext=(-25, 5), textcoords='offset points',
                        **{'fontsize': 'xx-small'},
                        )

        fig1.savefig('figs/levels.pdf', format='pdf')

    return np.unique(np.append(new_levels, 1.))


if __name__ == "__main__":
    np.random.seed()

    print(optimize_levels(np.arange(1, 11, dtype=float) / 10, np.linspace(0.1, 1, 10), plot=True))

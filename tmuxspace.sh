#!/bin/sh
tmux split-window -hb # p 45
tmux send-keys "cd ~/Desktop/studie/thesis" C-m
tmux send-keys C-l
tmux select-pane -t 1
tmux split-window -vb # p 45
tmux send-keys "cd ~/Desktop/studie/thesis" C-m
tmux send-keys "conda activate base" C-m
tmux send-keys C-l
tmux select-pane -t 2
tmux send-keys "cd ~/Desktop/studie/thesis" C-m
tmux send-keys "conda activate base" C-m
tmux send-keys "ipython" C-m
tmux send-keys "%load_ext autoreload" C-m
tmux send-keys "%autoreload 2" C-m
tmux send-keys "import numpy as np; import matplotlib.pyplot as plt; import scipy as sp; import orthopy; from tabulate import tabulate; import datetime" C-m
tmux send-keys "from bm_splitting import exact_prob_bm, Phi" C-m
tmux send-keys "from UQ_bm_drift_rm import exact_prob_bm_rm" C-m
# tmux send-keys C-l
tmux select-pane -t 1
tmux split-window -v
tmux send-keys "cd ~/Desktop/studie/thesis" C-m
# tmux send-keys "ssh lisa" C-m

tmux select-pane -t 0
tmux send-keys "sshfs lisa: /home/inieuweboer/Desktop/studie/thesis/server" C-m
tmux resize-pane -R 20

# tmux select-pane -t 1
# tmux send-keys "cd ./server" C-m
# tmux resize-pane -L 20
# tmux send-keys "vim server/thesis_climate/" C-m

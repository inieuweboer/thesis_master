# indices = np.array([(i, j, k) for i in range(11) for j in range(11) for k in range(11) if i + j == k or abs(i - j) == k])
# T[indices[:, 0], indices[:, 1], indices[:, 2]]

#     cmc_uniform_skewness_test = stats.moment((cmc_samples - cmc_uniform_mean) / cmc_uniform_std, 3)


#     fcs_temp = fcs[1:]

#     return (fcs_temp @ (fcs_temp @ (
#             fcs_temp @ three_tensor[1:, 1:, 1:]
#             ))) * std_power_minus_three

#     fcs_temp =  fcs[1:]

#     return (fcs_temp @ (fcs_temp @ (fcs_temp @ (
#             fcs_temp @ four_tensor[1:, 1:, 1:, 1:]
#             )))) * std_power_minus_four




def make_four_tensor_legendre_bak(K):
    def orthopolval(u, deg):
        return np.polynomial.legendre.legval(u, np.eye(deg)[-1])

    norms_sq = 1 / (2 * np.arange(K+1) + 1)

    return make_four_tensor(orthopolval=orthopolval, K=K, norms_sq=norms_sq, wsum=2, a=-1, b=1)


#### Both slower
def make_four_tensor_from_three(
        orthopolval=None, K=None,
        norms_sq=None, three_tensor=None,
        wsum=2, a=-1, b=1):

    if orthopolval is None and K is None:
        assert three_tensor is not None and norms_sq is not None

    D = 2*K

    if norms_sq is None:
        norms_sq = np.append(1, np.array([quad(lambda u: orthopolval(
            u, i+1)**2, a, b)[0] / wsum for i in range(1, D+1)]))

    if three_tensor is None:
        three_tensor = make_three_tensor(orthopolval, D, wsum, a, b)

    assert three_tensor.shape[0] == norms_sq.size

    four_tensor = contract(
        'nij,nkl->ijkl', three_tensor / norms_sq[:, np.newaxis, np.newaxis], three_tensor)

    return four_tensor[:K+1, :K+1, :K+1, :K+1]


def make_four_tensor_loopedbak(
        orthopolval=None, K=None,
        norms_sq=None, three_tensor=None,
        wsum=2, a=-1, b=1):

    if orthopolval is None and K is None:
        assert three_tensor is not None and norms_sq is not None

    D = 2*K

    if norms_sq is None:
        norms_sq = np.append(1, np.array([quad(lambda u: orthopolval(
            u, i+1)**2, a, b)[0] / wsum for i in range(1, D+1)]))

    if three_tensor is None:
        three_tensor = make_three_tensor(orthopolval, D, wsum, a, b)

    assert three_tensor.shape[0] == norms_sq.size

    indices = [(i, j, k, l) for i in range(K+1) for j in range(i+1)
               for k in range(j+1) for l in range(k+1)
               if j + k >= abs(i - l) and i + l >= abs(j - k)
               ]
    four_tensor = np.zeros((K+1, K+1, K+1, K+1))

    for i, j, k, l in indices:
        out = (three_tensor / norms_sq[:, np.newaxis, np.newaxis])[:i+j+1, i, j] @ three_tensor[:i+j+1, k, l]

        for perm in set(permutations((i, j, k, l))):
            four_tensor[perm] = out

    assert np.allclose(four_tensor, four_tensor.T)

    return four_tensor


    #  quad_out_arr = np.array([quad(
    #      lambda u: orthopolval(u, i) * orthopolval(u, j) * \
    #      orthopolval(u, k) * orthopolval(u, l),
    #      a, b)[0] for i, j, k, l in indices]) / wsum

    #  quad_out_arr = quad_vect(
    #      lambda u: np.array([orthopolval(u, i) * orthopolval(u, j) * \
    #                  orthopolval(u, k) * orthopolval(u, l) for i, j, k, l in indices]),
    #      a, b)[0] / wsum
    #  quad_out_arr = quad_vect(
    #      lambda u: np.array([orthopolval(u, i) * orthopolval(u, j) * \
    #                  orthopolval(u, k) * orthopolval(u, l) for i, j, k, l in indices]),
    #      a=0, b=1)[0]

import matplotlib.pyplot as plt
import numpy as np
from scipy.special import erf

from itertools import cycle

from importance_functions import (
    plot_levels,
    make_importance_fn_brownian_motion,
    make_importance_fn_distance,
    make_importance_fn_large_deviation,
    )
from scipy.special import erf, expit, logit
from optimize_levels import optimize_levels


def Phi(z):
    return (1 + erf(z / np.sqrt(2))) / 2


def exact_prob_bm_rm(L, T, drift):
    r"""
    $\xprob{ M_T^{(\alpha)} > L}$ where
    $M_T^{(\\alpha)} := \sup_{0 \le t \le T} X_t$ and
    $X_t := W_t + \alpha t$

    https://math.stackexchange.com/questions/1053294/density-of-first-hitting-time-of-brownian-motion-with-drift
    """
    return Phi((drift*T - L) / np.sqrt(T)) + \
        np.exp(2*L*drift) * Phi(-(L + drift*T) / np.sqrt(T))


def bm_drift_splitting_fixed_successes(L, T, drift,
                                 alg_params=(None, 4, 40, None),
                                 optimize=False, show=False, save=False,
                                 ):
    """
    nlevels is no of levels
    success is no of succeeded particles per level required
    """
    #  stepsize, nlevels, success, max_bootstraps, importance_fn = alg_params
    stepsize, nlevels, success, importance_fn = alg_params

    # Step size semi-agnostic of threshold L and amount of levels. Dividing
    # by 100 means a standard deviation factor of 10 between two levels,
    # which makes the probability of multiple level-skipping quite low
    if stepsize is None:
        #  stepsize = 0.01
        stepsize = L / ((nlevels + 1) * 100)

    if importance_fn is None:
        importance_fn = make_importance_fn_distance(T, L)

    # Breadth-first algorithm
    def breadth_first(plot=True):
        # Start dry, total pos, entry time;
        particles = [(0, 0)]

        for lvl_index in range(nlevels):
            survived_particles = []

            # Iterator for generating random cycled picks
            it = cycle(np.random.permutation(len(particles)))

            # Go on until success level reached or max bootstraps reached
            #  for _ in range(max_bootstraps):
            #      if len(survived_particles) == success:
            #          break
            while len(survived_particles) < success:

                #  index = np.random.randint(len(particles))
                index = next(it)

                pos, entry_time = particles[index]
                trajectory = [pos]

                # Skip current particle if entry time after end time
                if entry_time >= T:
                    continue

                particle_amounts[lvl_index] += 1

                # Pre-sample normally distributed steps
                nsteps = int((T - entry_time) / stepsize) + 1
                samples = iter(np.random.normal(scale=np.sqrt(stepsize),
                                                size=nsteps) + drift*stepsize)

                split_color = 'red'

                for t in np.arange(entry_time, T, stepsize):
                    pos += next(samples)
                    trajectory.append(pos)

                    # Save current particle if survived
                    if importance_fn(t, pos) >= levels[lvl_index]:
                        survival_amounts[lvl_index] += 1

                        # Add at next time step
                        survived_particles.append((pos, t + stepsize))

                        split_color = 'green'

                        break

                if plot and (show or save):
                    # Note that particle trajectories overlap!
                    plt.plot(
                        np.linspace(
                            entry_time,
                            entry_time + (len(trajectory) - 1)*stepsize,
                            len(trajectory)),
                        trajectory,
                        #  color=(0, 0, lvl_index/nlevels),
                        color=((nlevels - lvl_index - 1)*2/(nlevels*3),
                               (nlevels - lvl_index - 1)*2/(nlevels*3),
                               (nlevels - lvl_index - 1)*2/(nlevels*3)),
                        linewidth=.125)

                    # Plot endpoint of trajectory
                    if len(trajectory) > 1:
                        plt.plot(entry_time + (len(trajectory) - 1)*stepsize,
                                 trajectory[-1],
                                 color=split_color, marker='.')

            particles = survived_particles

    # Set standard equidistant levels
    levels = [lvl_index/nlevels for lvl_index in range(1, nlevels+1)]

    if optimize:
        # Set survival amounts to zero and do a test run
        survival_amounts = np.zeros(nlevels, dtype=int)
        particle_amounts = np.zeros(nlevels, dtype=int)
        breadth_first(False)

        # Re-run with optimized levels
        probs = np.array(particle_amounts, dtype=float)
        probs /= particle_amounts
        levels = optimize_levels(probs, levels)
        nlevels = len(levels)

    survival_amounts = np.zeros(nlevels, dtype=int)
    particle_amounts = np.zeros(nlevels, dtype=int)
    breadth_first()

    # Plot the levels
    if show:
        plot_levels(T, L, importance_fn, levels)
        plt.show()
    elif save:
        plot_levels(T, L, importance_fn, levels)

    probs = np.array(survival_amounts, dtype=float)
    probs /= particle_amounts

    # eq 5.14 in Wadman
    sre_bound = -1 + np.prod(1 / (survival_amounts - 2) + 1)
    est = np.prod(probs)
    var_bound = sre_bound * est * est

    return est, var_bound, np.sqrt(sre_bound)


if __name__ == "__main__":
    import time

    np.random.seed()
    np.seterr(divide='ignore')

    L = 5
    T = 30

    drift = 0.5

    print('L = ', L)
    print('T = ', T)
    print('drift = ', drift)

    print(exact_prob_bm_rm(L, T, drift))

    stepsize = 0.01
    nruns = np.power(10, 3)

    show = True

    nlevels = 3
    success = 7
    #  start_particles = effort = 5
    #  mult = 2
    #  max_bootstraps = 1000

    #  print('non-optimized')
    print(bm_drift_splitting_fixed_successes(
        L=L, T=T, drift=drift,
        alg_params=(stepsize, nlevels,
                    success,
                    make_importance_fn_brownian_motion(T, L)
                    ),
        #  alg_params=(stepsize, nlevels,
        #              success, None),
        show=show,
        ))
    #  print('optimized')
    #  print(bm_splitting_fixed_successes(L=L, T=T,
    #          show=True,
    #          optimize=True,
    #          ))

    print()

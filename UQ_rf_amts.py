import matplotlib.pyplot as plt
import numpy as np

from helper_functions import (
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    rfparams_alpha, rfparams_ucov,
    )
from rainfall_model_cumulative_splitting import *
from UQ_methods import *

from text_render_latex import *
latexify(columns=2)


def gpc_rf_prob_fit():
    # Get args
    args = rf_argparser()

    (T, rfparams_def,
     M, K,
     a, b,
     Lmin, Lmax, sp_mult,
     outfolder,
     ) = (
          args.T, args.rfparams,
          args.gauss_quad_deg, args.gpc_deg,
          args.a, args.b,
          args.Lmin, args.Lmax, args.sp_mult,
          args.outfolder,
          )

    Lminmin = np.min(Lmin)
    Lmaxmax = np.max(Lmax)
    Lspace = np.linspace(Lminmin, Lmaxmax, int((Lmaxmax - Lminmin)*sp_mult) + 1)

    filename_merged_pre = "{}merged_".format(outfolder)

    work_nodes = gpc_approx_discrete_proj_uniform_tree_nodes(
        a=a, b=b,
        gauss_quad_deg=M,
        )

    model_fn_arr = []

    for node_index in range(M):
        rfparams = rfparams_ucov(rfparams_def, work_nodes, node_index)

        args_key = args_str_maker(
            T=T, rfparamshash=rf_params_tostring(rfparams),
            )

        mergedfile = filename_merged_pre + args_key

        with open(mergedfile, 'r') as fp_merged:
            print(mergedfile)
            ests = np.loadtxt(fp_merged)

        # Here the tail probabilities are calculated
        model_fn_arr.append([
            np.mean(ests > L) for L in Lspace
            ])

    #  model_fn_arr = np.array(model_fn_arr)
    model_fn_arr = np.array(model_fn_arr).T

    fcs, norms_sq = gpc_approx_discrete_proj_param_uniform_tree_from_data(
        model_fn_arr,
        a=a, b=b,
        gauss_quad_deg=M, gpc_deg=K,
        )

    means = fcs[:, 0]
    stds = np.sqrt(np.sum(
        fcs[:, 1:]**2 * norms_sq[1:], axis=1))

    #####
    # Plot true and approx using both naive gPC and func gPC approx
    fig_L, ax_L = plt.subplots(nrows=2, ncols=2)
    #  fig_L.set_figheight(fig_L.get_figheight() / 2)

    a0 = np.where(Lspace == Lmin[0])[0][0]
    b0 = np.where(Lspace == Lmax[0])[0][0]
    a1 = np.where(Lspace == Lmin[1])[0][0]
    b1 = np.where(Lspace == Lmax[1])[0][0]

    ax_L[0, 0].plot(Lspace[a0:b0], means[a0:b0],
                    linewidth=.5,
                    label='Mean')
    ax_L[0, 0].fill_between(Lspace[a0:b0],
                            means[a0:b0] - stds[a0:b0],
                            means[a0:b0] + stds[a0:b0],
                            alpha=0.5, label='Std')

    ax_L[0, 1].plot(Lspace[a1:b1], means[a1:b1],
                    linewidth=.5,
                    label='Mean')
    ax_L[0, 1].fill_between(Lspace[a1:b1],
                            means[a1:b1] - stds[a1:b1],
                            means[a1:b1] + stds[a1:b1],
                            alpha=0.5, label='Std')

    ax_L[1, 0].plot(Lspace[a0:b0], means[a0:b0],
                    linewidth=.5,
                    label='Mean')
    ax_L[1, 1].plot(Lspace[a1:b1], means[a1:b1],
                    linewidth=.5,
                    label='Mean')

    rv = '\\gamma_{{\\scriptscriptstyle L}}(\\kappa)'
    xlabel = '$L$'
    ylabel = '${} \\pm {}$'.format(xe(rv), xstd(rv))
    ax_L[0, 0].legend()
    ax_L[0, 0].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    ax_L[0, 1].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    set_axis_labels_endaxes('', ylabel, ax_L[0, 0])
    set_axis_labels_endaxes(xlabel, '', ax_L[0, 1])

    ylabel = '${}$'.format(xe(rv))
    ax_L[1, 0].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    ax_L[1, 1].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    ax_L[1, 0].set_yscale('log')
    ax_L[1, 1].set_yscale('log')
    set_axis_labels_endaxes('', ylabel, ax_L[1, 0])
    set_axis_labels_endaxes(xlabel, '', ax_L[1, 1])

    figname = 'multiple_log'

    fig_L.savefig(
        'figs/UQ_rf_Ucov_rmax_gpc_{}_L_{}-{}_{}_T_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         Lmin, Lmax,
                                         sp_mult,
                                         T, K, M, a, b),
        format='pdf', transparent=True,
        )


if __name__ == "__main__":
    gpc_rf_prob_fit()

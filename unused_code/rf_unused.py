#  Test function to generate occurence process
def gen_occurrences():
    # Start dry
    X = []
    X.append(np.zeros(nstations, dtype=int))

    for i in range(ndays):
        U = stats.norm.cdf(np.random.multivariate_normal(mean, Ucov))

        prob_crit = precip_params[np.arange(nstations), X[-1]]  # eq 4
        new_weather_vec = (U <= prob_crit).astype(int)  # eq 5
        X.append(new_weather_vec)

    return X


# Test function to generate amount for a certain rmin and beta
def gen_amounts(rmin, beta):
    V = stats.norm.cdf(np.random.multivariate_normal(mean, Vcov))

    return rmin - beta*np.log(V)  # eq 8



def test_markov_chain():
    # Test the Markov chain and print the precitipation amounts
    XR = gen_occurrences_amounts(params=(15, 0.2, 2, 3))
    for X, Y in XY[1:]:
        print(X)
        print(Y)
        print(X*Y)
        print()


def find_level_cumulative_splitting(ndays, params, target_prob=np.power(0.1, 2),
                                    level_low_init=0, level_high_init=1200, tol=np.power(0.1, 2)):
    def get_prob(level):
        return cumulative_rainfall_splitting_fixed_successes(params,
                                                             rmax=level,
                                                             ndays=ndays,
                                                             show=False)

    # probability to exceed 0 is 1 almost surely
    prob_lower = 1
    prob_higher = get_prob(level_high_init)

    level_low = level_low_init
    level_high = level_high_init

    while level_high - level_low > tol:
        new_level = (level_low + level_high) / 2
        new_prob = get_prob(new_level)

        if target_prob > new_prob:
            prob_lower = new_prob
            level_low = new_level
        else:
            prob_higher = new_prob
            level_high = new_level

        print(level_low, level_high, new_prob)

    return (level_low + level_high) / 2



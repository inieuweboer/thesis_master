import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

figfactor = 3/4
loosely_dotted = (0, (1, 10))

axes_labelsize = 11
tick_labelsize = 9
#  axes_labelsize = 20
#  tick_labelsize = 16
#  axes_labelsize = 3*11
#  tick_labelsize = 3*9

tau = 2*np.pi


# https://nipunbatra.github.io/blog/2014/latexify.html
# https://web.archive.org/web/20190917140552/https://nipunbatra.github.io/blog/2014/latexify.html
def latexify(fig_width=None, fig_height=None, columns=1):
    """Set up matplotlib's RC params for LaTeX plotting.
    Call this before plotting a figure.

    Parameters
    ----------
    fig_width : float, optional, inches
    fig_height : float,  optional, inches
    columns : {1, 2}
    """

    # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

    # Width and max height in inches for IEEE journals taken from
    # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

    assert(columns in [1,2])

    if fig_width is None:
        if columns == 1:
            #  fig_width = 3.39
            fig_width = 2.84393
        else:
            #  fig_width = 6.2677
            # inches, text/linewidth without fullpage
            fig_width = 5.78853

            #  fig_width = 6.9  # (old) width in inches

    if fig_height is None:
        golden_mean = (np.sqrt(5)-1.0)/2.0    # Aesthetic ratio
        fig_height = fig_width*golden_mean # height in inches

    MAX_HEIGHT_INCHES = 8.0
    if fig_height > MAX_HEIGHT_INCHES:
        print("WARNING: fig_height too large:" + fig_height +
              "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
        fig_height = MAX_HEIGHT_INCHES

    plt.style.use('seaborn-paper')
    #  plt.style.use('seaborn-ticks')
    params = {
              #  'backend': 'ps',
              'text.usetex': True,
              'text.latex.preamble': [
                                      '\\usepackage{gensymb}',
                                      '\\usepackage{amsmath}',
                                      '\\usepackage{amsfonts}',
                                      ],
              'font.family': 'serif',
              'font.size': 11,
              'axes.labelsize': axes_labelsize, # fontsize for x and y labels (was 10)
              'axes.titlesize': 11,
              'legend.fontsize': 7,
              'xtick.labelsize': tick_labelsize,
              'ytick.labelsize': tick_labelsize,
              'figure.constrained_layout.use': True,
              'figure.figsize': [fig_width, fig_height],
              #  'axes.formatter.min_exponent': 2,
              #  'figure.autolayout': True
    }

    mpl.rcParams.update(params)


def format_axes(ax):

    for spine in ['top', 'right']:
        ax.spines[spine].set_visible(False)

    for spine in ['left', 'bottom']:
        ax.spines[spine].set_color(SPINE_COLOR)
        ax.spines[spine].set_linewidth(0.5)

    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_tick_params(direction='out', color=SPINE_COLOR)

    return ax


# https://jwalton.info/Embed-Publication-Matplotlib-Latex/
# https://web.archive.org/web/20190917140521/https://jwalton.info/Embed-Publication-Matplotlib-Latex/
""" A simple example of creating a figure with text rendered in LaTeX. """
def set_size(width, fraction=1, unit='pt'):
    """ Set aesthetic figure dimensions to avoid scaling in latex.

    Parameters
    ----------
    width: float
            Width in pts
    fraction: float
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure
    fig_width_pt = width * fraction

    # Convert from pt to inches
    if unit == 'in':
        inches_per_pt = 1
    elif unit == 'pt':
        inches_per_pt = 1 / 72.27
    else:
        print('No unit given, defaulting to points')
        inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def set_axis_labels(xlabel, ylabel, ax=plt):
    if ax == plt:
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
    else:
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)


def set_axis_labels_endaxes(xlabel, ylabel, ax=plt):
    ax.annotate(xlabel,
                xycoords='axes fraction',
                xy=(1, 0),
                textcoords='offset points',
                xytext=(1, 0),
                ha='left',
                va='center',
                fontsize=axes_labelsize)
    ax.annotate(ylabel,
                xycoords='axes fraction',
                xy=(0, 1),
                textcoords='offset points',
                xytext=(-10, 1),
                #  xytext=(-30, 1),
                ha='left',
                va='bottom',
                fontsize=axes_labelsize)


def set_legend_under(fig, axes, ncol=6):
    # Don't forget to set
    # bbox_inches='tight',
    # when using .savefig()!

    handles = []
    labels = []

    for ax in axes.flatten():
        h, l = ax.get_legend_handles_labels()
        handles.extend(h)
        labels.extend(l)

    fig.legend(
        handles, labels,
        # This is still a huge mystery to me... align instead of location?
        loc='upper center',
        bbox_to_anchor=(0.5, 0),
        bbox_transform=fig.transFigure,
        ncol=ncol,
        #  mode='expand',  # Doesn't work, sadly
        )


def xe(rv):
    return '\\mathbb{{E}}[{}]'.format(rv)


def xv(rv):
    return '\\mathbb{{V}}\\mathrm{{ar}}[{}]'.format(rv)


def xstd(rv):
    return '\\sigma({})'.format(rv)


if __name__ == "__main__":
    x = np.linspace(0, tau, 100)

    # Initialise figure instance
    #  fig, ax = plt.subplots(1, 1, figsize=set_size(width))

    #  size = 452.96796  # pt
    size = 5.78853  # in, text/linewidth without fullpage
    sizes = set_size(size, unit='in')
    plt.figure(figsize=sizes)

    #  latexify()
    latexify(*sizes)

    # Plot
    #  ax.plot(x, np.sin(x))
    #  ax.set_xlim(0, tau)
    #  ax.set_xlabel(r'$\theta$')
    #  ax.set_ylabel(r'$\sin{(\theta)}$')
    plt.plot(x, np.sin(x))
    plt.xlim(0, tau)
    plt.xlabel(r'$\theta$')
    plt.ylabel(r'$\sin{(\theta)}$')

    plt.show()

    # Save and remove excess whitespace
    #  plt.savefig('./figs/example_2.pdf', format='pdf')

import matplotlib.pyplot as plt


fig, ax = plt.subplots()
ax.set_yticks([0.2, 0.6, 0.8], minor=False)
#  ax.set_yticks([0.3, 0.55, 0.7], minor=True)
ax.set_yticks([0, 1], minor=True)
#  ax.yaxis.grid(False, which='major')
ax.yaxis.grid(True, which='minor')
ax.set_ylim([-0.5, 1.5])
plt.show()

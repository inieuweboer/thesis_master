def test_gpc_uniform_transform_mean_var_multiple(
        model_fns=None,
        funcnames=None,
        npoints=15,
        plot=False,
        ):

    if model_fns is None or funcnames is None:
        model_fns = [
                     #  lambda x: (x**2 - 4*x) / (1 - x),
                     lambda x: np.tan(np.pi / 2 - x),
                     ]
        funcnames = [
                     '$x\\mapsto \\frac{{x^2 - 4x}}{{1-x}}$',
                     'adjusted tan',
                     ]
    #####
    def pdf(x):
        return stats.uniform.pdf(x, loc=-1, scale=2)

    mean_approxes_strong = []
    var_approxes_strong = []

    mean_approxes_weak = []
    var_approxes_weak = []

    for model_fn in model_fns:
        mean_approx_strong = []
        var_approx_strong = []

        mean_approx_weak = []
        var_approx_weak = []

        K = 1
        KK = np.arange(K, K + npoints)
        MM = KK + 1

        for K, M in zip(KK, MM):
            ##### Strong gPC
            _, fcs_strong, hsq_strong = gpc_approx_discrete_proj_uniform_tree(
                model_fn,
                a=-1, b=1,
                gpc_deg=K, gauss_quad_deg=M,
                )

            mean_approx_strong.append(fcs_strong[0])
            var_approx_strong.append(np.nansum(fcs_strong[1:]**2 * hsq_strong[1:]))

            ##### Weak gPC
            uniform_model_fn = lambda y: model_fn(
                make_affine_transf(0, 1, -1, 1)(y))

            _, fcs_weak, hsq_weak = gpc_approx_discrete_proj_uniform_tree(
                uniform_model_fn,
                a=0, b=1,
                gpc_deg=K, gauss_quad_deg=M,
                )

            mean_approx_weak.append(fcs_weak[0])
            var_approx_weak.append(np.nansum(fcs_weak[1:]**2 * hsq_weak[1:]))

        mean_approxes_strong.append(mean_approx_strong)
        var_approxes_strong.append(var_approx_strong)
        mean_approxes_weak.append(mean_approx_weak)
        var_approxes_weak.append(var_approx_weak)

    mean_approxes_strong = np.array(mean_approxes_strong)
    var_approxes_strong = np.array(var_approxes_strong)
    mean_approxes_weak = np.array(mean_approxes_weak)
    var_approxes_weak = np.array(var_approxes_weak)

    fig, axes = plt.subplots(nrows=1, ncols=2)
    #  fig.set_figheight(fig.get_figheight() / 2)

    figname = 'est_mean_var_strong_un'

    beta_label = 'Strong app.'
    xlabel = 'K'
    other = 'M'
    rv = 'f(U)'

    it = enumerate(zip(
        model_fns, funcnames, mean_approxes_strong, var_approxes_strong))
    for i, (model_fn, func, mean_approx_strong, var_approx_strong) in it:
        # Baseline
        mean_quad_strong = quad(lambda x: model_fn(x) * pdf(x), -1, 1)[0]
        var_quad_strong = quad(
            lambda x: (model_fn(x) - mean_quad_strong)**2 * pdf(x), -1, 1)[0]

        axes[0].plot(
            KK, np.abs((mean_quad_strong - mean_approx_strong) / mean_quad_strong),
            '.--', lw=0.5, label='{} of {}'.format(beta_label, func))
        axes[1].plot(
            KK, np.abs((var_quad_strong - var_approx_strong) / var_quad_strong),
            '.--', lw=0.5, label='{} of {}'.format(beta_label, func))

    axes[0].set_yscale('log')
    axes[0].legend(frameon=False)
    axes[1].set_yscale('log')
    set_axis_labels_endaxes(
        xlabel, 'Rel. bias est. of ${}$'.format(xe(rv)), axes[0])
    set_axis_labels_endaxes(
        xlabel, 'Rel. bias est. of ${}$'.format(xv(rv)), axes[1])

    fig.savefig(
        './figs/UQ_{}_{}_{}_{}_{}_{}-{}'
        '.pdf'.format(
            figname,
            'multiple',
            other, '=K+1',
            xlabel, KK[0], KK[-1],
            ),
        format='pdf',
        )

    fig, axes = plt.subplots(nrows=1, ncols=2)

    figname = 'est_mean_var_weak_un'
    uniform_label = 'Weak app.'

    it = enumerate(zip(
        model_fns, funcnames, mean_approxes_weak, var_approxes_weak))
    for i, (model_fn, func, mean_approx_weak, var_approx_weak) in it:
        # Baseline
        mean_quad_strong = quad(lambda x: model_fn(x) * pdf(x), 0, 1)[0]
        var_quad_strong = quad(
            lambda x: (model_fn(x) - mean_quad_strong)**2 * pdf(x), 0, 1)[0]

        axes[0].plot(
            KK, np.abs((mean_quad_strong - mean_approx_weak) / mean_quad_strong),
            '.--', lw=0.5, label='{} of {}'.format(uniform_label, func))
        axes[1].plot(
            KK, np.abs((var_quad_strong - var_approx_weak) / var_quad_strong),
            '.--', lw=0.5, label='{} of {}'.format(uniform_label, func))

    axes[0].set_yscale('log')
    axes[0].legend(frameon=False)
    axes[1].set_yscale('log')
    set_axis_labels_endaxes(
        xlabel, 'Rel. bias est. of ${}$'.format(xe(rv)), axes[0])
    set_axis_labels_endaxes(
        xlabel, 'Rel. bias est. of ${}$'.format(xv(rv)), axes[1])

    fig.savefig(
        './figs/UQ_{}_{}_{}_{}_{}_{}-{}'
        '.pdf'.format(
            figname,
            'multiple',
            other, '=K+1',
            xlabel, KK[0], KK[-1],
            ),
        format='pdf',
        )



import multiprocessing



def worker(procnum, return_dict):
    '''worker function'''
    name = multiprocessing.current_process().name

    print(name + ' represent!')

    return_dict[name] = procnum + 1


if __name__ == '__main__':
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    jobs = []

    for i in range(5):
        p = multiprocessing.Process(target=worker, args=(i, return_dict))
        jobs.append(p)
        p.start()

    for proc in jobs:
        proc.join()
        print(proc.name, 'joined.')

    print(return_dict)
    print('Amt of processors: ', multiprocessing.cpu_count())

#!/bin/bash
#SBATCH --job-name=UQTEST
#SBATCH --output=%x-%j.out
#SBATCH -N 1
#SBATCH --tasks-per-node 5
#SBATCH -p short
#SBATCH --time=00:05:00
#SBATCH --mail-type=BEGIN,END
echo "start of job: UQTEST"

pyth="python3"
work_dir="$HOME/thesis_rainfall"

time "${pyth}" "${work_dir}/UQ_tests.py"

echo end of job

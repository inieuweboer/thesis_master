import os
import shutil

from rainfall_model import *

from helper_functions import (
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    )


def main():
    # Get args
    args = rf_argparser()
    (datetimestr, nsims,
     L,
     T, rfparams,
     gauss_quad_deg,
     node_index,
     a, b,
     outfolder,
     ) = (args.datetime, args.nsims,
          args.L,
          args.T, args.rfparams,
          args.gauss_quad_deg,
          args.node_index,
          args.a, args.b,
          args.outfolder,
          )

    # Prepare filenames
    if args.test:
        outfolder = outfolder + 'test/'

    os.makedirs(outfolder, exist_ok=True)

    args_key = args_str_maker(
        L=L, T=T,
        rfparamshash=rf_params_tostring(rfparams),
        )

    mergedfile = '{}{}.bakbak'.format(outfolder, args_key)
    infiles = [f for f in os.listdir(outfolder) if f.endswith(args_key) and f.startswith('2020')]

    # Appends to existing file
    with open(mergedfile, 'ab') as fp_merged:
        for infile in infiles:
            infilename = outfolder + infile

            with open(infilename, 'rb') as fp_in:
                shutil.copyfileobj(fp_in, fp_merged)


if __name__ == "__main__":
    main()

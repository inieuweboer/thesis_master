def rescale(x, p=0.5):
    xmin = np.min(x)
    xmax = np.max(x)
    xshifted = (x - xmin) / (xmax - xmin)
    xwarped = np.power(xshifted, p)

    return (xmax - xmin) * xwarped + xmin


class Normal_sampler():
    def __init__(self, loc=0, scale=1, cachesize=10):
        self.loc = loc
        self.scale = scale
        self.cachesize = cachesize

        self.set_it()

    def set_it(self):
        self.it = iter(np.random.normal(loc=self.loc, scale=self.scale, size=self.cachesize))

    def sample(self):
        try:
            return next(self.it)
        except:
            self.cachesize *= 2
            self.set_it()
            return next(self.it)


def stoch_round(x):
    xdown = np.floor(x)
    p = np.random.sample()
    if p < x - xdown:
        return xdown + 1
    else:
        return xdown


def memoize(func):
    cache = dict()

    def memoized_func(*args):
        if args in cache:
            return cache[args]
        result = func(*args)
        cache[args] = result
        return result

    return memoized_func


def identity(*args, **kwargs):
    if not args:
        if not kwargs:
            return None
        elif len(kwargs) == 1:
            return kwargs.values()[0]
        else:
            return (*kwargs.values(),)
    elif not kwargs:
        if len(args) == 1:
            return args[0]
        else:
            return args
    else:
        return (*args, *kwargs.values())


def compose(*functions):
    return functools.reduce(lambda f, g: lambda *a, **kwa: f(g(*a, **kwa)),
                            functions,
                            lambda x, *a: (x,) + a if a else x,
                            # lambda *a: a[0] if len(a) == 1 else a
                            #  identity,  # speed...
                            )



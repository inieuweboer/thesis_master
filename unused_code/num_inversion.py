import numpy as np
from scipy.optimize import root_scalar

from pynverse import inversefunc
#  from inverse import inversefunc


# 9.7 ms
class Num_inv_sampler():
    def __init__(self, CDF):
        self.inv = inversefunc(CDF)

    def solve(self, u_vals):
        return self.inv(u_vals)

    def sample(self, size=1):
        un_samples = np.random.sample(size=size)
        return self.inv(un_samples)


# 30 ms
class Num_inv_sampler_optim():
    def __init__(self, CDF, pdf=None, pdf_deriv=None, x0=0.5, x1=None, bracket=None):
        if pdf is None:
            bracket = [-10, 10]

        # Note - if passing pdf, CDF must be right and not survival func/complementary, i.e. 1 - CDF
        # due to sign in derivative
        self.solver = lambda u: root_scalar(lambda x: CDF(x) - u, fprime=pdf, fprime2=pdf_deriv, x0=x0, x1=x1, bracket=bracket)

    def solve(self, u_vals):
        if isinstance(u_vals, int):
            u_vals = (uvals, )

        return np.array([sol.root for sol in map(self.solver, u_vals)])

    def sample(self, size=1):
        un_samples = np.random.sample(size=size)
        return self.solve(un_samples)


import numpy as np
import matplotlib.pyplot as plt

N = 1000

beta = 2

Nquantiles = 100
quantile_space = np.linspace(0, 1, Nquantiles)

#  samples1 = np.random.exponential(beta, size=N)
#  samples2 = -beta * np.log(np.random.sample(size=N))

#  samples1 = np.random.randn(N)
#  samples2 = np.random.normal(size=N)

samples1 = np.random.normal(size=N)
samples2 = np.random.choice(samples1, size=int(N/10), replace=False)

quantiles1 = np.quantile(samples1, quantile_space)
quantiles2 = np.quantile(samples2, quantile_space)


plt.scatter(quantiles1, quantiles2)

both = np.concatenate((samples1, samples2))
both_min = np.min(both)
both_max = np.max(both)
both_len = both_max - both_min
tweak = both_len / 10

domain = np.linspace(np.min(both), np.max(both))
#  domain = np.linspace(np.min(both) - tweak, np.max(both) + tweak)

plt.plot(domain, domain, 'r-', linewidth=0.5)

plt.show()

#!/bin/bash
#SBATCH --job-name=RFGENCMC
#SBATCH --output=%x-%j.out
#SBATCH -N 1
#SBATCH --tasks-per-node 15
#
#SBATCH --time=02:00:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: RFGENCMC"

#Determining the number of processors in the system
NPROC=`nproc --all`
let lim=$NPROC-1

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

case $HOSTNAME in
  (antergos-sopropo) work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl) echo "should not run on ${HOSTNAME} without sbatch, exit"; exit 1;;
  (*) module load 2019; module unload Python; module load Python/3.6.6-intel-2018b; work_dir="$HOME/thesis_rainfall";;
esac
echo `which python3`

nloops=1000
nsimsperrun=100
outfolder='out/rf/cmc/'

for i in `seq $nloops`; do
  params="--nsims ${nsimsperrun} --outfolder ${outfolder}";

  # python3 "${work_dir}/rainfall_model_uq_gendata_one.py" --datetime "$dt" $params &
  python3 ${work_dir}/rainfall_model_cmc_gendata.py $params &

  while (( `jobs | wc -l` >= $lim )); do
    sleep 5
  done
done

wait

echo end of job

#!/bin/bash
#SBATCH --job-name=BMSPTEST
#SBATCH --output=%x-%j.out
#SBATCH -p short
#SBATCH -N 1
#SBATCH --tasks-per-node 3
#SBATCH --time=00:05:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: BMSPTEST"

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

case $HOSTNAME in
  (antergos-sopropo)
    pyth="python";
    work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl)
    echo "should not run on ${HOSTNAME} without sbatch, exit";
    exit 1;;
  (*)
    # py3 and latest versions of numpy, scipy
    # pyth="$HOME/.virtualenvs/math/bin/python";
    pyth="python3";
    work_dir="$HOME/thesis_rainfall";;
esac

# params="--L 5"
# params="--L 18"
# params="--L 26"

# params="--nsims 2 --FO 5 2 --FE 10 --FNS 10 --test"
# params="--nsims 2 --FO 5 5 --CMC -1 --FNS -1 --FE -1 --test"
params="--L 5 --nsims 2 --FO 5 2 --CMC -1 --FNS -1 --FE -1 --test"
# params=" --nsims 100 --L 26 --FO 5 2 --FE 10 --FNS 10 --test"

time "${pyth}" "${work_dir}/bm_splitting_runall.py"   --datetime "$dt" $params && \
time "${pyth}" "${work_dir}/bm_splitting_validate.py" --datetime "$dt" $params

# time "${pyth}" -u "${work_dir}/bm_splitting_compare_if.py"       --datetime "$dt" $params && \
# time "${pyth}" -u "${work_dir}/bm_splitting_compare_if_valid.py" --datetime "$dt" $params

# time "${pyth}" "${work_dir}/bm_splitting_runall.py"   --datetime "$dt" $params && \
# time "${pyth}" "${work_dir}/bm_splitting_validate.py" --datetime "$dt" $params

# time "${pyth}" "${work_dir}/bm_splitting_compare_if.py"       --datetime "$dt" $params && \
# time "${pyth}" "${work_dir}/bm_splitting_compare_if_valid.py" --datetime "$dt" $params
echo end of job

import numpy as np
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d
from scipy.stats import halfnorm
from scipy.special import erf
#  from warnings import warn

from bm_splitting import exact_prob_bm
from helper_functions import qq_plotter
from num_inversion import Num_inv_sampler, Num_inv_sampler_optim
from text_render_latex import set_size; size=452.96796


spmult = 10


Phi = lambda z: (1 + erf(z / np.sqrt(2))) / 2
#  CDF_half_normal = lambda z: (erf(z / np.sqrt(2)))

def phi(z, loc=0, scale=1):
    return np.exp(-(z-loc)*(z-loc) / (2*scale*scale)) / (np.sqrt(2*np.pi) * scale)

Phi_rm_bm_drift_surv = lambda L, T, alpha: Phi( (alpha*T - L) / np.sqrt(T)) + np.exp(2*L*alpha) * Phi( -(L + alpha*T) / np.sqrt(T))
phi_rm_bm_drift = lambda L, T, alpha: phi( (alpha*T - L) / np.sqrt(T)) / np.sqrt(T) - \
                                      2*alpha * np.exp(2*L*alpha) * Phi( -(L + alpha*T) / np.sqrt(T)) + \
                                      np.exp(2*L*alpha) * phi( -(L + alpha*T) / np.sqrt(T)) / np.sqrt(T)



def bm_drift_rm_cmc(L=5, T=30,
                    drift=0,
                    alg_params=(None, np.power(10, 3)),
                    show=False,
                    ):

    stepsize, nruns = alg_params

    if stepsize is None:
        stepsize = 0.01

    nsteps = int(T / stepsize)

    hits = 0

    bm_samples = []
    rm_samples = []

    for _ in range(nruns):
        pos = 0
        rm = 0
        trajectory_bm = [pos]
        trajectory_rm = [rm]

        # Pre-sample normally distributed steps
        normal_samples = iter(np.random.normal(scale=np.sqrt(stepsize), size=nsteps))

        for delta_w in normal_samples:
            # http://etheses.lse.ac.uk/3833/1/Ho__on-the-running-maximum.pdf
            delta_x = delta_w + stepsize*drift

            #  rm = max(rm, np.sqrt(delta_w*delta_w + 2*stepsize*np.random.exponential())/2 + pos)
            rm = max(rm, np.sqrt(delta_x*delta_x + 2*stepsize*np.random.exponential())/2 + pos)  # GOK
            pos += delta_x
            #  rm = max(rm, pos)

            trajectory_bm.append(pos)
            trajectory_rm.append(rm)

            #  if pos >= L:
            #      hits += 1
            #      break

        bm_samples.append(pos)
        rm_samples.append(rm)

        if show:
            plt.plot(np.linspace(0, len(trajectory_bm)*stepsize, len(trajectory_bm)),
                    trajectory_bm,
                    #  color='k',
                    linewidth=.1)

            plt.plot(np.linspace(0, len(trajectory_rm)*stepsize, len(trajectory_rm)),
                    trajectory_rm,
                    color='k',
                    linewidth=.1)

            # Plot endpoint of bm trajectory
            #  plt.plot((len(trajectory_bm) - 1)*stepsize, trajectory_bm[-1] ,
            #           color='green', marker='.')

    if show:
        tspace = np.linspace(0, T, T*spmult + 1)
        plt.plot(tspace, np.full_like(tspace, L), 'r--', linewidth=1)

        plt.show()

    return np.array(bm_samples), np.array(rm_samples)


def hist_half_normal(samples=None, loc=0, scale=1):
    if samples is None:
        size = np.power(10, 3)
        np.abs(np.random.normal(loc=loc, scale=scale, size=size))

    size = len(samples)

    fig, ax = plt.subplots()

    x = np.linspace(0, T)

    ax.plot(x, halfnorm.pdf(x, scale=sigma))
    ax.hist(samples, bins=int(np.power(size, 1/3)), density=True)


####


def sample_half_normal(loc=0, scale=1, size=1):
    return np.abs(np.random.normal(loc=loc, scale=scale, size=size))


def half_normal_e(scale=1):
    return np.sqrt(2*scale / np.pi)


def half_normal_var(scale=1):
    return scale*(1 - 2/np.pi)


####


#### WRONG
# This is sort of an interpolant per value for alpha, not a gPC expansion
def sample_rm_bm_drift_and_interpolate():
    from scipy.stats import t

    T = 30

    def chebyshev_nodes(n, a=-1, b=1):
        return (b - a) * (np.flip(np.cos( (2*np.arange(0, n) + 1) * np.pi / (2 * n))) + 1)/2 + a

    nsims = np.power(10, 2)
    nnodes = 30
    drift_nodes = chebyshev_nodes(nnodes)

    rm_samples = []

    for drift_node in drift_nodes:
        #  _, rm_samples_single_node = bm_drift_rm_cmc(T=T,
        #                          drift=drift_node,
        #                          alg_params=(None, nsims),
        #                          #  show=True,
        #                          )
        drift_CDF = lambda L: 1 - Phi_rm_bm_drift_surv(L, T, drift_node)
        drift_pdf = lambda L: phi_rm_bm_drift(L, T, drift_node)

        bm_rm_drift_sampler = Num_inv_sampler(CDF=drift_CDF)
        #  bm_rm_drift_sampler = Num_inv_sampler_optim(CDF=drift_CDF, pdf=drift_pdf)

        rm_samples_single_node = bm_rm_drift_sampler.sample(size=nsims)

        rm_samples.append(rm_samples_single_node)

    rm_samples = np.array(rm_samples)
    rm_means = np.mean(rm_samples, axis=1)
    rm_stds = np.std(rm_samples, axis=1, ddof=1.5)
    rm_sems = rm_stds / np.sqrt(nsims)

    confidence_level = 0.05
    zeta = t.ppf(1 - confidence_level/2, nsims - 1)

    interp_mean = interp1d(drift_nodes, rm_means, kind='linear')
    interp_std = interp1d(drift_nodes, rm_stds, kind='linear')
    interp_sem = interp1d(drift_nodes, rm_sems, kind='linear')

    drift_space = np.linspace(np.min(drift_nodes), np.max(drift_nodes))
    interpd_means = interp_mean(drift_space)
    interpd_stds = interp_std(drift_space)
    interpd_sems = interp_sem(drift_space)

    plt.plot(drift_space, interpd_means, 'k', label='interpolant')
    #  plt.fill_between(drift_space, interpd_means - zeta*interpd_sems, interpd_means + zeta*interpd_sems, alpha=0.2, label='uncertainty')
    plt.fill_between(drift_nodes, rm_means - rm_stds, rm_means + rm_stds, alpha=0.2, label='uncertainty')
    plt.show()


def test_samplers():
    normal_sampler = Num_inv_sampler(CDF=Phi)
    normal_sampler2 = Num_inv_sampler_optim(CDF=Phi, pdf=phi, pdf_deriv=lambda x: -x*phi(x), x0=0.5)

    x = np.linspace(-5, 5)
    eps = 0.001; nspace = 201; y = np.linspace(eps, 1- eps, nspace)
    print(np.max(np.abs(sampler.solve(Phi(x)) - x)))
    print(np.max(np.abs(Phi(sampler.solve(y)) - y)))

    n = 1000
    qq_plotter(np.random.normal(size=n), normal_sampler.sample(size=n))
    qq_plotter(np.random.normal(size=n), normal_sampler2.sample(size=n))
    plt.show()


def main():
    L = 5
    T = 30

    _, rm_samples = bm_drift_rm_cmc(L=L, T=T,
                              drift=0.5,
                              alg_params=(None, 10),
                              show=True,
                             )
    size = rm_samples.size

    prob_samples = rm_samples >= L
    cmc_est = np.sum(prob_samples) / size
    exact_prob = exact_prob_bm(L, T)

    print('Real value:', exact_prob)
    print('CMC est:', cmc_est)
    print('Bias:', np.abs(exact_prob - cmc_est))
    print('True RE:', (1 - exact_prob) / np.sqrt(exact_prob * size))
    print('Sample RE:', np.std(prob_samples, ddof=1) / exact_prob)

    fig, ax = plt.subplots()

    #  hn_samples_sp = halfnorm.rvs(scale=np.sqrt(T), size=size)
    hn_samples = sample_half_normal(scale=np.sqrt(T), size=size)
    qq_plotter(rm_samples, hn_samples, ax=ax)

    #  plt.figure(figsize=set_size(width))
    fig.figsize = set_size(size)
    #  fig.savefig('./figs/bm_running_max.pdf', format='pdf', bbox_inches='tight')
    plt.show()


    #  param_min = 0.5
    #  param_max = 2

    #  n_sample_points = 7

    #  param_space = np.linspace(param_min, param_max)


if __name__ == "__main__":
    #  main()
    sample_rm_bm_drift_and_interpolate()
    #  test_samplers()

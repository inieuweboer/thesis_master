Code for master thesis on rare event simulation and uncertainty quantification of a precipitation model by D. S. Wilks.

Licensed under [3-Clause BSD](https://opensource.org/licenses/BSD-3-Clause), see [LICENSE](https://gitlab.com/inieuweboer/thesis_master/-/blob/master/LICENSE).

For the written thesis, see [here](https://www.overleaf.com/read/tnzqdwqsdqgz).

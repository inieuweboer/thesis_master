import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation

import sys

from text_render_latex import latexify
latexify(columns=2)


def bm_cmc_update(L, T, alg_params=(0.01, np.power(10, 2)),
           show=False):

    stepsize, nruns = alg_params

    pos = 0
    trajectory = [pos]

    # Pre-sample normally distributed steps
    normal_samples = iter(np.random.normal(scale=np.sqrt(stepsize),
                                            size=nsteps))

    for sample in normal_samples:
        pos += sample
        trajectory.append(pos)

        if pos >= L:
            hits += 1
            break

    if show:
        plt.plot(np.linspace(0, (len(trajectory) - 1)*stepsize,
                                len(trajectory)),
                    trajectory,
                    color='k',
                    linewidth=.125)

        # Plot endpoint of trajectory
        plt.plot((len(trajectory) - 1)*stepsize, trajectory[-1],
                    color='green', marker='o', zorder=100)


    return hits / nruns



def update(i, x, line, ax):
    label = 'timestep {0}'.format(i)
    print(label)
    # Update the line and the axes (with a new xlabel). Return a tuple of
    # "artists" that have to be redrawn for this frame.
    line.set_ydata(x - 5 + i)
    ax.set_xlabel(label)

    return line, ax


def main():
    nframes = 1000

    # Query the figure's on-screen size and DPI. Note that when saving the figure to
    # a file, we need to provide a DPI for that separately.
    print('fig size: {0} DPI, size in inches {1}'.format(
        fig.get_dpi(), fig.get_size_inches()))

    # Plot persisting things.
    tspace = np.linspace(0, T, T*spmult + 1)
    plt.plot(tspace, np.full_like(tspace, L), 'r--', linewidth=1)

    # FuncAnimation will call the 'update' function for each frame; here
    # animating over 10 frames, with an interval of 200ms between frames.
    anim = FuncAnimation(fig, update, frames=np.arange(0, 10), fargs=(x, line, ax), interval=200)

    if len(sys.argv) > 1 and sys.argv[1] == 'save':
        anim.save('line.gif', dpi=80, writer='imagemagick')
    else:
        # plt.show() will just loop the animation forever.
        plt.show()


if __name__ == '__main__':
    main()

import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

from helper_functions import (
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    rfparams_alpha, rfparams_ucov,
    )
from rainfall_model_cumulative_splitting import *
from UQ_methods import *

from text_render_latex import *
latexify(columns=2)


def gpc_rf_prob_fit(sp_mult=20):
    # Get args
    args = rf_argparser()

    (L, T, nlevels, rfparams_def,
     M, K,
     a, b,
     outfolder,
     ) = (
          args.L, args.T, args.nlevels, args.rfparams,
          args.gauss_quad_deg, args.gpc_deg,
          args.a, args.b,
          args.outfolder,
          )

    name = '_rf_'
    filename_merged_pre = "".join([outfolder, 'merged', name])

    work_nodes = gpc_approx_discrete_proj_uniform_tree_nodes(
        a=a, b=b,
        gauss_quad_deg=M,
        )

    model_fn_vec = []

    #  for work_node in work_nodes:
    for node_index in range(len(work_nodes)):
        rfparams = rfparams_ucov(rfparams_def, work_nodes, node_index)
        #  rfparams = rfparams_alpha(rfparams_def, work_nodes, node_index)

        args_key = args_str_maker(
            L=L, T=T, nlevels=nlevels,
            rfparamshash=rf_params_tostring(rfparams),
            )

        mergedfile = filename_merged_pre + args_key

        with open(mergedfile, 'r') as fp_merged:
            ests = np.loadtxt(fp_merged)
            model_fn_vec.append(np.mean(ests))

    model_fn_vec = np.array(model_fn_vec)

    gpc_fn, fcs, norms_sq = gpc_approx_discrete_proj_uniform_tree_from_data(
        model_fn_vec,
        a=a, b=b,
        gauss_quad_deg=M, gpc_deg=K,
        )

    #####
    # Plot true and approx using both naive gPC and func gPC approx
    fig, ax1 = plt.subplots()
    fig.set_figheight(fig.get_figheight() * 3/4)
    fig.set_figwidth(fig.get_figwidth() * 3/4)

    space = np.linspace(a, b, int(np.ceil(b - a))*sp_mult + 1)

    ax1.plot(space, gpc_fn(space), 'midnightblue',
             linewidth=.75, label='gPC')

    print(gpc_fn(space))

    xlabel = '$\\kappa$'
    ylabel = '$\\mathbb{{P}}[R_{{ \\mathrm{{cumul}}}}(\\kappa) > {}]$'.format(L)
    #  xlabel = '$\\alpha$'
    #  ylabel = '$\\mathbb{{P}}[R_{{ \\mathrm{{cumul}}}}(\\alpha) > {}]$'.format(L)
    ax1.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    set_axis_labels_endaxes(xlabel, ylabel, ax1)

    figname = 'normal'
    fig.savefig(
        'figs/fit_rf_Ucov_gpc_{}_T_{}_L_{}_'
        #  'figs/fit_rf_alpha_gpc_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf',
        )



if __name__ == "__main__":
    gpc_rf_prob_fit()

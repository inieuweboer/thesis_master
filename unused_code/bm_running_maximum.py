import numpy as np
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d, lagrange, PchipInterpolator
from scipy.stats import halfnorm
from scipy.special import erf

from bm_splitting import exact_prob_bm
from num_inversion import Num_inv_sampler, Num_inv_sampler_optim
from helper_functions import qq_plotter


spmult = 10

CDF_normal = lambda z: (1 + erf(z / np.sqrt(2))) / 2
CDF_half_normal = lambda z: (erf(z / np.sqrt(2)))



def bm_rm_cmc(L, T, alg_params=(None, np.power(10, 2)),
           show=False):

    stepsize, nruns = alg_params

    if stepsize is None:
        stepsize = 0.01

    nsteps = int(T / stepsize)

    hits = 0

    bm_samples = []
    rm_samples = []

    for _ in range(nruns):
        pos = 0
        rm = 0
        trajectory_bm = [pos]
        trajectory_rm = [rm]

        # Pre-sample normally distributed steps
        normal_samples = iter(np.random.normal(scale=np.sqrt(stepsize), size=nsteps))

        for _ in range(nsteps):
            delta_w = next(normal_samples)

            rm = max(rm, np.sqrt(delta_w*delta_w + 2*stepsize*np.random.exponential())/2 + pos)
            pos += delta_w
            #  rm = max(rm, pos)

            trajectory_bm.append(pos)
            trajectory_rm.append(rm)

            #  if pos >= L:
            #      hits += 1
            #      break

        bm_samples.append(rm)
        rm_samples.append(rm)

        if show:
            plt.plot(np.linspace(0, len(trajectory_bm)*stepsize, len(trajectory_bm)),
                    trajectory_bm,
                    #  color='k',
                    linewidth=.1)

            plt.plot(np.linspace(0, len(trajectory_rm)*stepsize, len(trajectory_rm)),
                    trajectory_rm,
                    color='k',
                    linewidth=.1)

            # Plot endpoint of bm trajectory
            #  plt.plot((len(trajectory_bm) - 1)*stepsize, trajectory_bm[-1] ,
            #           color='green', marker='.')

    if show:
        tspace = np.linspace(0, T, T*spmult + 1)
        plt.plot(tspace, np.full_like(tspace, L), 'r--', linewidth=1)

        plt.show()

    return bm_samples, rm_samples



def plot_half_normal(sigma, n=np.power(10, 5)):
    plt.hist(np.abs(np.random.normal(scale=sigma, size=n)), bins=int(np.power(n, 1/3)), density=True)


def sample_half_normal(loc=0, scale=1, size=1):
    return np.abs(np.random.normal(loc=loc, scale=scale, size=size))


def half_normal_expec(scale=1):
    return scale*np.sqrt(2 / np.pi)


def half_normal_var(scale=1):
    return scale*scale*(1 - 2/np.pi)


def rm_expec(T=1):
    return np.sqrt(2 * T / np.pi)


def rm_var(T=1):
    return T*(1 - 2/np.pi)

####


def uq_on_bm_drift():
    pass


def pos_rv_approx_mean(surv_fn):
    # https://en.m.wikipedia.org/wiki/Expected_value#Alternative_formula_for_expected_value

    from scipy.integrate import quad

    return quad(surv_fn, 0, np.inf)


#### WRONG RV
def sample_rm_bm_and_interpolate():
    from scipy.stats import t

    T_max = 1000

    def chebyshev_nodes(n, a=0, b=T_max):
        return (b - a) * (np.flip(np.cos( (2*np.arange(0, n) + 1) * np.pi / (2 * n))) + 1)/2 + a

    nsims = np.power(10, 3)
    nnodes = 20
    T_nodes = chebyshev_nodes(nnodes)

    rm_samples = []

    for T_node in T_nodes:
        #  _, rm_samples_single_node = bm_rm_cmc(T=T,
        #                          drift=T_node,
        #                          alg_params=(None, nsims),
        #                          #  show=True,
        #                          )

        #  CDF = lambda L: Phi_rm_bm(L, T_node)
        #  pdf = lambda L: phi_rm_bm_drift(L, T, T_node)

        #  bm_rm_sampler = Num_inv_sampler(CDF=drift_CDF)
        #  bm_rm_sampler = Num_inv_sampler_optim(CDF=drift_CDF, pdf=drift_pdf)
        #  rm_samples_single_node = bm_rm_sampler.sample(size=nsims)

        rm_samples_single_node = sample_half_normal(scale=np.sqrt(T_node), size=nsims)

        rm_samples.append(rm_samples_single_node)

    rm_samples = np.array(rm_samples)
    rm_means = np.mean(rm_samples, axis=1)
    rm_stds = np.std(rm_samples, axis=1, ddof=1.5)
    rm_sems = rm_stds / np.sqrt(nsims)

    confidence_level = 0.05
    zeta = t.ppf(1 - confidence_level/2, nsims - 1)

    interp_mean = interp1d(T_nodes, rm_means, kind='linear')
    interp_sem = interp1d(T_nodes, rm_sems, kind='linear')
    #  interp_mean = lagrange(T_nodes, rm_means)
    #  interp_sem = lagrange(T_nodes, rm_sems)
    #  interp_mean = interp1d(T_nodes, rm_means, kind='cubic')
    #  interp_sem = interp1d(T_nodes, rm_sems, kind='cubic')
    #  interp_mean = PchipInterpolator(T_nodes, rm_means)
    #  interp_sem = PchipInterpolator(T_nodes, rm_sems)

    T_space = np.linspace(np.min(T_nodes), np.max(T_nodes))
    interpd_means = interp_mean(T_space)
    interpd_sems = interp_sem(T_space)

    plt.scatter(T_nodes, rm_means, c='k')
    plt.plot(T_space, interpd_means, 'k', label='interpolant')
    plt.plot(T_space, rm_expec(T_space), 'g', label='true mean', linewidth=.5)
    plt.fill_between(T_space, interpd_means - zeta*interpd_sems, interpd_means + zeta*interpd_sems, alpha=0.2, label='uncertainty')
    plt.show()




def main():

    L = 5
    T = 30

    _, rm_samples = bm_rm_cmc(L, T)

    fig, ax = plt.subplots()

    x = np.linspace(0, T)

    ax.plot(x, halfnorm.pdf(x))
    ax.hist(rm_samples, bins=int(np.power(len(rm_samples), 1/3)), density=True)

    fig, ax = plt.subplots()

    size = 1000  # len(rm_samples)
    #  hn_samples_sp = halfnorm.rvs(scale=T, size=size)
    hn_samples = sample_half_normal(scale=T, size=size)
    qq_plotter(rm_samples, hn_samples, ax=ax)

    #  print(hn_samples)
    #  print(rm_samples)
    plt.show()

    #  param_min = 0.5
    #  param_max = 2

    #  n_sample_points = 7

    #  param_space = np.linspace(param_min, param_max)


if __name__ == "__main__":
    sample_rm_bm_and_interpolate()
    #  main()

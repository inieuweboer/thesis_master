import numpy as np
import matplotlib.pyplot as plt

from scipy.special import erf
from warnings import warn

from text_render_latex import (latexify,
                               set_axis_labels_endaxes,
                               )
latexify(columns=2)

Phi = lambda z: (1 + erf(z / np.sqrt(2))) / 2


def test_phi_approx():
    # http://digitalcommons.odu.edu/cgi/viewcontent.cgi?article=1007&context=emse_fac_pubs
    # https://www.researchgate.net/profile/Naveen_Boiroju2/publication/279877108_Naveen_Kumar_Boiroju_ASR_290814/links/559d047108ae5d5eae66f550.pdf

    from scipy import stats
    Phi0 = stats.norm.cdf
    Phi7 = lambda z: 1 / (1 + np.exp(-1.702 * z))

    Phi12 = lambda z: 1 / (1 + np.exp(-17.007 * z / (11.108 - z)))

    # Used, max abs error of 10**-4
    from scipy.special import expit
    Phi3 = lambda z: 1 / (1 + np.exp( -( 1.5976*z + 0.07056*np.power(z, 3) )))
    #  Phi3 = lambda z: expit(1.5976*z + 0.07056*np.power(z, 3))

    # MSE lower with factor 10 than Phi3 but behaves badly around t = T
    Phi9 = lambda z: 1 / (1 + np.exp(-np.sqrt(np.pi)*(0.9*z + 0.0418198*np.power(z, 3) - 0.0004406*np.power(z, 5)) ))

    Phi = Phi3

    np.max(np.abs(Phi0(s) - Phi(s)))  # Max absolute error
    np.mean(np.abs(Phi0(s) - Phi(s)))  # Mean abs error
    np.square(Phi0(s) - Phi(s)).mean()  # Mean square error


##

def make_importance_fn_distance(T, L):
    #  return lambda t, x: np.full_like(t, x / L, dtype=float)  # 1 - np.abs(L - x) / L
    #  return lambda t, x: np.full_like(t, x / L, dtype=float)
    return lambda t, x: x / L


def make_importance_fn_brownian_motion(T, L):
    # Behaves badly for L / sqrt(T) > 8; Phi(9) == 1.0 gives True
    if L / np.sqrt(T) > 8:
        warn('L / sqrt(T) > 8 means normal CDF becomes almost 0 or 1, i.e. not enough precision!')

    h = lambda t, x: 2 * Phi((x-L) / np.sqrt(T-t))
    h00 = h(0, 0)
    h0L = h(0, L)

    #  return (h(t, x) - h(0, 0)) / (h(0, L) - h(0, 0)) * (t < T) + \
           #  np.full_like(t, 1, dtype=float) * (t >= T)
    return lambda t, x: (h(t, x) - h00) / (h0L - h00) * (x >= 0)


def make_importance_fn_large_deviation(T, L, p=2):
    return lambda t, x: ( 1 - np.power(1 - x / L, p) * T / (T - t) ) * (x < L) + \
           np.full_like(t, 1, dtype=float) * (x >= L)


def make_importance_fn_custom(T, L):
    return lambda t, x: np.power(x / L, T / (T - t), where=(x > 0))


## Bad performance, only use these for testing or plot making!

def importance_fn_distance_full(t, x, T, L):
    return make_importance_fn_distance(T, L)(t, x)


def importance_fn_brownian_motion_full(t, x, T, L):
    return make_importance_fn_brownian_motion(T, L)(t, x)


def importance_fn_large_deviation_full(t, x, T, L, p=2):
    return make_importance_fn_large_deviation(T, L)(t, x)


def importance_fn_custom_full(t, x, T, L):
    return make_importance_fn_custom(T, L)(t, x)


##


def plot_levels(T, L, importance_fn, levels, eps=0.1, space_mult=10,
                xlabel='$t$',
                ylabel='$x$',
                ax=plt):
    # More plotting grid points towards (T, L)
    tspace = np.concatenate((np.linspace(eps, (1 - 1/space_mult)*T, endpoint=False),
                             np.linspace((1 - 1/space_mult)*T, T - eps)))
    xspace = np.concatenate((np.linspace(0, (1 - 1/space_mult)*L, endpoint=False),
                             np.linspace((1 - 1/space_mult)*L, L - eps)))

    TT, XX = np.meshgrid(tspace, xspace)

    # Horizontal last level
    ax.plot(tspace, np.full_like(tspace, L), 'r--', linewidth=1)

    if len(levels) > 1:
        ax.contour(TT, XX, importance_fn(TT, XX), levels=np.append(0, levels),
                   colors=[(0.25, 0, 0)] + ['r']*len(levels),
                   linestyles='dashed',
                   linewidths=[.25] + [.5]*len(levels),
                   )
    if ax == plt:
        plt.xlim([0, T*1.01])
        #  plt.ylim([-L/10, L*1.1])
        plt.ylim([-L/10, L*1.25])
    else:
        ax.set_xlim([0, T*1.01])
        #  ax.set_ylim([-L/10, L*1.1])
        ax.set_ylim([-L/10, L*1.25])

    set_axis_labels_endaxes(xlabel, ylabel, ax=ax)


def plot_importance_contour(levels=None, sp_mult=10):
    #  np.seterr(divide='ignore')
    #  np.seterr(invalid='raise')

    T = 30
    L = 6
    nlevels = 3

    if levels is None:
        #  levels = [lvl_index/nlevels for lvl_index in range(1, nlevels+1)]
        levels = np.linspace(1/nlevels, 1, nlevels)

        # Test for LD
        #  rescale = lambda s: np.power(s, 0.5)
        #  rescale = lambda s: np.sqrt(1 - np.power(1 - s, 2))
        #  levels = rescale(levels)

    #  plot_levels(T, L, importance_fn, levels, ax=ax)

    noxlabel = ''
    xlabel = '$t$'
    ylabel = '$x$'

    fig, ax = plt.subplots()
    importance_fn = make_importance_fn_distance(T, L)
    plot_levels(T, L, importance_fn, levels, xlabel=noxlabel, ylabel=ylabel)
    #  print(importance_fn_distance_full(0, rescale(np.linspace(0, L, L*sp_mult+1)), T, L))

    yticks = np.arange(0, L + 1, 2)
    ax.yaxis.set_ticks(yticks)
    yticks = list(map(int, ax.get_yticks().tolist()))
    yticks[-1] = 'L={}'.format(yticks[-1])
    ax.set_yticklabels(yticks)

    xticks = np.arange(0, T + 1, 10)
    ax.xaxis.set_ticks(xticks)

    fig.savefig('./figs/imp_func_dist.pdf', format='pdf', transparent=True)

    fig, ax = plt.subplots()
    importance_fn = make_importance_fn_brownian_motion(T, L)
    plot_levels(T, L, importance_fn, levels, xlabel=noxlabel, ylabel=ylabel)
    #  print(importance_fn_brownian_motion_full(0, rescale(np.linspace(0, L, L*sp_mult+1)), T, L))

    #  yticks = np.arange(0, L + 1, 2)
    #  ax.yaxis.set_ticks(yticks)
    ax.set_yticklabels([])

    xticks = np.arange(0, T + 1, 10)
    ax.xaxis.set_ticks(xticks)

    fig.savefig('./figs/imp_func_BM.pdf', format='pdf', transparent=True)

    fig, ax = plt.subplots()
    importance_fn = make_importance_fn_large_deviation(T, L)
    plot_levels(T, L, importance_fn, levels, xlabel=xlabel, ylabel=ylabel)
    #  print(importance_fn_large_deviation_full(0, np.linspace(0, L, L*sp_mult+1), T, L))

    #  yticks = np.arange(0, L + 1, 2)
    #  ax.yaxis.set_ticks(yticks)
    ax.set_yticklabels([])

    xticks = np.arange(0, T + 1, 10)
    ax.xaxis.set_ticks(xticks)
    xticks = list(map(int, ax.get_xticks().tolist()))
    xticks[-1] = 'T={}'.format(xticks[-1])
    ax.set_xticklabels(xticks)

    fig.savefig('./figs/imp_func_LD.pdf', format='pdf', transparent=True)


if __name__ == "__main__":
    plot_importance_contour()
    #  plt.show()

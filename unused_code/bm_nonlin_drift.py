import matplotlib.pyplot as plt
import numpy as np


spmult = 10

a = 2
b = 5

def drift_fn(x, t, L, T):
    #  return np.exp(-x**2 / a) * x * (b - x**2)
    #  return np.log((np.exp(-x**2 / a) * x**2 * b) * (x > 0) + (x <= 0))
    return a*(x >= L/2)


def bm_cmc(L, T, alg_params=(0.01, np.power(10, 2)),
           show=False):

    step, nruns = alg_params
    nsteps = int(T / step)

    hits = 0

    for _ in range(nruns):
        pos = 0
        trajectory = [pos]

        # Pre-sample normally distributed steps
        normal_samples = iter(np.random.normal(scale=np.sqrt(step),
                                               size=nsteps))

        for i, bm_step in enumerate(normal_samples):
            pos += drift_fn(pos, i, L, T)*step + bm_step
            trajectory.append(pos)

            if pos >= L:
                hits += 1
                break

        if show:
            plt.plot(np.linspace(0, (len(trajectory) - 1)*step,
                                 len(trajectory)),
                     trajectory,
                     color='k',
                     linewidth=.125)

            # Plot endpoint of trajectory
            plt.plot((len(trajectory) - 1)*step, trajectory[-1],
                     color='green', marker='o', zorder=100)

    if show:
        tspace = np.linspace(0, T, T*spmult + 1)
        plt.plot(tspace, np.full_like(tspace, L), 'r--', linewidth=1)

        plt.show()

    return hits / nruns


if __name__ == "__main__":
    np.random.seed()

    L = 20
    T = 30

    print('L = ', L)
    print('T = ', T)

    step = 0.01
    nruns = 20

    show = True

    print(bm_cmc(L=L, T=T,
                 alg_params=(step, nruns),
                 show=show,
                 ))

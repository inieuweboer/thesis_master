### https://scicomp.stackexchange.com/a/29789

#!/usr/bin/env python3
import numpy as np
from scipy import integrate
from numba import complex128, float64, jit
import time


def integral(integrand, a, b,  arg):
    def real_func(x,arg):
        return np.real(integrand(x,arg))

    def imag_func(x,arg):
        return np.imag(integrand(x,arg))

    real_integral = integrate.quad(real_func, a, b, args=(arg))
    imag_integral = integrate.quad(imag_func, a, b, args=(arg))

    return real_integral[0] + 1j*imag_integral[0]

vintegral = np.vectorize(integral)


@jit(complex128(float64, float64), nopython=True, cache=True)
def f_integrand(s, omega):
    sigma = np.pi/(np.pi+2)
    xs = np.exp(-np.pi*s/(2*sigma))
    x1 = -2*sigma/np.pi*(np.log(xs/(1+np.sqrt(1-xs**2)))+np.sqrt(1-xs**2))
    x2 = 1-2*sigma/np.pi*(1-xs)
    zeta = x2+x1*1j
    Vc = 1/(2*sigma)
    theta =  -1*np.arcsin(np.exp(-np.pi/(2.0*sigma)*s))
    t1 = 1/np.sqrt(1+np.tan(theta)**2)
    t2 = -1/np.sqrt(1+1/np.tan(theta)**2)

    return np.real((t1-1j*t2)/np.sqrt(zeta**2-1))*np.exp(1j*omega*s/Vc);

t0 = time.process_time()
omega = 10

for i in range(300):
    #  result = integral(f_integrand, 0, np.inf, omega)
    result = integral(f_integrand, 0, 30, omega)

print(time.process_time()-t0)
print(result)

import matplotlib.pyplot as plt
import numpy as np

from bm_splitting import Phi

from text_render_latex import *
latexify(columns=2)


def main():
    N = 100

    a = -10
    b = 10
    x_mult = 4

    #  model_fn = lambda x, param: Phi(x / param)
    model_fn = lambda x, param: 1 / (1 + np.exp(-param*x))

    x = np.linspace(a, b, int((b - a)*x_mult) + 1)

    # CMC
    #  params_sample = np.random.exponential(scale=1, size=N)
    params_sample = np.random.uniform(0, 0.5, size=N)
    XX, PS = np.meshgrid(x, params_sample, indexing='ij')

    samples = model_fn(XX, PS)

    prob_means = np.mean(samples, axis=1)
    prob_stds = np.std(samples, axis=1, ddof=1.5)

    fig1, ax1 = plt.subplots()
    fig1.set_figheight(fig1.get_figheight() * figfactor)
    fig1.set_figwidth(fig1.get_figwidth() * figfactor)

    ax1.plot(x, prob_means,
             #  'k--',
             linewidth=.5,
             label='CMC mean')
    ax1.fill_between(x,
                     prob_means - prob_stds,
                     prob_means + prob_stds,
                     alpha=0.5, label='CMC std',
                     )

    rv = 'y(x, \\beta)'
    xlabel = '$x$'
    ylabel = '${} \\pm {}$'.format(
            xe(rv), xstd(rv),
            )

    set_axis_labels_endaxes(xlabel, ylabel, ax1)
    fig1.savefig(
        './figs/presentation_logistic_diff_eq.pdf',
        format='pdf', transparent=True,
        )


if __name__ == "__main__":
    main()

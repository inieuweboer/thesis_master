from rainfall_model import *

if __name__ == "__main__":
    np.random.seed()

    params = (15, 0.2, 2, 3)
    ndays = 30
    #  rmax = 1000

    nruns = 10**10

    print('nruns = ', '{:,}'.format(nruns).replace(',', ' '))
    print('ndays = ', '{:,}'.format(ndays).replace(',', ' '))
    print()

    cumulative_rainfall_gen_outcomes(params, ndays=ndays, c=np.ones(nstations),
                                     nruns=nruns,
                                     fp=open("./out/rainfall_cmc_data", 'a'))

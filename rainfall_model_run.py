import multiprocessing
import os
import shutil
import time

from rainfall_model_cumulative_splitting import *

from helper_functions import (
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    )


def worker(work_args, times_dict):
    outfile, nsims, alg_dict, method, rfparams, rmax, ndays, alg_params, args_key = work_args

    with open(outfile, 'a+') as fp_meth:
        t = time.process_time()

        for _ in range(nsims):
            print(alg_dict[method](params=rfparams, rmax=rmax, ndays=ndays,
                                   alg_params=alg_params,
                  ),
                file=fp_meth)

        elapsed = time.process_time() - t

    print('name:')
    print(multiprocessing.current_process().name)
    print('Method:')
    print(method)
    #  print('nsims:')
    #  print('{:,}'.format(nsims).replace(',', ' '))
    print('Alg params:')
    print(alg_params)
    print('Process time:')
    print(elapsed)
    print('Dictionary entry:')
    print({args_key: elapsed})
    print()
    print()

    times_dict[args_key] = elapsed


def main():
    # Prepare multiprocessing
    manager = multiprocessing.Manager()
    times_dict = manager.dict()
    jobs = []

    # Get args
    args = rf_argparser()
    (datetimestr, nsims,
     L, T,
     nlevels, rfparams,
     outfolder,
     ) = (args.datetime, args.nsims,
          args.L, args.T,
          args.nlevels, args.rfparams,
          args.outfolder,
          )

    rmax = L
    ndays = T
    importance_fn_distance = make_importance_fn_distance(ndays, rmax)

    # c = 0.6275; gamma = 0.001; int(np.round(c * np.abs(np.log(gamma))))

    print('nsims = ', '{:,}'.format(nsims).replace(',', ' '))
    print('nlevels = ', '{:,}'.format(nlevels).replace(',', ' '))
    print()
    print('L = ', L)
    print('T = ', T)
    print()

    alg_dict = {
                'CMC': cumulative_rainfall_cmc,
                'FE': cumulative_rainfall_splitting_fixed_effort,
                'FNS': cumulative_rainfall_splitting_fixed_successes,
                'FO': cumulative_rainfall_splitting_fixed_offspring,
                }

    # Prepare filenames
    tofolder = outfolder
    if args.test:
        outfolder = outfolder + 'test/'
        tofolder = tofolder + 'test/'
    if not args.local:
        outfolder = '{}/multi_splitting_inb_{}/{}'.format(os.getenv("TMPDIR"),
                                                          datetimestr,
                                                          outfolder)
        tofolder = '{}/{}'.format(os.getenv("HOME"), tofolder)

    os.makedirs(outfolder, exist_ok=True)
    os.makedirs(tofolder, exist_ok=True)

    name = '_rf_'
    filename_out_pre = "".join([outfolder, datetimestr, name])
    filename_to_pre = "".join([tofolder, datetimestr, name])

    # Times printed to local file to parse them
    filename_times = "".join([tofolder, datetimestr, name + "times"])

    with open(filename_times, 'a+') as fp_time:
        for method in ['CMC', 'FE', 'FNS', 'FO', ]:
            for method_params in getattr(args, method):
                if method_params[0] < 0:
                    print('Skipping ', method)
                    break

                args_key = args_str_maker(
                    method, method_params,
                    nsims=nsims, L=L, T=T, nlevels=nlevels,
                    rfparams=rf_params_tostring(rfparams),
                    )
                outfile = filename_out_pre + args_key

                work_args = outfile, nsims, alg_dict, method, \
                    rfparams, rmax, ndays, \
                    (nlevels, *method_params, importance_fn_distance), \
                    args_key

                p = multiprocessing.Process(
                        target=worker, args=(work_args, times_dict))
                p.name = args_key
                jobs.append(p)
                p.start()

        for p in jobs:
            args_key = p.name
            outfile = filename_out_pre + args_key
            tofile = filename_to_pre + args_key

            p.join()

            # Move from scratch to local
            shutil.move(outfile, tofile)

            # Save partial time results
            print(times_dict, file=fp_time)
            print(args_key, 'joined!')
            print()


if __name__ == "__main__":
    np.random.seed()
    np.seterr(divide='ignore')

    main()

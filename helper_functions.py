import numpy as np
import matplotlib.pyplot as plt

from scipy import stats

import copy
from functools import reduce
from operator import xor


colors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']


def get_first(tup):
    if 'nan' in tup:
        return 0
    else:
        return eval(tup)[0]


#####
#  def args_str_maker(nsims, L, T, nlevels, method, params):
    #  return "nsims_{}_L_{}_T_{}_nlevels_{}_{}_{}".format(
    #      nsims, L, T, nlevels, method, '_'.join(map(str, params)))

def args_str_maker(method=None, method_params=None, **kwargs):
    if method is not None and method_params is not None:
        return "{}_{}_{}".format(
            method,
            '_'.join(map(str, method_params)),
            '_'.join("{}_{}".format(*kv) for kv in sorted(kwargs.items())),
            )
    else:
        return '_'.join("{}_{}".format(*kv) for kv in sorted(kwargs.items()))


def bm_splitting_argparser():
    import argparse, datetime

    parser = argparse.ArgumentParser()
    parser.add_argument('--datetime', type=str, default=datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
    parser.add_argument('--L', type=int, default=5)
    parser.add_argument('--T', type=int, default=30)
    parser.add_argument('--CMC', nargs='+', type=int, default=(-1, ))
    parser.add_argument('--FE',  nargs='+', type=int, default=(-1, ))
    parser.add_argument('--FNS', nargs='+', type=int, default=(-1, ))
    parser.add_argument('--FO',  nargs='+', type=int, default=(-1, -1))
    parser.add_argument('--nlevels', type=int, default=8)
    parser.add_argument('--nsims', type=int, default=100)
    parser.add_argument('--test', action='store_true')
    parser.add_argument('--local', action='store_true')

    args = parser.parse_args()
    assert len(args.FO) % 2 == 0, 'Need even amount of arguments for fixed amount of offspring'

    args.CMC = [(el, ) for el in args.CMC]
    args.FE = [(el, ) for el in args.FE]
    args.FNS = [(el, ) for el in args.FNS]
    args.FO = list(map(tuple, (args.FO[2*i: 2*i+2] for i in range(len(args.FO)//2))))

    return args


def rf_argparser():
    import argparse, datetime

    parser = argparse.ArgumentParser()
    parser.add_argument('--datetime', type=str, default=datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
    #  parser.add_argument('--datetime', type=str, required=True)
    parser.add_argument('--rfparams', default=rf_default_params())
    parser.add_argument('--L', type=int, default=1100)
    parser.add_argument('--T', type=int, default=30)

    parser.add_argument('--a', type=float, default=0.5)
    parser.add_argument('--b', type=float, default=1.5)

    # too slow
    #  parser.add_argument('--CMC', nargs='+', type=int, default=(np.power(10, 4), np.power(10, 5), np.power(10, 6)))
    #  parser.add_argument('--FE', nargs='+', type=int, default=(np.power(10, 2), np.power(10, 3), np.power(10, 4)))
    #  parser.add_argument('--FNS', nargs='+', type=int, default=(np.power(10, 2), np.power(10, 3), np.power(10, 4)))
    #  parser.add_argument('--FO', nargs='+', type=int, default=(100, 5, 100, 3, 100, 2))
    parser.add_argument('--CMC', nargs='+', type=int, default=(-1, ))
    parser.add_argument('--FE',  nargs='+', type=int, default=(-1, ))
    parser.add_argument('--FNS', nargs='+', type=int, default=(-1, ))
    parser.add_argument('--FO',  nargs='+', type=int, default=(-1, -1))
    parser.add_argument('--nlevels', type=int, default=4)
    parser.add_argument('--nsims', type=int, default=100)

    parser.add_argument('--gauss_quad_deg', type=int, default=8)
    parser.add_argument('--gpc_deg', type=int, default=8)
    parser.add_argument('--node_index', type=int, default=0)

    parser.add_argument('--test', action='store_true')
    parser.add_argument('--local', action='store_true')

    parser.add_argument('--simamt', type=int, default=-1)

    parser.add_argument('--Lmin', nargs='+', type=float, default=1000.0)
    parser.add_argument('--Lmax', nargs='+', type=float, default=1200.0)

    parser.add_argument('--Lindex', type=int, default=0)
    parser.add_argument('--Lamt', type=int, default=1)

    parser.add_argument('--sp_mult', type=int, default=4)

    parser.add_argument('--outfolder', type=str, default='out/rf/')

    args = parser.parse_args()
    assert len(args.FO) % 2 == 0, 'Need even amount of arguments for fixed amount of offspring'

    args.CMC = [(el, ) for el in args.CMC]
    args.FE = [(el, ) for el in args.FE]
    args.FNS = [(el, ) for el in args.FNS]
    args.FO = list(map(tuple, (args.FO[2*i: 2*i+2] for i in range(len(args.FO)//2))))

    if args.outfolder[-1] != '/':
        args.outfolder += '/'

    return args


def rf_default_params():
    rmin = 15
    alpha = 0.2
    beta1, beta2 = 2, 3
    betas = np.array([beta1, beta2])

    # params
    nstations = 3  # to keep calculation time a bit down
    ndays = 30  # one month, or a season, short-term predictivity

    #  np.random.seed(42)  # Seed for precip_params, Ucov, Vcov
    #  precip_params = np.random.sample((nstations, 2))  # [p_{0, 1}, p_{1, 1}] per weather station

    #  A_U = np.random.sample((nstations, nstations))
    #  A_V = np.random.sample((nstations, nstations))
    #  Ucov = A_U.T @ A_U  # replace with empirical correlations omega eq 9
    #  Vcov = A_V.T @ A_V  # replace with empirical correlations zeta eq 9, see also eq 17

    precip_params = np.array([[0.37454012, 0.95071431],
                              [0.73199394, 0.59865848],
                              [0.15601864, 0.15599452]])
    Ucov = np.array([[1.19770123, 0.24164609, 0.87304036],
                     [0.24164609, 0.79577273, 0.57924514],
                     [0.87304036, 0.57924514, 1.3351247 ]])
    Vcov = np.array([[0.23967225, 0.22234676, 0.41163473],
                     [0.22234676, 0.26272625, 0.44487312],
                     [0.41163473, 0.44487312, 0.78395428]])

    return list((rmin, alpha, betas, precip_params, Ucov, Vcov))


def rf_params_tostring(params):
    rmin, alpha, (beta1, beta2) = params[0:3]
    precip_params = params[3]
    Ucov = params[4]
    Vcov = params[5]

    #  hash_str = 'rmin_{}_alpha_{}_betas_{}_{}_pp_{}_Ucov_{}_Vcov_{}'.format(
    hash_str = '_'.join(map(str, [
            rmin, alpha, beta1, beta2,
            #  array_hasher(precip_params),
            #  array_hasher(Ucov),
            #  array_hasher(Vcov),
            array_hasher(np.concatenate(list(map(np.ndarray.flatten, [
                precip_params,
                Ucov,
                Vcov,
                ]))))
            ]))

    return hash_str


def rfparams_alpha(rfparams_def, work_nodes, node_index):
    rfparams = copy.copy(rfparams_def)
    rfparams[1] = work_nodes[node_index]

    return rfparams


def rfparams_ucov(rfparams_def, work_nodes, node_index):
    rfparams = copy.deepcopy(rfparams_def)
    rfparams[4] = work_nodes[node_index] * rfparams_def[4]

    return rfparams


def arg_rmax(Lmin, Lmax, Lindex, Lamt):
    assert Lindex < Lamt

    return np.linspace(Lmin, Lmax, Lamt)[Lindex]


def hash_pol(x):
    return x**3 + x + 1


def array_hasher(array):
    # These don't seem reliable
    #  return hash(str(array))
    #  return hash(array.tostring())

    return reduce(xor, hash_pol((10e16 * array).astype(int)).flatten().tolist())


#####


def max_abs_dist(array1, array2):
    return np.max(np.abs(array1 - array2))


def mean_abs_dist(array1, array2):
    return np.mean(np.abs(array1 - array2))


def mean_sq_dist(array1, array2):
    return np.mean(np.square(array1 - array2))


def calc_three_char_dists(array1, array2):
    return (max_abs_dist(array1, array2),
            mean_abs_dist(array1, array2),
            mean_sq_dist(array1, array2))


def calc_three_char_dists_re(true, approx):
    return (max_abs_dist(np.ones_like(true), approx / true),
            mean_abs_dist(np.ones_like(true), approx / true),
            mean_sq_dist(np.ones_like(true), approx / true))


def print_three_char_dists(array1, array2):
    print('Maximal absolute error:')
    print('{:.4g}'.format(max_abs_dist(array1, array2)))
    print('Mean absolute error:')
    print('{:.4g}'.format(mean_abs_dist(array1, array2)))
    print('Mean square error:')
    print('{:.4g}'.format(mean_sq_dist(array1, array2)))



def mve(samples):
    mean = np.mean(samples)
    var = np.var(samples, ddof=1)
    std = np.std(samples, ddof=1.5)

    return mean, var, std / mean


def mvec(samples, confidence=0.95):
    mean = np.mean(samples, axis=0)
    var = np.var(samples, ddof=1, axis=0)
    std = np.std(samples, ddof=1, axis=0)

    nsamples = samples.shape[0]
    sem = std / np.sqrt(nsamples)

    #  h = sem * stats.t.ppf((1 + confidence) / 2., nsamples-1)

    #  ci_l = mean - h
    #  ci_r = mean + h

    # Mean, variance, relative error, ci length, left ci bound, right ci bound
    #  return np.array([mean, var, std / mean, 2*h, ci_l, ci_r])

    return np.array([mean, var, std / mean, *stats.t.interval(confidence, nsamples-1, loc=mean, scale=sem)])


def calc_chars(samples, proc_time):
    mean = np.mean(samples, axis=0)
    nsamples = samples.shape[0]

    svar = np.var(samples, ddof=1, axis=0) / nsamples
    sre = np.std(samples, ddof=1, axis=0) / (np.sqrt(nsamples) * mean)

    return [mean, proc_time, svar, sre, svar*proc_time, sre*proc_time]

#####

def make_affine_transf(a, b, c, d):
    assert isinstance(a, (int, float))
    assert isinstance(b, (int, float))
    assert isinstance(c, (int, float))
    assert isinstance(d, (int, float))

    assert not np.allclose(a, b)
    assert not np.allclose(c, d)

    """
    Initializes and returns a affine transform
    f: [a, b] -> [c, d]
    """
    def f(x):
        return (d - c) * (x - a) / (b - a) + c

    return f

#####

def qq_plotter(samples1, samples2, nquantiles=100,
               ax=plt, edgecolors=None, linecolor='k', marker='o'):
    """
    Plots quantiles of empirical distribution functions (linearly interpolating
    between data points)
    """
    # Plot line y = x
    both = np.concatenate((samples1, samples2))
    #  both_min = np.min(both)
    #  both_max = np.max(both)
    #  both_len = both_max - both_min
    #  tweak = both_len / 10

    #  domain = np.linspace(np.min(both) - tweak, np.max(both) + tweak)
    domain = np.linspace(np.min(both), np.max(both))

    ax.plot(domain, domain, '-', linewidth=0.5, color=linecolor, zorder=1)

    # Plot quantiles
    quantile_space = np.linspace(0, 1, nquantiles)

    quantiles1 = np.quantile(samples1, quantile_space)
    quantiles2 = np.quantile(samples2, quantile_space)

    #  ax.scatter(quantiles1, quantiles2, marker=marker)
    if edgecolors is None:
        edgecolors = np.random.choice(colors)

    ax.scatter(quantiles1, quantiles2, marker=marker,
               s=20,
               facecolors='none',
               #  edgecolors='k',
               #  edgecolors=np.random.sample(3),
               edgecolors=edgecolors,
               linewidth=0.75,
               zorder=2,
               )


if __name__ == "__main__":
    np.random.seed(42)

    ## stuff

import multiprocessing
import time
import datetime

import numpy as np

from bm_splitting import exact_prob_bm
from helper_functions import calc_chars

from tabulate import tabulate


outfolder = "/scratch/multi_splitting/out/bm_cmc/"
#  outfolder = "./out/bm_cmc/"


def bm_cmc_bare(L, T, alg_params=(0.01, 10**2), fp=None):

    stepsize, nruns = alg_params

    if stepsize is None:
        stepsize = 0.01

    nsteps = int(T / stepsize)

    for _ in range(nruns):
        pos = 0
        #  trajectory = [pos]

        #  Pre-sample normally distributed steps
        normal_samples = iter(np.random.normal(scale=np.sqrt(stepsize), size=nsteps))

        for _ in range(nsteps):
            pos += next(normal_samples)
            #  trajectory.append(pos)

            if pos >= L:
                print(1, file=fp)
                break
        else:
            print(0, file=fp)


def worker(work_args, times_dict):
    L, T, i, dt_str, key = work_args

    nruns = 10**i
    #  print('nruns = ', '{:,}'.format(nruns).replace(',', ' '))

    filename_out = "{}{}_bm_cmc_{}".format(outfolder, dt_str, nruns)

    with open(filename_out, 'a+') as fp:
        t = time.process_time()
        bm_cmc_bare(L=L, T=T,
                    alg_params=(None, nruns),
                    fp=fp
                    )
        elapsed = time.process_time() - t

    print('Method:')
    print('CMC')
    print('nruns:')
    print('{:,}'.format(nruns).replace(',', ' '))
    print('Process time:')
    print(elapsed)

    times_dict[key] = elapsed



def data_gen(L, T, dt_str, exps, jobs, times_dict):
    for i in exps:
        key = 'CMC_{}'.format(i)
        work_args = L, T, i, dt_str, key

        p = multiprocessing.Process(
                target=worker, args=(work_args, times_dict))
        jobs.append(p)
        p.start()

        #  print('Elapsed time for ', nruns, ': ', elapsed)

    for p in jobs:
        p.join()



def data_valid(L, T, exps, times_dict):

    exact_prob = exact_prob_bm(L, T)

    table = []

    for i in exps:
        nruns = 10**i
        key = 'CMC_{}'.format(i)

        filename_in = "{}{}_bm_cmc_{}".format(outfolder, dt_str, nruns)

        with open(filename_in, 'r') as fp_in:
            fp_in.seek(0)
            hits = np.array([eval(line) for line in fp_in])

            prob_chars = calc_chars(hits, times_dict[key])
            prob_bias = np.abs(prob_chars[0] - exact_prob)
            prob_chars = np.insert(prob_chars, 1, prob_bias)

            row = ['CMC $10^{{{}}}$'.format(i)]
            row.extend(prob_chars)

            table.append(row)

    filename_out = "{}bm_cmc_validation{}".format(outfolder, dt_str)

    with open(filename_out, 'a+') as fp_out:
        print('Datetime simulation: ', dt_str, file=fp_out)
        print('Datetime now: ', datetime.datetime.now().strftime("%Y%m%d_%H%M%S"), file=fp_out)
        print(file=fp_out)

        ### Workaround! Cannot make a full row of strings without destroying the formatting of floats
        ### only need to delete 4 lines from outfile with this solution
        #  print(tabulate(first_row, tablefmt="latex"), file=fp_out)

        print(tabulate(table, tablefmt="latex_raw", floatfmt=".4g"), file=fp_out)


if __name__ == "__main__":
    # Prepare multiprocessing
    manager = multiprocessing.Manager()
    times_dict = manager.dict()
    jobs = []

    dt_str = str(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
    print('Datetime: ', dt_str)

    np.random.seed()
    #  np.seterr(divide='ignore')

    #  L = 5
    L = 26
    T = 30

    print('L = ', L)
    print('T = ', T)

    #  exps = [3, 5, 7]
    exps = [1, 2]

    data_gen(L, T, dt_str, exps, jobs, times_dict)

    data_valid(L, T, exps, times_dict)

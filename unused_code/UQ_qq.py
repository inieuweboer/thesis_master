
def gpc_dists_errors(L=18, T=30, K=10, M=10, a=-1, b=1, printerrors=True):
    pass

    # ...

    fig_qq_interp, ax_qq_interp = plt.subplots()

    qq_plotter(cmc_mean, gpc_interp_mean, nquantiles=100,
               ax=ax_qq_interp,
               edgecolors='darkred')

    xlabel = 'CMC quantiles'
    ylabel = 'gPC interp quantiles'
    set_axis_labels(xlabel, ylabel, ax_qq_interp)

    fig_qq_interp.savefig(
        './figs/QQ_CMC_gPC_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format('interp_mean',
                                         T, L, K, M, a, b),
        format='pdf',
        #  bbox_inches='tight',
        )

    fig_qq_dp, ax_qq_discproj = plt.subplots()

    qq_plotter(cmc_mean, gpc_dp_mean, nquantiles=100,
               ax=ax_qq_discproj,
               edgecolors='darkblue')

    xlabel = 'CMC quantiles'
    ylabel = 'gPC disc. proj. quantiles'
    set_axis_labels(xlabel, ylabel, ax_qq_discproj)

    fig_qq_dp.savefig(
        './figs/QQ_CMC_gPC_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format('dp_mean',
                                         T, L, K, M, a, b),
        format='pdf',
        #  bbox_inches='tight',
        )


import numpy as np

from helper_functions import mvec


if __name__ == "__main__":
    filename_in = 'server/out/bm_cmc'
    filename_out = 'server/out/bm_cmc_mve'

    print('start')
    with open(filename_in, 'r') as fp_in, open(filename_out, 'a+') as fp_out:
        for i in range(4, 8):
            N = 10**i
            ests = np.array([eval(next(fp_in)) for _ in range(N)])

            print('Empirical mean, sample variance, relative error', file=fp_out)
            print('for N = ', N, file=fp_out)
            print(mvec(ests), file=fp_out)
            fp_in.seek(0)

    print('done')

import os
import shutil

from helper_functions import (
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    rfparams_alpha, rfparams_ucov,
    )
from rainfall_model_cumulative_splitting import *
from UQ_methods import gpc_approx_discrete_proj_uniform_tree_nodes


def main():
    # Get args
    args = rf_argparser()

    (L, T, nlevels, rfparams_def,
     gauss_quad_deg,
     a, b,
     outfolder,
     ) = (
          args.L, args.T, args.nlevels, args.rfparams,
          args.gauss_quad_deg,
          args.a, args.b,
          args.outfolder,
          )

    work_nodes = gpc_approx_discrete_proj_uniform_tree_nodes(
        a=a, b=b,
        gauss_quad_deg=gauss_quad_deg,  # M
        )

    # Prepare filenames
    if args.test:
        outfolder = outfolder + 'test/'

    os.makedirs(outfolder, exist_ok=True)

    name = '_rf_'
    filename_merged_pre = "".join([outfolder, 'merged', name])

    for node_index in range(gauss_quad_deg):
        #  rfparams = rfparams_ucov(rfparams_def, work_nodes, node_index)
        rfparams = rfparams_alpha(rfparams_def, work_nodes, node_index)

        args_key = args_str_maker(
            L=L, T=T, nlevels=nlevels,
            rfparamshash=rf_params_tostring(rfparams),
            )

        mergedfile = filename_merged_pre + args_key
        infiles = [f for f in os.listdir(outfolder) if f.endswith(args_key)]

        # Overwrites existing file
        with open(mergedfile, 'wb') as fp_merged:
            for infile in infiles:
                infilename = outfolder + infile

                with open(infilename, 'rb') as fp_in:
                    shutil.copyfileobj(fp_in, fp_merged)


if __name__ == "__main__":
    main()

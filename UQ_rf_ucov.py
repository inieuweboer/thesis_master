import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d

from helper_functions import (
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    rfparams_alpha, rfparams_ucov,
    arg_rmax,
    )
from rainfall_model_cumulative_splitting import *
from UQ_methods import *

from text_render_latex import *
latexify(columns=2)


def gpc_rf_prob_fit(rmax=800, sp_mult=20):
    # Get args
    args = rf_argparser()

    (T, nlevels, rfparams_def,
     M, K,
     a, b,
     Lmin, Lmax, Lamt,
     outfolder,
     ) = (
          args.T, args.nlevels, args.rfparams,
          args.gauss_quad_deg, args.gpc_deg,
          args.a, args.b,
          args.Lmin, args.Lmax, args.Lamt,
          args.outfolder,
          )

    name = '_rf_'
    filename_merged_pre = "".join([outfolder, 'merged', name])

    work_nodes = gpc_approx_discrete_proj_uniform_tree_nodes(
        a=a, b=b,
        gauss_quad_deg=M,
        )

    model_fn_arr = []

    for Lindex in range(Lamt):
        rmax = arg_rmax(Lmin, Lmax, Lindex, Lamt)

        model_fn_vec = []

        for node_index in range(M):
            rfparams = rfparams_ucov(rfparams_def, work_nodes, node_index)

            args_key = args_str_maker(
                #  L=L, T=T, nlevels=nlevels,
                L=rmax, T=T, nlevels=nlevels,
                rfparamshash=rf_params_tostring(rfparams),
                )

            mergedfile = filename_merged_pre + args_key

            with open(mergedfile, 'r') as fp_merged:
                ests = np.loadtxt(fp_merged)
                if len(ests) != 0:
                    model_fn_vec.append(np.mean(ests))
                else:
                    model_fn_vec.append(0)

        model_fn_arr.append(model_fn_vec)

    model_fn_arr = np.array(model_fn_arr)

    fcs, norms_sq = gpc_approx_discrete_proj_param_uniform_tree_from_data(
        model_fn_arr,
        a=a, b=b,
        gauss_quad_deg=M, gpc_deg=K,
        )

    print(model_fn_arr.shape)

    means = fcs[:, 0]
    stds = np.sqrt(np.sum(
        fcs[:, 1:]**2 * norms_sq[1:], axis=1))

    Lobsvs = np.linspace(Lmin, Lmax, Lamt)

    means_interp = interp1d(Lobsvs, means, kind='cubic')
    stds_interp = interp1d(Lobsvs, stds, kind='cubic')

    Lspace = np.linspace(Lmin, Lmax, int((Lmax - Lmin)*sp_mult) + 1)
    means_plot = means_interp(Lspace)
    stds_plot = stds_interp(Lspace)

    #####
    # Plot true and approx using both naive gPC and func gPC approx
    fig_L, ax_L = plt.subplots()

    ax_L.plot(Lspace, means_plot,
              linewidth=.5,
              label='Mean')
    ax_L.fill_between(Lspace,
                      means_plot - stds_plot,
                      means_plot + stds_plot,
                      alpha=0.5, label='Std')


    ax_L.legend()
    #  ax1.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    #  ax1.axhline(1, linestyle=loosely_dotted, color='k', linewidth=.25)

    figname = 'normal'
    fig_L.savefig(
        '../figs/UQ_rf_Ucov_rmax_gpc_{}_T_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, K, M, a, b),
        format='pdf',
        )



if __name__ == "__main__":
    gpc_rf_prob_fit()

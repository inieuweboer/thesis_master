import matplotlib.pyplot as plt
import numpy as np

from orthopy.line_segment import tree_jacobi, tree_legendre
from quadpy import quad
from scipy import stats
from scipy.special import roots_jacobi, factorial
from scipy.special import gamma as gamma_fn


def gpc_approx_discrete_proj_jacobi(model_fn, a=-1, b=1, calpha=0, cbeta=0,
                                    gauss_quad_deg=10, gpc_deg=10,  # M, K
                                    assert_norms=False,
                                    ):
    """
    calpha and cbeta > -1 from Jacobi convention
    Defaults to Legendre approx
    """
    assert calpha > -1, 'calpha should be greater than -1'
    assert cbeta > -1, 'cbeta should be greater than -1'

    # Transformation from support of Jacobi polynomials and back
    affine_fn = make_affine_transf(-1, 1, a, b)
    affine_fn_inv = make_affine_transf(a, b, -1, 1)

    def orthopolval(x):
        return np.array(tree_jacobi(
            X=x, n=gpc_deg, alpha=calpha, beta=cbeta,
            standardization="p(1)=(n+alpha over n)"))

    def orthopolgauss(deg):
        return roots_jacobi(deg, calpha, cbeta, mu=True)

    # Jacobi quad
    z_nodes, weights, wsum = orthopolgauss(gauss_quad_deg)

    # Coefs for Jacobi polynomial expansion
    N = np.arange(gpc_deg + 1)
    c = calpha + cbeta + 1
    norms_sq = np.power(2, c) * gamma_fn(N + calpha + 1) * gamma_fn(N + cbeta + 1) \
        / ((2*N + c) * gamma_fn(N + c) * factorial(N))
    norms_sq_wsum = norms_sq * wsum

    if assert_norms:
        norms_quad = quad(lambda u: orthopolval(u)**2 *
                          np.power(1-u, calpha)*np.power(1+u, cbeta), -1, 1)[0]
        assert np.allclose(norms_sq, norms_quad)

    Phi_km = orthopolval(z_nodes)

    # Must have that nodes are zeros of highest pol order
    assert np.allclose(Phi_km[-1], 0)

    model_fn_vec = model_fn(affine_fn(z_nodes))

    fourier_coefs = Phi_km @ (model_fn_vec * weights) / norms_sq_wsum

    def gpc_approx_fn(z):
        return fourier_coefs @ orthopolval(affine_fn_inv(z))

    return gpc_approx_fn, fourier_coefs, norms_sq

#####

def cmc_approx_jacobi(model_fn, calpha=0, cbeta=0, nsims=100):
    affine_fn = make_affine_transf(1, 0, -1, 1)

    Bspace = np.random.beta(a=calpha + 1, b=cbeta + 1, size=nsims)
    samples = model_fn(affine_fn(Bspace))

    return np.mean(samples), np.std(samples, ddof=1.5)

#####

def test_jacobi(model_fn=lambda x: np.sin(np.exp(-x))):
    calpha, cbeta = 2, 5

    # only when normalized, but numerically unstable
        #  standardization="normal"))
    #  norm_sq = quad(lambda u: orthopolval(u)[0]**2 * stats.beta.pdf(u, a=calpha, b=cbeta), 0, 1)[0]
    #  norms_sq = np.full(shape=gpc_deg + 1, fill_value=norm_sq)

    print('jacobi gpc, cmc')
    g_jacobi, fcs_jacobi, hsq_jacobi = gpc_approx_discrete_proj_jacobi(
        model_fn,
        #  a=a, b=b,
        calpha=calpha, cbeta=cbeta,
        gpc_deg=10,
        assert_norms=True,
        )

    cmc_jacobi_mean, cmc_jacobi_std = cmc_approx_jacobi(
        model_fn, calpha=calpha, cbeta=cbeta, nsims=np.power(10, 6))

    gpc_jacobi_mean = fcs_jacobi[0]
    gpc_jacobi_std = np.sqrt(np.nansum(fcs_jacobi[1:]**2 * hsq_jacobi[1:]))

    print('mean comparison ', gpc_jacobi_mean, cmc_jacobi_mean)
    print('std comparison ', gpc_jacobi_std, cmc_jacobi_std)
    print('std comparison /', gpc_jacobi_std / cmc_jacobi_std)
    print('std comparison -', gpc_jacobi_std - cmc_jacobi_std)
    print('FIXME std klopt niet')
    print()

    #####

def make_affine_transf(a, b, c, d):
    assert isinstance(a, (int, float))
    assert isinstance(b, (int, float))
    assert isinstance(c, (int, float))
    assert isinstance(d, (int, float))

    assert not np.allclose(a, b)
    assert not np.allclose(c, d)

    """
    Initializes and returns a affine transform
    f: [a, b] -> [c, d]
    """
    def f(x):
        return (d - c) * (x - a) / (b - a) + c

    return f

if __name__ == "__main__":
    test_jacobi()

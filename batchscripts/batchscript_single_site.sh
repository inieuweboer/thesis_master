#!/bin/bash
#SBATCH --job-name=RF_S_IMP
# py='~/.virtualenvs/math/bin/python'  # py3 and latest versions of numpy, scipy
#SBATCH -N 1
#SBATCH --time=120:00:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: RF_S_IMP"

case $HOSTNAME in
  (antergos-sopropo) pyth="python"; work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl) echo "should not run on ${HOSTNAME} without sbatch, exit"; exit 1;;
  (*) pyth="$HOME/.virtualenvs/math/bin/python"; work_dir="$HOME/thesis_rainfall";;  # py3 and latest versions of numpy, scipy
esac

time ~/.virtualenvs/math/bin/python ~/thesis_rainfall/rainfall_model_single_site.py
time ${pyth} "${work_dir}/rainfall_model_single_site.py"
echo end of job

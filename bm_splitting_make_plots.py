from bm_splitting import *

from text_render_latex import latexify, set_axis_labels_endaxes
latexify(columns=2)


if __name__ == "__main__":
    rand = np.random.randint(1000)
    print(rand)
    np.random.seed(rand)
    #  np.random.seed(659)  # cmc
    #  np.random.seed(398)  # fe
    #  np.random.seed(273)  # fns
    #  np.random.seed(337)  # fo

    #  np.random.seed(610)  # fe and fns

    np.seterr(divide='ignore')

    L = 5
    #  L = 10
    T = 30

    stepsize = 0.25

    optimize = False
    show = False
    save = True

    plt.figure()
    while True:
        est = bm_cmc(L=L, T=T,
           alg_params=(stepsize, 1),
           show=show, save=save,
           )

        if est > 0:
            break
        else:
            plt.clf()

    plt.xlim([0, T*1.01])
    plt.ylim([-L/10, L*1.25])
    xlabel = '$t$'
    ylabel = '$x$'
    set_axis_labels_endaxes(xlabel, ylabel)

    plt.savefig('./figs/presentation_bm_cmc_intro.pdf',
                format='pdf', transparent=True,
                )
    #####

    plt.figure()
    est = bm_cmc(L=L, T=T,
        alg_params=(stepsize, 3),
        show=show, save=save,
        )

    plt.xlim([0, T*1.01])
    plt.ylim([-L/10, L*1.25])
    xlabel = '$t$'
    ylabel = '$x$'
    set_axis_labels_endaxes(xlabel, ylabel)

    plt.savefig('./figs/bm_cmc.pdf',
                format='pdf', transparent=True,
                )
    #####

    plt.figure()
    bm_splitting_fixed_offspring(L=L, T=T,
            alg_params=(stepsize, 4, 1, 2, None),
            show=show, save=save,
            )
    plt.savefig('./figs/bm_splitting_fo.pdf',
                format='pdf', transparent=True,
                )
    #####

    plt.figure()
    bm_splitting_fixed_effort(L=L, T=T,
            alg_params=(stepsize, 3, 3, None),
            show=show, save=save,
            )
    plt.savefig('./figs/bm_splitting_fe.pdf',
                format='pdf', transparent=True,
                )
    #####

    plt.figure()
    bm_splitting_fixed_successes(L=L, T=T,
            alg_params=(stepsize, 3, 3, None),
            #  alg_params=(stepsize, 3, 10, importance_fn_brownian_motion),
            show=show,
            optimize=optimize,
            save=save,
            )
    plt.savefig('./figs/bm_splitting_fns.pdf',
                format='pdf', transparent=True,
                )
    #  plt.savefig('./figs/bm_splitting_fns_opt_no.pdf', format='pdf')

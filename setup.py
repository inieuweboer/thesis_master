from setuptools import setup

setup(
   version='1.0',
   description='Code for MSc thesis: "Rare event simulation and uncertainty quantification of a rainfall model"',
   author='Ismani Nieuweboer',
   install_requires=['numpy', 'scipy', 'matplotlib', 'opt-einsum', 'quadpy', 'orthopy', 'tabulate'], #external packages as dependencies
)

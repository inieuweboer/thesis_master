import matplotlib.pyplot as plt
import numpy as np

### https://stackoverflow.com/questions/45597092/expanded-legend-over-2-subplots

fig, axs = plt.subplots(1, 2,
                        #  constrained_layout=True,
                        )
axs[0].plot(np.arange(10), label='First plot')
axs[1].plot(np.arange(10), label='Second plot')

handles = []
labels = []

for ax in axs.flatten():
    h, l = ax.get_legend_handles_labels()
    handles.extend(h)
    labels.extend(l)

s = fig.subplotpars
axs[0].legend(handles, labels,
              bbox_to_anchor=(0., 1.02, 2 + s.wspace, .1), loc='upper left',
              ncol=4, mode="expand", borderaxespad=0,
              #  fancybox=False, edgecolor="k",
              )

fig.savefig('figs/testlegend.png',
            bbox_inches='tight',
            #  dpi=100
            )

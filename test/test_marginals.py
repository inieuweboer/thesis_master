import numpy as np
from scipy import stats
import matplotlib.pyplot as plt


np.random.seed(42)

nstations = 25
#  ndays = 10

# params
precip_params = np.random.sample((nstations, 2))
L = np.triu(np.ones((nstations, nstations)))
mean = np.zeros(nstations)
cov = L.T @ L  # replace this with correlations

#  U = stats.norm.cdf(np.random.multivariate_normal(mean, cov))
# Natuurlijk krijg je geen diagonale QQ plot. Dat zou gek zijn als je gecorreleerde uniforme samplet.
#  fig = plt.figure()
#  ax = fig.add_subplot(111)
#  stats.probplot(U, dist=stats.uniform, plot=ax)
#  plt.show()
### of
#  norm = stats.norm()
#  x_unif = norm.cdf(x)
#  h = sns.jointplot(U[:, 0], U[:, 1], kind='hex', stat_func=None)
#  h.set_axis_labels('Y1', 'Y2', fontsize=16);

# Je hoort wel een diagonaal te krijgen als je de marginale samplet.
samples = []

for _ in range(1000):
    # Simulate from Gaussian copula
    U = stats.norm.cdf(np.random.multivariate_normal(mean, cov))
    samples.append(U[0])

fig = plt.figure()
ax = fig.add_subplot(111)
stats.probplot(samples, dist=stats.uniform, plot=ax)
plt.show()

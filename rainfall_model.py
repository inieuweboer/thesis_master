import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

from itertools import cycle

from importance_functions import (plot_levels,
                                  make_importance_fn_brownian_motion,
                                  make_importance_fn_distance,
                                  make_importance_fn_large_deviation,
                                  )
from helper_functions import mve, mvec, rf_default_params
from optimize_levels import optimize_levels


nstations = 3
mean = np.zeros(nstations)

# Alternative, see https://math.stackexchange.com/a/358092
#  Ucov = (A_U.T + A_U) / 2 + nstations * np.eye(nstations)
#  Vcov = (A_V.T + A_V) / 2 + nstations * np.eye(nstations)

#  v_U = np.random.sample(nstations); v_U /= np.linalg.norm(v_U)
#  v_V = np.random.sample(nstations); v_V /= np.linalg.norm(v_V)
#  Q_U = np.eye(nstations) - 2*np.outer(v_U, v_U)
#  Q_V = np.eye(nstations) - 2*np.outer(v_V, v_V)
#  S_U = np.diag(np.random.sample(nstations))
#  S_V = np.diag(np.random.sample(nstations))
#  Ucov = Q_U @ S_U @ Q_U.T  # replace with empirical correlations omega eq 9
#  Vcov = Q_V @ S_V @ Q_V.T  # replace with empirical correlations zeta eq 9, see also eq 17


# Helper functions
def sliding_beta(U, alpha, prob_crit, betas):
    return betas[(U <= alpha*prob_crit).astype(int)]  # eq 11


def sliding_beta_alt(U, alpha, prob_crit, betas):
    return betas[1] + (U <= alpha*prob_crit).astype(int) * 2 * \
            (betas[0] - betas[1])*(1 - U/(alpha*prob_crit))


def one_step_multisite(params, X, sliding_beta_fn=sliding_beta):
    """
    Do one step of the multisite model and return occurence and amounts vector
    """

    rmin, alpha, betas, precip_params, Ucov, Vcov = params

    # eq 4, get vector of critical probabilities (per weather station)
    # for today depending on occurence of yesterday
    prob_crit = precip_params[np.arange(nstations), X]

    # Two uniformly distributed random vectors, correlated using
    # Gaussian copula
    U = stats.norm.cdf(np.random.multivariate_normal(mean, Ucov))
    V = stats.norm.cdf(np.random.multivariate_normal(mean, Vcov))

    # eq 5, generate new boolean/bernoulli occurences vector of dry/wet day
    X = (U <= prob_crit).astype(int)

    # eq 11, 12, get vector of beta parameters
    # implementing the mixed exponential
    beta_v = sliding_beta_fn(U, alpha, prob_crit, betas)

    # eq 8, inversion
    R = rmin - beta_v*np.log(V)

    return X, R


def one_step_multisite_cumulative(
        X, tot_rf, params, c=None, sliding_beta_fn=sliding_beta):

    """
    Do one step of the multisite model and return occurence and amounts vector
    """
    rmin, alpha, betas, precip_params, Ucov, Vcov = params

    if c is None:
        c = np.ones(nstations)

    # eq 4, get vector of critical probabilities (per weather station)
    # for today depending on occurence of yesterday
    prob_crit = precip_params[np.arange(nstations), X]

    # Two uniformly distributed random vectors, correlated using
    # Gaussian copula
    U = stats.norm.cdf(np.random.multivariate_normal(mean, Ucov))
    V = stats.norm.cdf(np.random.multivariate_normal(mean, Vcov))

    # eq 5, generate new boolean/bernoulli occurences vector of dry/wet day
    X = (U <= prob_crit).astype(int)

    # eq 11, 12, get vector of beta parameters
    # implementing the mixed exponential
    beta_v = sliding_beta_fn(U, alpha, prob_crit, betas)

    # eq 8, inversion
    R = rmin - beta_v*np.log(V)

    Y = R * X
    tot_rf += c @ Y

    return X, tot_rf


def one_step_single_site(params, X, site, sliding_beta_fn=sliding_beta):
    """
    Do one step of the single site model and return occurence and amounts vector
    """

    rmin, alpha, betas, precip_params, _, _ = params

    U = np.random.uniform()

    # eq 4, get vector of critical probabilities (per weather station)
    # for today depending on occurence of yesterday
    prob_crit = precip_params[site, X]

    # eq 5, generate new boolean/bernoulli occurences vector of dry/wet day
    X = (U <= prob_crit).astype(int)

    # eq 11, 12, get vector of beta parameters
    # implementing the mixed exponential
    beta_v = sliding_beta_fn(U, alpha, prob_crit, betas)

    Y = rmin + np.random.exponential(beta_v)

    return X, Y


def one_step_single_site_importance(params, X, site, shift):
    """
    Do one step of the single site model for importance sampling and return occurence and amounts vector
    """

    rmin, alpha, betas, precip_params, _, _ = params

    U = np.random.uniform()

    # eq 4, get vector of critical probabilities (per weather station)
    # for today depending on occurence of yesterday
    prob_crit = precip_params[site, X]

    # eq 5, generate new boolean/bernoulli occurences vector of dry/wet day
    X = (U <= prob_crit).astype(int)

    # eq 11, 12, get vector of beta parameters
    # implementing the mixed exponential
    beta_v = sliding_beta(U, alpha, prob_crit, betas)

    sample = rmin + np.random.exponential(shift)

    return X, sample, beta_v


def one_step_single_site_occ(params, X, site):
    """
    Do one step of the single site model for importance sampling and return occurence vector and critical probability
    """

    U = np.random.uniform()

    # eq 4, get vector of critical probabilities (per weather station)
    # for today depending on occurence of yesterday
    prob_crit = precip_params[site, X]

    # eq 5, generate new boolean/bernoulli occurences vector of dry/wet day
    X = (U <= prob_crit).astype(int)

    return X, np.take(prob_crit, 0)
# End helper functions


#  Test function to generate both occurences and amounts at the same time
def gen_occurrences_amounts(params):
    nstations = params[3].shape[0]

    # Start dry
    XR = []
    XR.append((np.zeros(nstations, dtype=int), []))

    for _ in range(ndays):
        X, R = one_step_multisite(params, XR[-1][0])
        XR.append((X, Y))

    return XY


##########


def extreme_rainfall_est_cmc(params, rmax, c=None):
    """
    c is such that the sum is taken, but could also look more localized

    Returns sample mean and sample variance with Bessel's correction
    """
    if c is None:
        c = np.ones(nstations)

    # Start dry
    X = np.zeros(nstations, dtype=int)

    samples = []

    for _ in range(ndays):
        X, R = one_step_multisite(params, X)

        # Add to total
        Y = R * X
        QoI = c @ Y
        samples.append(int(QoI >= rmax))

    return mve(samples)


# This is for the limit and does not include ndays!
def extreme_rainfall_single_site_est_exact(params, rmax, ndays, site):
    rmin, alpha, betas, precip_params, _, _ = params
    beta1, beta2 = betas

    P = np.exp((rmin - rmax) / beta1) * alpha / beta1 + \
        np.exp((rmin - rmax) / beta2) * (1-alpha) / beta2

    p01, p11 = precip_params[site]

    stat_prob = p01 / (p01 + 1 - p11)

    return stat_prob * P


def extreme_rainfall_single_site_est_only_occ(params, rmax, ndays, site):
    rmin, alpha, betas, _, _, _ = params
    beta1, beta2 = betas

    P = np.exp((rmin - rmax) / beta1) * alpha / beta1 + \
        np.exp((rmin - rmax) / beta2) * (1-alpha) / beta2

    # Start dry
    X = 0

    samples = []

    for _ in range(ndays):
        X, prob_crit = one_step_single_site_occ(params, X, site)

        samples.append(prob_crit * P)

    return mve(samples)


def extreme_rainfall_single_site_est_cmc(params, rmax, ndays, site):
    # Start dry
    X = np.zeros(1, dtype=int)

    samples = []

    for _ in range(ndays):
        X, Y = one_step_single_site(params, X, site)
        samples.append(int(Y >= rmax))

    return mvec(samples)


def extreme_rainfall_single_site_est_importance(params, rmax, ndays, site):
    rmin, alpha, betas, _, _, _ = params
    beta1, beta2 = betas

    b1 = 1/beta1 - 1/rmax
    b2 = 1/beta2 - 1/rmax

    # Start dry
    X = 0

    samples = []

    for _ in range(ndays):
        X, sample, beta_v = one_step_single_site_importance(
            params, X, site, rmax)

        if sample >= rmax:
            s = rmin - sample

            samples.append(rmax * ((alpha / beta1) * np.exp(b1*s) +
                           ((1 - alpha) / beta2) * np.exp(b2*s)))
        else:
            samples.append(0)

    return mvec(samples)


##########


def cumulative_rainfall_gen_paths(
        params, ndays=30, nruns=np.power(10, 10), fp=None):

    amounts = []

    # Start dry
    X = np.zeros(nstations, dtype=int)

    for _ in range(nruns):
        # Start dry
        X = np.zeros(nstations, dtype=int)
        rainfall = np.zeros(nstations)
        print(tuple(rainfall), file=fp)

        for _ in range(ndays):
            X, R = one_step_multisite(params, X)

            # Add to total
            Y = R * X
            rainfall += Y
            print(tuple(rainfall), file=fp)


def cumulative_rainfall_gen_outcomes(
        params, ndays=30, c=None, nruns=10**10, fp=None):

    """
    c is such that the sum is taken, but could also look more localized
    """
    if c is None:
        c = np.ones(nstations)

    samples = []

    for _ in range(nruns):
        # Start dry
        X = np.zeros(nstations, dtype=int)
        rainfall = 0

        for _ in range(ndays):
            X, R = one_step_multisite(params, X)

            # Add to total
            Y = R * X
            rainfall += c @ Y

        print(rainfall, file=fp)


###############################################################################
def run_extreme_rainfall_single_site(params):
    np.random.seed(1)  # get same results

    import time

    # Estimate extreme rainfall
    #  print(extreme_rainfall_est_cmc(
    #        params=(15, np.full(nstations, 0.2), 2, 3), rmax=25))

    rmax = 25

    ndays = np.power(10, 3)
    print('ndays = ', '{:,}'.format(ndays).replace(',', ' '))

    #  print(extreme_rainfall_single_site_est_exact(
    #        params=params, rmax=rmax, ndays=ndays, site=0))
    #  print(extreme_rainfall_single_site_est_only_occ(
    #        params=params, rmax=rmax, ndays=ndays, site=0))
    t = time.process_time()
    print(extreme_rainfall_single_site_est_cmc(
          params=params, rmax=rmax, ndays=ndays, site=0))
    elapsed = time.process_time() - t
    print(elapsed)

    t = time.process_time()
    print(extreme_rainfall_single_site_est_importance(
          params=params, rmax=rmax, ndays=ndays, site=0))
    print(elapsed)


    ##


if __name__ == "__main__":
    np.random.seed()

    params = rf_default_params()

    cumulative_rainfall_gen_paths(params, nruns=np.power(10, 3))

import matplotlib.pyplot as plt
import numpy as np

from text_render_latex import *
latexify(columns=2)

"""
#1
#RE
CMC = [np.nan, 0.04331, np.nan, 0.004236, 0.0004256]
FE  = [0.01084, 0.003535, 0.00115, np.nan, np.nan]
FNS = [0.01062, 0.003512, 0.001084, np.nan, np.nan]
# PTN Var
CMC = [np.nan, 0.001198, np.nan, 0.0012, 0.001194]
FE  = [0.0005633, 0.0005665, 0.0005836, np.nan, np.nan]
FNS = [0.0006223, 0.0006348, 0.0005835, np.nan, np.nan]
# PTN RE
#  CMC = [np.nan, 0.2284, np.nan, 2.213, 22.18]
#  FE  = [0.3995, 1.262, 4, np.nan, np.nan]
#  FNS = [0.4455, 1.426, 4.248, np.nan, np.nan]


#2
#RE
CMC = [np.nan, np.nan, np.nan, 0.09617, 0.01014]
FE  = [0.04734, 0.01629, 0.004523, np.nan, np.nan]
FNS = [0.0338, 0.01033, 0.003297, np.nan, np.nan]
# PTN Var
CMC = [np.nan, np.nan, np.nan,   1.502e-06, 1.339e-06]
FE  = [1.225e-07, 1.488e-07, 1.099e-07, np.nan, np.nan]
FNS = [1.403e-07, 1.25e-07, 1.244e-07, np.nan, np.nan]
# PTN RE
#  CMC = [np.nan, np.nan, np.nan, 13.39, 139.9]
#  FE  = [2.772, 9.402, 25.42, np.nan, np.nan]
#  FNS = [4.228, 12.65, 38.65, np.nan, np.nan]


#3
#RE
CMC = [np.nan, np.nan, np.nan, 1, 0.2182]
FE  = [0.1371, 0.04238, 0.01321, np.nan, np.nan]
FNS = [0.07725, 0.02013, 0.006135, np.nan, np.nan]
# PTN Var
CMC = [np.nan, np.nan, np.nan, 1.397e-08, 2.911e-09]
FE  = [2.318e-12, 3.665e-12, 3.928e-12, np.nan, np.nan]
FNS = [6.935e-12, 4.013e-12, 3.354e-12, np.nan, np.nan]
# PTN RE
#  CMC = [np.nan, np.nan, np.nan, 139.7, 3025]
#  FE  = [7.914, 24.4, 74.66, np.nan, np.nan]
#  FNS = [18.98, 47.95, 138.6, np.nan, np.nan]
"""
                    #  's--',
                    #  lw=0.5,
                    #  markersize=4,
                    #  label='blank',
    #  axes[0, 1].legend()

if __name__ == "__main__":
    fig, axes = plt.subplots(nrows=3, ncols=2)
    fig.set_figheight(fig.get_figheight() * 1.3)

    #  NN = ['$10^2$', '$10^3$', '$10^4$', '$10^5$', '$10^7$']
    #  NN_CMC = ['$10^3$', '$10^5$', '$10^7$']
    #  NN_MLS = ['$10^2$', '$10^3$', '$10^4$']
    NN_CMC = [10**3, 10**5, 10**7]
    NN_MLS = [10**2, 10**3, 10**4]
    xlabel = "N"

    ls1 = 's--'
    ls2 = '.--'
    lw = 0.5

    ylabel = 'RE est of $2\\Phi(-\\frac{{{}}}{{\\sqrt{{30}}}})$'.format(5)
    #  ylabel = 'RE $L = {}$'.format(5)

    CMC = [0.04331, 0.004236, 0.0004256]
    FE  = [0.01084, 0.003535, 0.00115]
    FNS = [0.01062, 0.003512, 0.001084]

    axes[0, 0].plot(NN_CMC, CMC,
                    ls2, lw=lw,
                    label='CMC',
                    )
    axes[0, 0].plot(NN_MLS, FE,
                    ls1, lw=lw,
                    label='FE',
                    )
    axes[0, 0].plot(NN_MLS, FNS,
                    ls2, lw=lw,
                    label='FNS',
                    )
    set_axis_labels_endaxes(xlabel, ylabel, axes[0, 0])
    axes[0, 0].set_xscale('log')
    axes[0, 0].set_yscale('log')

    ylabel = 'RE est of $2\\Phi(-\\frac{{{}}}{{\\sqrt{{30}}}})$'.format(18)

    CMC = [np.nan, 0.09617, 0.01014]
    FE  = [0.04734, 0.01629, 0.004523]
    FNS = [0.0338, 0.01033, 0.003297]

    axes[1, 0].plot(NN_CMC, CMC,
                    ls2, lw=lw,
                    )
    axes[1, 0].plot(NN_MLS, FE,
                    ls1, lw=lw,
                    )
    axes[1, 0].plot(NN_MLS, FNS,
                    ls2, lw=lw,
                    )
    set_axis_labels_endaxes(xlabel, ylabel, axes[1, 0])
    axes[1, 0].set_xscale('log')
    axes[1, 0].set_yscale('log')

    ylabel = 'RE est of $2\\Phi(-\\frac{{{}}}{{\\sqrt{{30}}}})$'.format(26)

    CMC = [np.nan, 1, 0.2182]
    FE  = [0.1371, 0.04238, 0.01321]
    FNS = [0.07725, 0.02013, 0.006135]

    axes[2, 0].plot(NN_CMC, CMC,
                    ls2, lw=lw,
                    )
    axes[2, 0].plot(NN_MLS, FE,
                    ls1, lw=lw,
                    )
    axes[2, 0].plot(NN_MLS, FNS,
                    ls2, lw=lw,
                    )
    set_axis_labels_endaxes(xlabel, ylabel, axes[2, 0])
    axes[2, 0].set_xscale('log')
    axes[2, 0].set_yscale('log')

    ylabel = 'PTN-$\\mathbb{{V}}\\mathrm{{ar}}$ est of $2\\Phi(-\\frac{{{}}}{{\\sqrt{{30}}}})$'.format(5)
    #  ylabel = 'PTN-$\\mathbb{{V}}\\mathrm{{ar}}$ $L = {}$'.format(5)

    CMC = [0.001198, 0.0012, 0.001194]
    FE  = [0.0005633, 0.0005665, 0.0005836]
    FNS = [0.0006223, 0.0006348, 0.0005835]

    axes[0, 1].plot(NN_CMC, CMC,
                    ls2, lw=lw,
                    )
    axes[0, 1].plot(NN_MLS, FE,
                    ls1, lw=lw,
                    )
    axes[0, 1].plot(NN_MLS, FNS,
                    ls2, lw=lw,
                    )
    set_axis_labels_endaxes(xlabel, ylabel, axes[0, 1])
    axes[0, 1].set_xscale('log')
    axes[0, 1].set_yscale('log')

    ylabel = 'PTN-$\\mathbb{{V}}\\mathrm{{ar}}$ est of $2\\Phi(-\\frac{{{}}}{{\\sqrt{{30}}}})$'.format(18)

    CMC = [np.nan, 1.502e-06, 1.339e-06]
    FE  = [1.225e-07, 1.488e-07, 1.099e-07]
    FNS = [1.403e-07, 1.25e-07, 1.244e-07]

    axes[1, 1].plot(NN_CMC, CMC,
                    ls2, lw=lw,
                    )
    axes[1, 1].plot(NN_MLS, FE,
                    ls1, lw=lw,
                    )
    axes[1, 1].plot(NN_MLS, FNS,
                    ls2, lw=lw,
                    )
    set_axis_labels_endaxes(xlabel, ylabel, axes[1, 1])
    axes[1, 1].set_xscale('log')
    axes[1, 1].set_yscale('log')

    ylabel = 'PTN-$\\mathbb{{V}}\\mathrm{{ar}}$ est of $2\\Phi(-\\frac{{{}}}{{\\sqrt{{30}}}})$'.format(26)
    CMC = [np.nan, 1.397e-08, 2.911e-09]
    FE  = [2.318e-12, 3.665e-12, 3.928e-12]
    FNS = [6.935e-12, 4.013e-12, 3.354e-12]

    axes[2, 1].plot(NN_CMC, CMC,
                    ls2, lw=lw,
                    )
    axes[2, 1].plot(NN_MLS, FE,
                    ls1, lw=lw,
                    )
    axes[2, 1].plot(NN_MLS, FNS,
                    ls2, lw=lw,
                    )
    set_axis_labels_endaxes(xlabel, ylabel, axes[2, 1])
    axes[2, 1].set_xscale('log')
    axes[2, 1].set_yscale('log')

    set_legend_under(fig, axes)

    fig.savefig(
        './figs/MLS_verif_bm'
        '.pdf'.format(
            #  NN[0], NN[-1],
            ),
        format='pdf', transparent=True,
        bbox_inches='tight',
        )

    ######################################################################

    fig2, axes2 = plt.subplots(nrows=1, ncols=2)
    fig2.set_figheight(fig2.get_figheight() / 2)

    ylabel = 'RE est of $\\mathbb{{P}}[M_{{{}}} > {}]$'.format(30, 26)

    CMC = [np.nan, 1, 0.2182]
    FE  = [0.1371, 0.04238, 0.01321]
    FNS = [0.07725, 0.02013, 0.006135]

    axes2[0].plot(NN_CMC, CMC,
                    ls2, lw=lw,
                    label='Crude Monte Carlo',
                    )
    axes2[0].plot(NN_MLS, FE,
                    ls1, lw=lw,
                    label='Fixed effort',
                    )
    axes2[0].plot(NN_MLS, FNS,
                    ls2, lw=lw,
                    label='Fixed number of successes',
                    )
    set_axis_labels_endaxes(xlabel, ylabel, axes2[0])
    axes2[0].set_xscale('log')
    axes2[0].set_yscale('log')

    ylabel = 'PTN-$\\mathbb{{V}}\\mathrm{{ar}}$ est of $\\mathbb{{P}}[M_{{{}}} > {}]$'.format(30, 26)

    CMC = [np.nan, 1.397e-08, 2.911e-09]
    FE  = [2.318e-12, 3.665e-12, 3.928e-12]
    FNS = [6.935e-12, 4.013e-12, 3.354e-12]

    axes2[1].plot(NN_CMC, CMC,
                    ls2, lw=lw,
                    )
    axes2[1].plot(NN_MLS, FE,
                    ls1, lw=lw,
                    )
    axes2[1].plot(NN_MLS, FNS,
                    ls2, lw=lw,
                    )
    set_axis_labels_endaxes(xlabel, ylabel, axes2[1])
    axes2[1].set_xscale('log')
    axes2[1].set_yscale('log')

    set_legend_under(fig2, axes2)

    fig2.savefig(
        './figs/MLS_verif_bm2'
        '.pdf'.format(
            #  NN[0], NN[-1],
            ),
        format='pdf', transparent=True,
        bbox_inches='tight',
        )


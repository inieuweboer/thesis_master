import numpy as np
import matplotlib.pyplot as plt

from text_render_latex import latexify, set_axis_labels_endaxes
latexify(columns=2)


def plot(N=np.power(10, 7), infolder='server/out/', infile='rainfall_cmc_data_15_0.2_2_3', filename='hist_rf'):
    with open(infolder + infile, 'r') as fp:
        lines = np.array([float(next(fp).strip()) for _ in range(N)])
        #  lines = np.array([eval(line) for line in fp])

    fig, ax = plt.subplots()

    ax.hist(lines,
             int(np.sqrt(N)),
             density=True,
             color='#8ebad9',
             )

    set_axis_labels_endaxes('$L$', '', ax=ax)

    # Save and remove excess whitespace
    fig.savefig('figs/{}_{}.pdf'.format(filename, infile),
        format='pdf', transparent=True,
        )


if __name__ == "__main__":
    #  for i in range(5, 8):
        #  plot(N=10**i)
    plot(N=np.power(10, 7))
    plot(N=np.power(10, 5), infolder='server/out/rf/cmc/', infile='L_1100_T_30_rfparamshash_15_0.2_2_3_-4697322418758345226')

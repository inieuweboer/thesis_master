    ####

    figname = 'comb'

    fig_qq, ax_qq = plt.subplots()

    ax_qq.set_xlabel(r'CMC mean quantiles')

    qq_plotter(cmc_mean, gpc_interp_mean, nquantiles=100,
               ax=ax_qq, linecolor='k')
    qq_plotter(cmc_mean, gpc_dp_mean, nquantiles=100,
               ax=ax_qq, linecolor='k')
    ax_qq.set_ylabel(r'gPC mean quantiles')
    fig_qq.savefig(
        './figs/QQ_mean_CMC_gPC_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf',
        #  bbox_inches='tight',
        )

    fig_qq_std, ax_qq_std = plt.subplots()

    ax_qq_std.set_xlabel(r'CMC std quantiles')

    qq_plotter(cmc_std, gpc_interp_std, nquantiles=100,
               ax=ax_qq_std, linecolor='k')
    qq_plotter(cmc_std, gpc_dp_std, nquantiles=100,
               ax=ax_qq_std, linecolor='k')
    ax_qq_std.set_ylabel(r'gPC std quantiles')
    fig_qq_std.savefig(
        './figs/QQ_std_CMC_gPC_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf',
        #  bbox_inches='tight',
        )

    fig_qq_interp_std, ax_qq_interp_std = plt.subplots()

    ax_qq_interp_std.set_xlabel('CMC std quantiles')

    qq_plotter(cmc_std, gpc_interp_std, nquantiles=100, ax=ax_qq_interp_std)
    ax_qq_interp_std.set_ylabel('gPC std interpolation quantiles')
    fig_qq_interp_std.savefig(
        './figs/QQ_CMC_gPC_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format('interp_std',
                                         T, L, K, M, a, b),
        format='pdf',
        bbox_inches='tight',
        )

    fig_qq_dp_std, ax_qq_discproj_std = plt.subplots()

    ax_qq_discproj_std.set_xlabel('CMC std quantiles')

    qq_plotter(cmc_std, gpc_dp_std, nquantiles=100, ax=ax_qq_discproj_std)
    ax_qq_discproj_std.set_ylabel('gPC std disc. proj. quantiles')
    fig_qq_dp_std.savefig('./figs/QQ_std_CMC_gPC_dp.pdf',
        format='pdf',
        #  bbox_inches='tight',
        )
    fig_qq_dp_std.savefig(
        './figs/QQ_CMC_gPC_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format('dp_std',
                                         T, L, K, M, a, b),
        format='pdf',
        #  bbox_inches='tight',
        )


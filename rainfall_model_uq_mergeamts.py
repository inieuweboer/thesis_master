import os
import shutil

from helper_functions import (
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    rfparams_alpha, rfparams_ucov,
    )
from rainfall_model_cumulative_splitting import *
from UQ_methods import gpc_approx_discrete_proj_uniform_tree_nodes


def main():
    # Get args
    args = rf_argparser()

    (T, rfparams_def,
     gauss_quad_deg,
     a, b,
     outfolder,
     ) = (
          args.T, args.rfparams,
          args.gauss_quad_deg,
          args.a, args.b,
          args.outfolder,
          )

    work_nodes = gpc_approx_discrete_proj_uniform_tree_nodes(
        a=a, b=b,
        gauss_quad_deg=gauss_quad_deg,  # M
        )

    # Prepare filenames
    if args.test:
        outfolder = outfolder + 'test/'

    os.makedirs(outfolder, exist_ok=True)

    filename_merged_pre = "{}merged_".format(outfolder)

    for node_index in range(gauss_quad_deg):
        rfparams = rfparams_ucov(rfparams_def, work_nodes, node_index)
        #  rfparams = rfparams_alpha(rfparams_def, work_nodes, node_index)

        args_key = args_str_maker(
            T=T, rfparamshash=rf_params_tostring(rfparams),
            )

        mergedfile = filename_merged_pre + args_key
        infiles = [f for f in os.listdir(outfolder) if f.endswith(args_key)]

        # Overwrites existing merged file
        with open(mergedfile, 'wb') as fp_merged:
            for filecounter, infile in enumerate(infiles, start=1):
                print("{}, {} of {}, {}".format(
                    filecounter, node_index, len(infiles), gauss_quad_deg))

                infilename = outfolder + infile

                # Add newline if not there due to crash or anything
                #  with open(infilename, 'a+') as fp_in:
                #      fp_in.seek(fp_in.tell() - 1, 0)

                #      if fp_in.read() != '\n':
                #          fp_in.write('\n')

                with open(infilename, 'rb') as fp_in:
                    shutil.copyfileobj(fp_in, fp_merged)


if __name__ == "__main__":
    main()

import multiprocessing



def worker(args):
    procnum, i = args

    name = multiprocessing.current_process().name
    print('I am number {} with name {}'.format(procnum, name))

    return procnum + i


if __name__ == '__main__':
    pool = multiprocessing.Pool(processes=3)

    print(pool.map(worker, zip(range(5), range(5, 10))))
    print('Amt of processors: ', multiprocessing.cpu_count())

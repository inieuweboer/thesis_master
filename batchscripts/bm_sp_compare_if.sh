#!/bin/bash
#SBATCH --job-name=BMIF
#SBATCH --output=%x-%j.out

#SBATCH -N 1
#SBATCH --tasks-per-node 3
#SBATCH --time=24:00:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: BMIF"

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

case $HOSTNAME in
  (antergos-sopropo)
    pyth="python";
    work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl)
    echo "should not run on ${HOSTNAME} without sbatch, exit";
    exit 1;;
  (*)
    # py3 and latest versions of numpy, scipy
    # pyth="$HOME/.virtualenvs/math/bin/python";
    pyth="python3";
    work_dir="$HOME/thesis_rainfall";;
esac

# params="--nsims 2 --FNS 10 --test"
params="--nsims 100 --L 18 --FNS 100"

time "${pyth}" -u "${work_dir}/bm_splitting_compare_if.py"       --datetime "$dt" $params && \
time "${pyth}" -u "${work_dir}/bm_splitting_compare_if_valid.py" --datetime "$dt" $params

echo end of job

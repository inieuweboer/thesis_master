import matplotlib.pyplot as plt
import numpy as np

from itertools import cycle
from scipy.special import erfc

from importance_functions import (
    plot_levels,
    make_importance_fn_brownian_motion,
    make_importance_fn_distance,
    make_importance_fn_large_deviation,
    )
from helper_functions import mve
from optimize_levels import optimize_levels


spmult = 10


###
# Simulate the probability P( \sup_{0 <= t <= T} W_t >= L).
###


def Phi(z):
    return erfc(- z / np.sqrt(2)) / 2


def exact_prob_bm(L, T):
    r"""
    $\xprob{ M_T > L}$ where $M_T := \sup_{0 \le t \le T} W_t$
    """
    return 2 * Phi(-L / np.sqrt(T))


def bm_cmc(L, T, alg_params=(0.01, np.power(10, 2)),
           show=False, save=False):

    stepsize, nruns = alg_params
    nsteps = int(T / stepsize)

    hits = 0

    for _ in range(nruns):
        pos = 0
        trajectory = [pos]

        # Pre-sample normally distributed steps
        normal_samples = iter(np.random.normal(scale=np.sqrt(stepsize),
                                               size=nsteps))

        split_color = 'red'

        for sample in normal_samples:
            pos += sample
            trajectory.append(pos)

            if pos >= L:
                hits += 1

                split_color = 'green'

                break

        if show or save:
            plt.plot(np.linspace(0, (len(trajectory) - 1)*stepsize,
                                 len(trajectory)),
                     trajectory,
                     color='k',
                     linewidth=.125)

            # Plot endpoint of trajectory
            plt.plot((len(trajectory) - 1)*stepsize, trajectory[-1],
                     color=split_color, marker='o', zorder=100)

    if show:
        tspace = np.linspace(0, T, T*spmult + 1)
        plt.plot(tspace, np.full_like(tspace, L), 'r--', linewidth=1)

        plt.show()
    elif save:
        tspace = np.linspace(0, T, T*spmult + 1)
        plt.plot(tspace, np.full_like(tspace, L), 'r--', linewidth=1)


    return hits / nruns


def bm_splitting_fixed_offspring(L, T,
                                 alg_params=(None, 4, 125, 3, None),
                                 optimize=False, show=False, save=False,
                                 ):
    """
    nlevels is no of levels
    effort is no of starting particles
    mult: amt of particles split into when new level reached
    """
    stepsize, nlevels, effort, mult, importance_fn = alg_params

    # Step size semi-agnostic of threshold L and amount of levels. Dividing
    # by 100 means a standard deviation factor of 10 between two levels,
    # which makes the probability of multiple level-skipping quite low
    if stepsize is None:
        #  stepsize = 0.01
        stepsize = L / ((nlevels + 1) * 100)

    if importance_fn is None:
        importance_fn = make_importance_fn_distance(T, L)

    # Pre-sample normally distributed steps
    #  nsteps = int(T / stepsize)
    #  normal_samples = iter(np.random.normal(scale=np.sqrt(stepsize),
    #                                         size=effort*nsteps*mult**nlevels))

    # Define depth-first algorithm as closure
    def depth_first(plot=True):
        while particles:
            pos, entry_time, lvl_index = particles.pop()
            trajectory = [pos]

            # Done with current particle if above last level
            # or entry time after end time
            if lvl_index >= nlevels or entry_time >= T:
                continue

            # Pre-sample normally distributed steps
            nsteps = int((T - entry_time) / stepsize) + 1
            normal_samples = iter(np.random.normal(scale=np.sqrt(stepsize),
                                                   size=nsteps))

            split_color = 'red'

            for t in np.arange(entry_time, T, stepsize):
                pos += next(normal_samples)
                trajectory.append(pos)

                # Split (and done with current particle) if
                # higher level is reached
                # Add at next time step and next level
                if importance_fn(t, pos) >= levels[lvl_index]:
                    particles.extend((pos, t + stepsize, lvl_index + 1)
                                     for _ in range(mult))
                    survival_amounts[lvl_index] += 1

                    split_color = 'green'

                    break

            if plot and (show or save):
                # Note that particle trajectories overlap!
                plt.plot(
                    np.linspace(entry_time,
                                entry_time + (len(trajectory) - 1)*stepsize,
                                len(trajectory)),
                    trajectory,
                    #  color=(0, 0, lvl_index/nlevels),
                    #  color='k',
                    color=((nlevels - lvl_index - 1)*2/(nlevels*3),
                           (nlevels - lvl_index - 1)*2/(nlevels*3),
                           (nlevels - lvl_index - 1)*2/(nlevels*3)),
                    linewidth=.125)

                # Plot endpoint of trajectory up till next level
                if len(trajectory) > 1:
                    plt.plot(entry_time + (len(trajectory) - 1)*stepsize,
                             trajectory[-1],
                             color=split_color, marker='o', zorder=100)

    # Set standard equidistant levels
    levels = [lvl_index/nlevels for lvl_index in range(1, nlevels+1)]

    # Make estimate in such a way to also calculate variance and relative error
    #  ests = []
    #  for _ in range(effort):
    #      survival_amounts = np.zeros(nlevels, dtype=int)
    #      particles = [(0, 0, 0)]
    #      depth_first()

    #      est = survival_amounts[-1] / mult**(nlevels-1)
    #      ests.append(est)

    survival_amounts = np.zeros(nlevels, dtype=int)
    particles = [(0, 0, 0) for _ in range(effort)]
    depth_first()

    # Plot the levels
    if show:
        plot_levels(T, L, importance_fn, levels)
        plt.show()
    elif save:
        plot_levels(T, L, importance_fn, levels)

    est = survival_amounts[-1] / (effort * mult**(nlevels-1))
    var = nlevels * (1 - est) * est * (2*nlevels - 1) / effort

    #  return mve(ests)
    return est, var, np.sqrt(var) / est


def bm_splitting_fixed_effort(L, T,
                              alg_params=(None, 4, 100, None),
                              optimize=False, show=False, save=False,
                              ):
    """
    nlevels is no of levels
    effort is no of starting particles per level
    """
    stepsize, nlevels, effort, importance_fn = alg_params

    # Step size semi-agnostic of threshold L and amount of levels. Dividing
    # by 100 means a standard deviation factor of 10 between two levels,
    # which makes the probability of multiple level-skipping quite low
    if stepsize is None:
        #  stepsize = 0.01
        stepsize = L / ((nlevels + 1) * 100)

    if importance_fn is None:
        importance_fn = make_importance_fn_distance(T, L)

    # Breadth-first algorithm
    def breadth_first(plot=True):
        for lvl_index in range(nlevels):
            survived_particles = []

            # Exhaust particles of level
            while particles:
                pos, entry_time = particles.pop()
                trajectory = [pos]

                # Skip current particle if entry time after end time
                if entry_time >= T:
                    continue

                # Pre-sample normally distributed steps
                nsteps = int((T - entry_time) / stepsize) + 1
                normal_samples = iter(np.random.normal(scale=np.sqrt(stepsize),
                                                       size=nsteps))

                split_color = 'red'

                for t in np.arange(entry_time, T, stepsize):
                    pos += next(normal_samples)
                    trajectory.append(pos)

                    # Save current particle if survived
                    if importance_fn(t, pos) >= levels[lvl_index]:
                        survival_amounts[lvl_index] += 1
                        survived_particles.append((pos, t + stepsize))

                        split_color = 'green'

                        break

                if plot and (show or save):
                    # Note that particle trajectories overlap!
                    plt.plot(
                        np.linspace(
                            entry_time,
                            entry_time + (len(trajectory) - 1)*stepsize,
                            len(trajectory)),
                        trajectory,
                        #  color=(0, 0, lvl_index/nlevels),
                        color=((nlevels - lvl_index - 1)*2/(nlevels*3),
                               (nlevels - lvl_index - 1)*2/(nlevels*3),
                               (nlevels - lvl_index - 1)*2/(nlevels*3)),
                        linewidth=.125)

                    # Plot endpoint of trajectory up till next level
                    if len(trajectory) > 1:
                        plt.plot(entry_time + (len(trajectory) - 1)*stepsize,
                                 trajectory[-1],
                                 color=split_color, marker='o', zorder=100)

            # Do the fixed-effort splitting, with balanced (random) selection
            np.random.shuffle(survived_particles)
            survived = survival_amounts[lvl_index]

            # If none survived, we are done.
            if survived == 0:
                survival_amounts[-1] = 0
                break

            # Else calculate multipliers for the surviving particles...
            mults = np.full(shape=survived,
                            fill_value=effort // survived, dtype=int)

            rest = effort % survived
            if rest > 0:
                rest_vect = np.ones_like(mults, dtype=int)
                rest_vect[rest:] = 0
                mults += rest_vect

            # ... and split particles
            for particle, mult in zip(survived_particles, mults):
                particles.extend(particle for _ in range(mult))

    # Set standard equidistant levels
    levels = [lvl_index/nlevels for lvl_index in range(1, nlevels+1)]

    particles = [(0, 0) for _ in range(effort)]
    survival_amounts = np.zeros(nlevels, dtype=int)
    breadth_first()

    # Plot the levels
    if show:
        plot_levels(T, L, importance_fn, levels)
        plt.show()
    elif save:
        plot_levels(T, L, importance_fn, levels)

    probs = np.array(survival_amounts, dtype=float)
    probs /= effort

    est = np.prod(probs)
    var = (est * ((1 - est) / effort + est))**nlevels - est**(2*nlevels)
    #  var = nlevels * est ** (2 - 1 / nlevels) / effort

    return est, var, np.sqrt(var) / est


def bm_splitting_fixed_successes(L, T,
                                 alg_params=(None, 4, 40, None),
                                 optimize=False, show=False, save=False,
                                 ):
    """
    nlevels is no of levels
    success is no of succeeded particles per level required
    """
    #  stepsize, nlevels, success, max_bootstraps, importance_fn = alg_params
    stepsize, nlevels, success, importance_fn = alg_params

    # Step size semi-agnostic of threshold L and amount of levels. Dividing
    # by 100 means a standard deviation factor of 10 between two levels,
    # which makes the probability of multiple level-skipping quite low
    if stepsize is None:
        #  stepsize = 0.01
        stepsize = L / ((nlevels + 1) * 100)

    if importance_fn is None:
        importance_fn = make_importance_fn_distance(T, L)

    # Breadth-first algorithm
    def breadth_first(plot=True):
        # Start dry, total pos, entry time;
        particles = [(0, 0)]

        for lvl_index in range(nlevels):
            survived_particles = []

            # Iterator for generating random cycled picks
            it = cycle(np.random.permutation(len(particles)))

            # Go on until success level reached or max bootstraps reached
            #  for _ in range(max_bootstraps):
            #      if len(survived_particles) == success:
            #          break
            while len(survived_particles) < success:

                #  index = np.random.randint(len(particles))
                index = next(it)

                pos, entry_time = particles[index]
                trajectory = [pos]

                # Skip current particle if entry time after end time
                if entry_time >= T:
                    continue

                particle_amounts[lvl_index] += 1

                # Pre-sample normally distributed steps
                nsteps = int((T - entry_time) / stepsize) + 1
                normal_samples = iter(np.random.normal(scale=np.sqrt(stepsize),
                                                       size=nsteps))

                split_color = 'red'

                for t in np.arange(entry_time, T, stepsize):
                    pos += next(normal_samples)
                    trajectory.append(pos)

                    # Save current particle if survived
                    if importance_fn(t, pos) >= levels[lvl_index]:
                        survival_amounts[lvl_index] += 1

                        # Add at next time step
                        survived_particles.append((pos, t + stepsize))

                        split_color = 'green'

                        break

                if plot and (show or save):
                    # Note that particle trajectories overlap!
                    plt.plot(
                        np.linspace(
                            entry_time,
                            entry_time + (len(trajectory) - 1)*stepsize,
                            len(trajectory)),
                        trajectory,
                        #  color=(0, 0, lvl_index/nlevels),
                        color=((nlevels - lvl_index - 1)*2/(nlevels*3),
                               (nlevels - lvl_index - 1)*2/(nlevels*3),
                               (nlevels - lvl_index - 1)*2/(nlevels*3)),
                        linewidth=.125)

                    # Plot endpoint of trajectory
                    if len(trajectory) > 1:
                        plt.plot(entry_time + (len(trajectory) - 1)*stepsize,
                                 trajectory[-1],
                                 color=split_color, marker='o', zorder=100)

            particles = survived_particles

    # Set standard equidistant levels
    levels = [lvl_index/nlevels for lvl_index in range(1, nlevels+1)]

    if optimize:
        # Set survival amounts to zero and do a test run
        survival_amounts = np.zeros(nlevels, dtype=int)
        particle_amounts = np.zeros(nlevels, dtype=int)
        breadth_first(False)

        # Re-run with optimized levels
        probs = np.array(particle_amounts, dtype=float)
        probs /= particle_amounts
        levels = optimize_levels(probs, levels)
        nlevels = len(levels)

    survival_amounts = np.zeros(nlevels, dtype=int)
    particle_amounts = np.zeros(nlevels, dtype=int)
    breadth_first()

    # Plot the levels
    if show:
        plot_levels(T, L, importance_fn, levels)
        plt.show()
    elif save:
        plot_levels(T, L, importance_fn, levels)

    probs = np.array(survival_amounts, dtype=float)
    probs /= particle_amounts

    # eq 5.14 in Wadman
    sre_bound = -1 + np.prod(1 / (survival_amounts - 2) + 1)
    est = np.prod(probs)
    var_bound = sre_bound * est * est

    return est, var_bound, np.sqrt(sre_bound)


if __name__ == "__main__":
    import time

    np.random.seed()
    np.seterr(divide='ignore')

    L = 5
    T = 30

    print('L = ', L)
    print('T = ', T)

    print(exact_prob_bm(L, T))

    stepsize = 0.01
    nruns = np.power(10, 3)

    show = True

    print(bm_cmc(L=L, T=T,
                 alg_params=(stepsize, nruns),
                 show=show,
                 ))

    nlevels = 3
    success = 7
    start_particles = effort = 5
    mult = 2
    #  max_bootstraps = 1000

    print(bm_splitting_fixed_offspring(L=L, T=T,
                                       alg_params=(stepsize, nlevels,
                                                   start_particles,
                                                   mult, None),
                                       show=show,
                                       ))
    print(bm_splitting_fixed_effort(L=L, T=T,
                                    alg_params=(stepsize, nlevels,
                                                effort, None),
                                    show=show,
                                    ))
    #  print('non-optimized')
    print(bm_splitting_fixed_successes(
        L=L, T=T,
        #  alg_params=(stepsize, nlevels,
        #              success,
        #              make_importance_fn_brownian_motion(T, L)
        #              ),
        alg_params=(stepsize, nlevels,
                    success, None),
        show=show,
        ))
    #  print('optimized')
    #  print(bm_splitting_fixed_successes(L=L, T=T,
    #          show=True,
    #          optimize=True,
    #          ))

    print()

import matplotlib.pyplot as plt
import numpy as np

from itertools import cycle

from importance_functions import (plot_levels,
                                  make_importance_fn_brownian_motion,
                                  make_importance_fn_distance,
                                  make_importance_fn_large_deviation)
from optimize_levels import optimize_levels
from rainfall_model import (one_step_multisite_cumulative,
                            #  nstations,
                            #  ndays,
                            )


def cumulative_rainfall_cmc(params, rmax, ndays=30, c=None,
                            alg_params=(None, np.power(10, 3), None),
                            show=False
                            ):
    """
    c is such that the sum is taken, but could also look more localized
    """
    nstations = params[4].shape[0]
    nruns = alg_params[1]

    samples = []

    for _ in range(nruns):
        # Start dry
        X = np.zeros(nstations, dtype=int)
        tot_rf = 0
        trajectory = [tot_rf]

        for _ in range(ndays):
            X, tot_rf = one_step_multisite_cumulative(X, tot_rf, params)
            trajectory.append(tot_rf)

            if tot_rf >= rmax:
                samples.append(1)
                break
        else:
            samples.append(0)

        if show:
            # Note that particle trajectories overlap!
            plt.plot(np.arange(len(trajectory)),
                     trajectory,
                     color='k',
                     linewidth=.5)

            # Plot endpoint of trajectory up till next level
            if len(trajectory) > 1:
                plt.plot(len(trajectory) - 1, trajectory[-1],
                         color='green', marker='.')

    if show:
        tspace = np.linspace(0, ndays)
        plt.plot(tspace, np.full_like(tspace, rmax), 'r--', linewidth=1)
        #  plt.grid(True, which='both')

        #  plt.savefig('figs/Figure_CMC.png')
        plt.show()

    return np.mean(samples)


# Splitting #####

def cumulative_rainfall_splitting_fixed_offspring(params, rmax, ndays,
                                                  c=None,
                                                  alg_params=(6, 10, 3, None),
                                                  show=False):
    """
    nlevels is no of levels
    effort is no of starting particles
    mult: amt of particles split into when new level reached
    """

    nstations = params[4].shape[0]
    nlevels, effort, mult, importance_fn = alg_params

    if importance_fn is None:
        importance_fn = make_importance_fn_distance(ndays, rmax)

    # Define depth-first algorithm as closure
    def depth_first(plot=True):
        while particles:
            X, tot_rf, entry_time, lvl_index = particles.pop()
            trajectory = [tot_rf]

            for t in range(entry_time, ndays):
                # Done with current particle if above last level
                if lvl_index >= nlevels:
                    break

                X, tot_rf = one_step_multisite_cumulative(
                    X, tot_rf, params)
                trajectory.append(tot_rf)

                # Split (and done with current particle) if
                # higher level is reached
                # Add at next time step and next level
                #  if tot_rf >= rmax * (lvl_index + 1) / nlevels:
                if importance_fn(t, tot_rf) >= levels[lvl_index]:
                    particles.extend((X, tot_rf, t + 1, lvl_index + 1)
                                     for _ in range(mult))
                    survival_amounts[lvl_index] += 1

                    break

            if plot and show:
                # Particle trajectories overlap!
                plt.plot(np.arange(entry_time,
                                   entry_time + len(trajectory)),
                         trajectory,
                         color=(0, 0, lvl_index/nlevels),
                         linewidth=.5)

                # Plot endpoint of trajectory up till next level
                if len(trajectory) > 1:
                    plt.plot(entry_time + len(trajectory) - 1, trajectory[-1],
                             color='green', marker='.')

    # Set standard equidistant levels
    levels = [lvl_index/nlevels for lvl_index in range(1, nlevels+1)]

    # Make estimate, in such a way to also calculate
    # variance and relative error
    ests = []
    for _ in range(effort):
        survival_amounts = np.zeros(nlevels, dtype=int)
        particles = [(np.zeros(nstations, dtype=int), 0, 0, 0)]
        depth_first()
        est = survival_amounts[-1] / np.power(mult, nlevels-1)
        ests.append(est)

    # Plot the levels
    if show:
        plot_levels(ndays, rmax, importance_fn, levels)
        plt.show()

    return np.mean(ests)


def cumulative_rainfall_splitting_fixed_effort(params, rmax, ndays,
                                               c=None,
                                               alg_params=(6, 300, None),
                                               show=False):
    """
    nlevels is no of levels
    effort is no of starting particles per level
    """
    nstations = params[4].shape[0]
    nlevels, effort, importance_fn = alg_params

    if importance_fn is None:
        importance_fn = make_importance_fn_distance(ndays, rmax)

    # Breadth-first algorithm
    def breadth_first(plot=True):
        for lvl_index in range(nlevels):
            survived_particles = []

            # Exhaust particles of level
            while particles:
                X, tot_rf, entry_time = particles.pop()
                trajectory = [tot_rf]

                for t in range(entry_time, ndays):
                    X, tot_rf = one_step_multisite_cumulative(
                        X, tot_rf, params)
                    trajectory.append(tot_rf)

                    # Save current particle if survived
                    #  if tot_rf >= rmax * (lvl_index + 1) / nlevels:
                    if importance_fn(t, tot_rf) >= levels[lvl_index]:
                        survival_amounts[lvl_index] += 1

                        # Add at next time step
                        survived_particles.append((X, tot_rf, t + 1))

                        break

                if plot and show:
                    # Particle trajectories overlap!
                    plt.plot(np.arange(entry_time,
                                       entry_time + len(trajectory)),
                             trajectory,
                             color=(0, 0, lvl_index/nlevels),
                             linewidth=.5)

                    # Plot endpoint of trajectory up till next level
                    if len(trajectory) > 1:
                        plt.plot(entry_time + len(trajectory) - 1,
                                 trajectory[-1],
                                 color='green', marker='.')

            # Don't split if at last level (only need to split
            # nlevels - 1 times)
            if lvl_index == nlevels - 1:
                break

            # Do the fixed-effort splitting
            np.random.shuffle(survived_particles)
            survived = survival_amounts[lvl_index]

            # If none survived, we are done.
            if survived == 0:
                survival_amounts[-1] = 0
                break

            # Else calculate multipliers for the surviving particles...
            mults = np.full(shape=survived,
                            fill_value=effort // survived, dtype=int)

            rest = effort % survived
            if rest > 0:
                rest_vect = np.ones_like(mults, dtype=int)
                rest_vect[rest:] = 0
                mults += rest_vect

            # ... and split particles
            for particle, mult in zip(survived_particles, mults):
                particles.extend(particle for _ in range(mult))

    # Set standard equidistant levels
    levels = [lvl_index/nlevels for lvl_index in range(1, nlevels+1)]

    particles = [(np.zeros(nstations, dtype=int), 0, 0) for _ in range(effort)]
    survival_amounts = np.zeros(nlevels, dtype=int)
    breadth_first()

    probs = np.array(survival_amounts, dtype=float)
    probs /= effort

    prob = np.prod(probs)
    #  var = np.power(prob * ((1 - prob) / effort + prob),
    #                 nlevels) - np.power(prob, 2*nlevels)

    # Plot the levels
    if show:
        plot_levels(ndays, rmax, importance_fn, levels)
        #  plt.grid(True, which='both')

        #  plt.savefig('figs/Figure_FE.png')
        plt.show()

    #  return prob, var, np.sqrt(var) / prob
    return prob


def cumulative_rainfall_splitting_fixed_successes(params, rmax, ndays,
                                                  c=None,
                                                  #  alg_params=(5, 50, 1000, None),
                                                  alg_params=(5, 50, None),
                                                  optimize=False,
                                                  show=False):
    """
    nlevels is no of levels
    success is no of succeeded particles per level required
    """
    nstations = params[3].shape[0]
    #  nlevels, success, max_bootstraps, importance_fn = alg_params
    nlevels, success, importance_fn = alg_params

    if importance_fn is None:
        importance_fn = make_importance_fn_distance(ndays, rmax)

    # Breadth-first algorithm
    def breadth_first(plot=True):
        # Start dry, total tot_rf, entry time;
        particles = [(np.zeros(nstations, dtype=int), 0, 0)]

        for lvl_index in range(nlevels):
            survived_particles = []

            # Iterator for generating random cycled picks
            it = cycle(np.random.permutation(len(particles)))

            # Go on until success level reached
            while len(survived_particles) < success:
                index = next(it)

                X, tot_rf, entry_time = particles[index]
                particle_amounts[lvl_index] += 1
                trajectory = [tot_rf]

                for t in range(entry_time, ndays):
                    X, tot_rf = one_step_multisite_cumulative(
                        X, tot_rf, params)
                    trajectory.append(tot_rf)

                    # Save current particle if survived
                    if importance_fn(t, tot_rf) >= levels[lvl_index]:
                        survival_amounts[lvl_index] += 1

                        # Add at next time step
                        survived_particles.append((X, tot_rf, t + 1))

                        break

                if plot and show:
                    # Particle trajectories overlap!
                    plt.plot(np.arange(entry_time,
                                       entry_time + len(trajectory)),
                             trajectory,
                             color=(0, 0, lvl_index/nlevels),
                             linewidth=.5)

                    # Plot endpoint of trajectory up till next level
                    if len(trajectory) > 1:
                        plt.plot(entry_time + len(trajectory) - 1,
                                 trajectory[-1],
                                 color='green', marker='.')

            particles = survived_particles

    # Set standard equidistant levels
    levels = [lvl_index/nlevels for lvl_index in range(1, nlevels+1)]

    if optimize:
        # Set survival amounts to zero and do a test run
        survival_amounts = np.zeros(nlevels, dtype=int)
        particle_amounts = np.zeros(nlevels, dtype=int)
        breadth_first(False)

        # Re-run with optimized levels
        probs = np.array(particle_amounts, dtype=float)
        probs /= particle_amounts
        levels = optimize_levels(probs, levels)
        nlevels = len(levels)

    survival_amounts = np.zeros(nlevels, dtype=int)
    particle_amounts = np.zeros(nlevels, dtype=int)
    breadth_first()

    probs = np.array(survival_amounts, dtype=float)
    probs /= particle_amounts

    est = np.prod(probs)
    # eq 5.14 in Wadman
    #  sre_bound = -1 + np.prod(1 / (survival_amounts - 2) + 1)
    #  var_bound = sre_bound * est * est

    # Plot the levels
    if show:
        plot_levels(ndays, rmax, importance_fn, levels)
        plt.show()

    #  return est, var_bound, np.sqrt(sre_bound)
    return est


###############################################################################


def run_cmc(params, ndays=30, rmax=1000):
    nruns = int(1e8)

    print('rmax = ', rmax)
    print('nruns = ', '{:,}'.format(nruns).replace(',', ' '))
    print(cumulative_rainfall_cmc(params=params, rmax=rmax, ndays=ndays,
                                  alg_params=(None, np.power(10, 2), None)))


def run_splitting(params, ndays=30, rmax=900):
    #  optimize = True
    show = False
    show = True

    #  print(cumulative_rainfall_splitting_fixed_offspring(params=params, rmax=rmax, ndays=ndays,
    #      show=show))
    #  print(cumulative_rainfall_splitting_fixed_successes(params=params, rmax=rmax, ndays=ndays,
    #      #  optimize=optimize,
    #      show=show))

    #  nsims = 40
    N = 5
    #  probs_cmc = []
    #  probs_fe = []
    #  for _ in range(nsims):
    #      probs_cmc.append(
    #  print(
    #          cumulative_rainfall_cmc(params=params, rmax=rmax, ndays=ndays,
    #                                  alg_params=(None, N, None),
    #                                  show=show,
    #                                  )
    #        )
    #  print(np.mean(probs_cmc))

    print(
            cumulative_rainfall_splitting_fixed_offspring(params=params,
                                                          rmax=rmax, ndays=ndays,
                                                          alg_params=(2, N, 2, None),
                                                          show=show,
                                                          )
          )
    #  for _ in range(nsims):
    #      probs_fe.append(
    print(
            cumulative_rainfall_splitting_fixed_effort(params=params,
                                                       rmax=rmax, ndays=ndays,
                                                       alg_params=(2, N, None),
                                                       show=show,
                                                       )
          )
    #  print(np.mean(probs_fe))
    print(
            cumulative_rainfall_splitting_fixed_successes(params=params,
                                                          rmax=rmax, ndays=ndays,
                                                          alg_params=(2, N, None),
                                                          show=show,
                                                          )
          )


def run_splitting_timed(ndays=30, rmax=3000):
    nruns = 100  # ~14 secs. Would need 20 minutes for 10000, etc

    show = False

    import time

    start = time.time()
    print(cumulative_rainfall_cmc(params=(15, 0.2, 2, 3), rmax=rmax, nruns=nruns))
    end = time.time()
    print("CMC nruns = " + str(nruns) + ": ", end - start)

    start = time.time()
    print(cumulative_rainfall_splitting_fixed_offspring(params=(15, 0.2, 2, 3),
                                                        rmax=rmax, ndays=ndays,
                                                        show=show))
    end = time.time()
    print("Splitting fixed bootstrap", end - start)

    start = time.time()
    print(cumulative_rainfall_splitting_fixed_effort(params=(15, 0.2, 2, 3),
                                                     rmax=rmax, ndays=ndays,
                                                     show=show))
    end = time.time()
    print("Splitting fixed effort", end - start)


###############################################################################


if __name__ == "__main__":
    np.random.seed(42)  # Seed for precip_params

    # params
    nstations = 3  # to keep calculation time a bit down
    ndays = 30  # one month, or a season, short-term predictivity

    precip_params = np.random.sample((nstations, 2))  # [p_{0, 1}, p_{1, 1}] per weather station
    mean = np.zeros(nstations)

    A_U = np.random.sample((nstations, nstations))
    A_V = np.random.sample((nstations, nstations))
    Ucov = A_U.T @ A_U  # replace with empirical correlations omega eq 9
    Vcov = A_V.T @ A_V  # replace with empirical correlations zeta eq 9, see also eq 17

    np.random.seed()

    params = (15, 0.2, 2, 3, precip_params, Ucov, Vcov)

    run_splitting(params)

import numpy as np
from scipy import stats

from bm_splitting import exact_prob_bm
from helper_functions import (
    calc_chars, bm_splitting_argparser,
    args_str_maker, get_first)

import datetime
from tabulate import tabulate


if __name__ == "__main__":
    # Get args
    args = bm_splitting_argparser()
    datetimestr, nsims, nlevels, L, T = \
        args.datetime, args.nsims, args.nlevels, args.L, args.T

    exact_prob = exact_prob_bm(L, T)

    # Prepare filenames
    outfolder = 'out/bm/'
    if args.test:
        outfolder = outfolder + 'test/'

    name = '_bm_sp_'
    filename_in_pre = "".join([outfolder, datetimestr, name])
    filename_out = "{}bm_sp_validation{}".format(outfolder, datetimestr)
    filename_times = "".join([outfolder, datetimestr, name + "times"])

    # Get times and put them in dict
    with open(filename_times, 'r') as fp_time:
        #  times_dict = eval(fp_time.readline())
        times_dict = eval(fp_time.read().splitlines()[-1])

    table_arr = []

    for method in ['FE', 'FNS', 'FO']:
        for meth_params in getattr(args, method):
            if meth_params[0] < 0:
                print('Skipping ', method)
                break

            args_key = args_str_maker(nsims, L, T, nlevels, method, meth_params)

            filename_in = filename_in_pre + args_key

            # Result might not exist due to simulation failing etc
            try:
                with open(filename_in, 'r') as fp_in:
                    print('Processing: ', filename_in)

                    fp_in.seek(0)
                    ests = np.array([get_first(line) for line in fp_in])

                    #  prob_chars = calc_chars(ests[:, 0], times_dict[args_key])
                    prob_chars = calc_chars(ests, times_dict[args_key])
                    prob_bias = np.abs(prob_chars[0] - exact_prob)

                    prob_chars = np.insert(prob_chars, 1, prob_bias)
                    row = ['{} {}'.format(method, ' '.join(map(str, meth_params)))]
                    row.extend(prob_chars)

                    table_arr.append(row)

            except FileNotFoundError:
                print('Not found: ', filename_in)

    # Prepare first row of table
    first_row = [['', 'Estimate', 'Bias', 'Proc. time',
                  '$\\xv{\\hat{\\gamma}}$',
                  '$\\operatorname*{RE}[\\hat{\\gamma}]$',
                  'PTN-$\\xv{\\hat{\\gamma}}$',
                  'PTN-$\\operatorname*{RE}[\\hat{\\gamma}]$']]

    # Finally print table to outfile
    with open(filename_out, 'a+') as fp_out:
        print('Datetime simulation: ', datetimestr, file=fp_out)
        print('Datetime now: ', datetime.datetime.now().strftime("%Y%m%d_%H%M%S"), file=fp_out)
        print('nsims = {:,}'.format(nsims).replace(',', ' '), file=fp_out)
        print('L = {}'.format(L), file=fp_out)
        print('T = {}'.format(T), file=fp_out)
        print('Exact prob = {}'.format(exact_prob), file=fp_out)

        # Workaround! Cannot make a full row of strings without destroying the formatting of floats
        # only need to delete 4 lines from outfile with this solution
        print(tabulate(first_row, tablefmt="latex_raw"), file=fp_out)
        print(tabulate(table_arr, tablefmt="latex_raw", floatfmt=".4g"), file=fp_out)

        print("\\caption{{Validation and comparison of multilevel splitting,"
              " $L = {0}, T = {1}$, exact prob $2\Phi(-\\frac{{{0}}}{{\sqrt{{{1}}}}})"
              " \\approx {2:.4g}$}}".format(L, T, exact_prob), file=fp_out
              )

        print(file=fp_out)
        print(file=fp_out)

import numpy as np
from scipy import stats

from helper_functions import (
    calc_chars,
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    )

import datetime
from tabulate import tabulate



if __name__ == "__main__":
    # Get args
    args = rf_argparser()
    (datetimestr, nsims,
     L, T,
     nlevels, rfparams,
     outfolder,
     ) = (args.datetime, args.nsims,
          args.L, args.T,
          args.nlevels, args.rfparams,
          args.outfolder,
          )

    rmax = L  # = 1100
    ndays = T   # = 30

    # Prepare filenames
    #  outfolder = 'out/rf/last/'
    if args.test:
        outfolder = outfolder + 'test/'
    #  if not args.local:
    #      outfolder = '/scratch/multi_splitting/' + outfolder

    name = '_rf_'
    filename_in_pre = "".join([outfolder, datetimestr, name])
    filename_out = outfolder + 'rf_validation' + datetimestr
    filename_times = "".join([outfolder, datetimestr, name + "times"])

    # Get last partial save of times and put them in dict
    with open(filename_times, 'r') as fp_time:
        #  times_dict = eval(fp_time.readline())
        times_dict = eval(fp_time.read().splitlines()[-1])

    table_arr = []

    for method in ['CMC', 'FE', 'FNS', 'FO', ]:
        for method_params in getattr(args, method):
            if method_params[0] < 0:
                print('Skipping ', method)
                break

            args_key = args_str_maker(
                method, method_params,
                nsims=nsims, L=L, T=T, nlevels=nlevels,
                rfparams=rf_params_tostring(rfparams),
                )

            filename_in = filename_in_pre + args_key

            # Result might not exist due to simulation failing etc
            try:
                with open(filename_in, 'r') as fp_in:
                    print('Processing ', filename_in)

                    fp_in.seek(0)
                    #  ests = np.array([eval(next(fp_in)) for _ in range(nsims)])
                    ests = np.array([eval(line) for line in fp_in])

                    prob_chars = calc_chars(ests, times_dict[args_key])

                    row = ['{} {}'.format(method, ' '.join(map(str, method_params)))]
                    row.extend(prob_chars)

                    table_arr.append(row)

            except FileNotFoundError:
                print('Not found: ', filename_in)

    # Prepare first row of table
    first_row = [['', 'Estimate', 'Proc. time', '$\\xv{\\hat{\\gamma}}$', '$\\operatorname*{RE}[\\hat{\\gamma}]$', 'PTN-$\\xv{\\hat{\\gamma}}$', 'PTN-$\\operatorname*{RE}[\\hat{\\gamma}]$']]

    # Finally print table to outfile
    with open(filename_out, 'a+') as fp_out:
        print('Datetime simulation: ', datetimestr, file=fp_out)
        print('Datetime now: ', datetime.datetime.now().strftime("%Y%m%d_%H%M%S"), file=fp_out)
        print('nsims = {:,}'.format(nsims).replace(',', ' '), file=fp_out)
        print('rmax = {}'.format(rmax), file=fp_out)
        print('ndays = {}'.format(ndays), file=fp_out)

        # Workaround! Cannot make a full row of strings without destroying the formatting of floats
        # only need to delete 4 lines from outfile with this solution
        print(tabulate(first_row, tablefmt="latex_raw"), file=fp_out)
        print(tabulate(table_arr, tablefmt="latex_raw", floatfmt=".4g"), file=fp_out)

        print("\\caption{{Validation and comparison of multilevel splitting,"
              " $L = {0}, T = {1}$ days}}".format(L, T), file=fp_out
              )

        print(file=fp_out)
        print(file=fp_out)

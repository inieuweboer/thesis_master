import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

from importance_functions import plot_importance_contour
from text_render_latex import *
latexify(columns=2)


def optimize_levels_wadman(probs, levels, plot=False):
    """
    Optimization of levels as described in Wadman (2015) paragraph 6.3.1,
    originally Amrein & Kunsch 2011
    """
    assert len(probs) == len(levels), 'Need as many initial probability estimates as levels'

    popt = 0.2032
    log_popt = np.log(popt)
    #  new_nlevels = 6
    new_nlevels = 20

    cumul_probs = np.flip(np.cumprod(np.flip(probs)))
    log_cumul_probs = np.log(cumul_probs)

    log_popts = [k*log_popt for k in range(1, new_nlevels)]
    popts = np.exp(log_popts)

    new_levels = np.interp(log_popts, log_cumul_probs, levels)

    if plot:
        fig, ax = plt.subplots()

        new_levels = np.flip(np.unique(new_levels))
        l = new_levels.size
        popts = popts[:l]
        new_levels = np.append(new_levels, 1.)
        popts = np.append(popts, 1.)

        ax.plot(levels, cumul_probs, color='k')

        ax.scatter(levels, cumul_probs, color='k', s=1)
        ax.scatter(new_levels, popts, color='r', s=10)

        ax.hlines(popts, 0, new_levels, linestyle="dashed", linewidth=0.5)
        ax.vlines(new_levels, 0, popts, linestyle="dashed", linewidth=0.5)

        #  plt.yscale('log')
        #  plt.xlabel('Levels')
        #  plt.ylabel('Cumulative probabilities')
        ax.set_yscale('log')
        ax.set_xlabel('Levels')
        ax.set_ylabel('Cumulative probabilities')

        ax.annotate('$p_{{\\text{{opt}}}}$',
                    (new_levels[0], popts[0]),
                    xytext=(-25, 5), textcoords='offset points',
                    **{'fontsize': 'xx-small'},
                    )

        for i in range(1, new_levels.size - 1):
            ax.annotate('$p_{{\\text{{opt}}}}^{}$'.format(i+1),
                        (new_levels[i], popts[i]),
                        xytext=(-25, 5), textcoords='offset points',
                        **{'fontsize': 'xx-small'},
                        )

        fig.savefig('figs/levels_wadman.pdf', format='pdf')

    return np.unique(np.append(new_levels, 1.))


def optimize_levels_bisewski(probs, levels, gamma=None, plot=False):
    """
    Optimization of levels as described in Bisewski,
    originally Amrein & Kunsch 2011
    """
    assert len(probs) == len(levels), 'Need as many initial probability estimates as levels'

    #  c = 0.6275  # scipy.optimize.newton(lambda x: np.exp(1/x)*(2*x - 1) - 2*x, 0.6275004874579876)
    c = 0.6275004874579876

    if gamma is None:
        gamma = np.prod(probs)

    mopt = int(np.round(c * np.abs(np.log(gamma))))  #  mopt = 5
    print(mopt)
    log_popt = np.log(gamma) / mopt
    log_probs = np.log(probs)

    #  cumul_probs = np.flip(np.cumprod(np.flip(probs)))
    #  log_cumul_probs = np.log(cumul_probs)
    log_cumul_probs = np.flip(np.cumsum(np.flip(log_probs)))

    # Append the values that normally arent considered/interesting
    log_cumul_probs = np.append(log_cumul_probs, 0)
    levels = np.append(0, levels)

    log_popts = [k*log_popt for k in range(1, mopt)]

    new_levels = np.interp(log_popts, log_cumul_probs, levels)
    new_levels = np.unique(np.append(new_levels, 1.))

    if plot:
        fig, ax = plt.subplots()

        l = new_levels.size

        popts = np.exp(log_popts)
        cumul_probs = np.exp(log_cumul_probs)
        popts = np.flip(popts[:l])
        popts = np.append(popts, 1.)

        ax.plot(levels, cumul_probs, color='k')

        ax.scatter(levels, cumul_probs, color='k', s=1)
        ax.scatter(new_levels, popts, color='r', s=10)

        ax.hlines(popts, 0, new_levels, linestyle="dashed", linewidth=0.5)
        ax.vlines(new_levels, 0, popts, linestyle="dashed", linewidth=0.5)

        ax.set_yscale('log')
        ax.set_xlabel('Levels')
        ax.set_ylabel('Cumulative probabilities')

        if mopt > 1:
            ax.annotate('$p_{{\\text{{opt}}}}$',
                        (new_levels[-2], popts[-2]),
                        xytext=(-25, 5), textcoords='offset points',
                        **{'fontsize': 'xx-small'},
                        )

        for i in range(2, mopt):
            ax.annotate('$p_{{\\text{{opt}}}}^{}$'.format(i),
                        (new_levels[-i-1], popts[-i-1]),
                        xytext=(-25, 5), textcoords='offset points',
                        **{'fontsize': 'xx-small'},
                        )

        fig.savefig('figs/levels_bisewski.pdf', format='pdf')

    return new_levels


if __name__ == "__main__":
    np.random.seed(42)

    test_m = 6

    test_gamma = 0.001
    probs = np.full(shape=test_m, fill_value=np.power(test_gamma, 1/test_m))

    probs = np.arange(1, test_m + 1, dtype=float) / test_m
    #  prob_up = 0.5; probs = np.linspace(prob_up/test_m, prob_up, test_m)
    #  prob_up = 0.5; probs = np.linspace(prob_up/test_m, prob_up, test_m)
    #  np.random.shuffle(probs)
    #  prob_up = 0.5; probs = np.random.uniform(high=prob_up, size=test_m)

    # Equidistant levels
    levels = np.linspace(0.05, 1, test_m)

    plot_importance_contour(levels)
    new_levels = optimize_levels_wadman(probs, levels, plot=True)
    plot_importance_contour(new_levels)
    print(new_levels)

    #  plt.show()

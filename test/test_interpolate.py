import matplotlib.pyplot as plt
import numpy as np

from scipy import poly1d
from scipy.integrate import quad
from scipy.interpolate import BPoly
from scipy.interpolate import interp1d
from scipy.interpolate import lagrange
#  from scipy.special import legendre
from scipy.special import roots_chebyt, roots_chebyu, roots_legendre


def bernstein_poly(ys, a, b):
    assert len(ys.shape) == 1, 'Need one-dimensional data vector'
    #  assert ys.size == n, 'Size ys does not match with given n'

    xs = np.linspace(a, b, ys.size)

    return BPoly(np.row_stack(ys), [a, b])


def chebyshev_nodes(n, a=-1, b=1):
    #  return np.flip(np.cos( (2*np.arange(0, n) + 1) * np.pi / (2 * n)))
    return (b - a) * (np.flip(np.cos( (2*np.arange(0, n) + 1) * np.pi / (2 * n))) + 1)/2 + a


def chebyshev_zeros_second(n):
    return np.flip(np.cos( np.arange(1, n+1) / (n+1) * np.pi))


def test_legendre(n=6):
    """Plot first n Legendre polynomials"""
    xspace = np.linspace(-1, 1)

    #  legvalled = np.polynomial.legendre.legval(xspace, [0]*i + [1]))

    #  for i in range(n):
    #      legvalled = np.polynomial.legendre.legval(xspace, np.eye(n, k=n-1)[0])
    #      legvalled = np.polynomial.legendre.legval(xspace, np.eye(n)[-1])
    #      plt.plot(xspace, legvalled)

    for k in range(0, n):
        n1 = quad(lambda x: np.polynomial.legendre.legval(x, np.eye(k+1)[-1])**2, -1, 1)[0]
        n2 = 2 / (2*k + 1)
        print(n1, n2)

    out = np.polynomial.legendre.legval(xspace, np.eye(n))
    stacked_xspace = np.broadcast_to(xspace, (out.shape[0], xspace.size))

    plt.plot(stacked_xspace.T, out.T)
    plt.show()


def main():
    a = -1
    b = 1
    n = 11

    #  s = chebyshev_nodes(n)
    s = roots_chebyt(n)[0]
    #  s2 = chebyshev_zeros_second(n)
    s2 = roots_chebyu(n)[0]
    s_un = np.linspace(a, b, n)
    s_leg = roots_legendre(n)[0]

    eps = 0.05

    x = np.linspace(np.min(s) + eps, np.max(s) - eps, 1000)

    fn = lambda x: np.sin(np.exp(x))
    #  fn = lambda x: 1 / (1 + 25*x**2)

    f_cubic_cheb = interp1d(s, fn(s), kind='cubic')
    f_cubic_cheb2 = interp1d(s2, fn(s2), kind='cubic')
    f_cubic_un = interp1d(s_un, fn(s_un), kind='cubic')
    f_cubic_legendre = interp1d(s_leg, fn(s_leg), kind='cubic')
    #  f2 = lagrange(s, fn(s))
    #  f3 = bernstein_poly(fn(np.linspace(a, b, n)), a, b)

    print('Cubic spline Chebyshev first kind roots')  # best
    print(np.max(np.abs(fn(x) - f_cubic_cheb(x))))  # Max absolute error
    print(np.mean(np.abs(fn(x) - f_cubic_cheb(x))))  # Mean abs error
    print(np.square(fn(x) - f_cubic_cheb(x)).mean())  # Mean square error
    print()

    print('Cubic spline Chebyshev second kind roots')
    print(np.max(np.abs(fn(x) - f_cubic_cheb2(x))))  # Max absolute error
    print(np.mean(np.abs(fn(x) - f_cubic_cheb2(x))))  # Mean abs error
    print(np.square(fn(x) - f_cubic_cheb2(x)).mean())  # Mean square error
    print()

    print('Cubic spline Legendre roots')
    print(np.max(np.abs(fn(x) - f_cubic_legendre(x))))  # Max absolute error
    print(np.mean(np.abs(fn(x) - f_cubic_legendre(x))))  # Mean abs error
    print(np.square(fn(x) - f_cubic_legendre(x)).mean())  # Mean square error
    print()

    print('Cubic spline uniform roots')
    print(np.max(np.abs(fn(x) - f_cubic_un(x))))  # Max absolute error
    print(np.mean(np.abs(fn(x) - f_cubic_un(x))))  # Mean abs error
    print(np.square(fn(x) - f_cubic_un(x)).mean())  # Mean square error
    print()

    #  print()
    #  print('Lagrange')  # ok
    #  print(np.max(np.abs(fn(x) - f2(x))))  # Max absolute error
    #  print(np.mean(np.abs(fn(x) - f2(x))))  # Mean abs error
    #  print(np.square(fn(x) - f2(x)).mean())  # Mean square error
    #  print()
    #  print('Bernstein poly')  # Poor
    #  print(np.max(np.abs(fn(x) - f3(x))))  # Max absolute error
    #  print(np.mean(np.abs(fn(x) - f3(x))))  # Mean abs error
    #  print(np.square(fn(x) - f3(x)).mean())  # Mean square error

    plt.plot(x, fn(x), 'r')
    plt.plot(x, f_cubic_un(x))
    plt.show()


if __name__ == "__main__":
    #  test_legendre()
    main()

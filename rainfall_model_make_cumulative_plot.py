import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from rainfall_model import *

from helper_functions import rf_default_params

from text_render_latex import set_size


#  def plot_cumulative_proc(N=10**7, filename='figs/cumulative_plot', size=452.96796):
def cumulative_rainfall_plot_paths(params, c=np.ones(nstations), ndays=30, L=810,
        #  nruns=10**10,
        size=452.96796,
        filename='figs/presentation_cumul_precip_plot',
        #  fp=None,
        ):
    #  filename += '_' + str(N)

    #  with open('server/out/rainfall_cmc_data', 'r') as fp:
    #      lines = np.array([float(next(fp).strip()) for _ in range(N)])

    plt.figure(figsize=set_size(size))

    """
    c is such that the sum is taken, but could also look more localized
    """
    rmin, alpha, beta1, beta2, _, _ = params
    betas = np.array([beta1, beta2])

    amounts = []

    # Start dry
    X = np.zeros(nstations, dtype=int)
    amounts.append(np.zeros(nstations))

    for _ in range(ndays):
        X, R = one_step_multisite(params, X)

        # Add to amounts
        Y = R * X
        amounts.append(Y)

    amounts = np.array(amounts)

    # Cumulative sums for both individual weather stations, and the total
    cumsums_indiv = np.cumsum(amounts, axis=0)
    cumsums_tot = c @ cumsums_indiv.T

    tspace = np.arange(0, ndays+1)

    #  plt.plot(tspace, cumsums_indiv, linewidth=0.5)
    plt.plot(tspace, cumsums_tot, 'k')

    plt.plot(tspace, np.full_like(tspace, L), 'r--', linewidth=1)

    plt.xlabel('Time (days)')
    plt.ylabel('Rainfall (mm)')
    #  plt.show()
    # Save and remove excess whitespace
    plt.savefig(filename + '.pdf',
                format='pdf', transparent=True,
                )


###############
if __name__ == "__main__":
    np.random.seed()

    #  params = (15, 0.2, 2, 3)
    params = rf_default_params()
    cumulative_rainfall_plot_paths(params=params)

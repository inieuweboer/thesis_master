def cmc_approx_uniform(model_fn, a=-1, b=1, nsamples=100):
    Aspace = np.random.uniform(a, b, nsamples)

    samples = model_fn(Aspace)

    return np.mean(samples), np.std(samples, ddof=1.5)


def cmc_approx_beta(model_fn,
        calpha=1, cbeta=1,
        nsamples=100,
        affine_fn=lambda x: x):

    Bspace = np.random.beta(a=calpha, b=cbeta, size=nsamples)
    samples = model_fn(affine_fn(Bspace))

    return np.mean(samples), np.std(samples, ddof=1.5)



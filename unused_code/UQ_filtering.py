def gpc_bm_rm_prob_filter(
        T=30, L=26,
        K=30, M=30,
        #  a=-1, b=1,
        a=-10, b=10,
        #  a=0, b=2,
        sp_mult=20, sp_mult_L=4,
        printerrors=True,
        qq=False
        ):
    assert M >= K

    # Params
    #  L = 0.1  # No Gibbs phenomenon
    #  L = 5
    #  L = 18  # 0.001 for zero drift
    #  L = 26  # 2e-6 for zero drift

    # Watch out with indexing against L!
    Lspace = np.linspace(0, L, int(np.ceil(L*sp_mult_L + 1)))

    assert a < b, 'a not smaller than b'

    model_fn = lambda alpha: exact_prob_bm_rm(L, T, alpha)

    #####
    gpc_approx_dp_fn_L, fcs_dp_L, norms_sq_L = gpc_approx_discrete_proj_uniform_tree(
        model_fn,
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )

    gpc_approx_dp_raised_cosine_fn_L, _, _ = gpc_approx_discrete_proj_uniform_tree(
        model_fn,
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K,
        filter_fn=lambda eta: (1 + np.cos(np.pi * eta)) / 2
        )

    gpc_approx_dp_lanczos_fn_L, _, _ = gpc_approx_discrete_proj_uniform_tree(
        model_fn,
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K,
        filter_fn=lambda eta: np.sinc(eta)
        )

    drift_space = np.linspace(a, b, int(np.ceil(b - a))*sp_mult + 1)
    true_model_drift_space = model_fn(drift_space)

    gpc_approx_drift_space = gpc_approx_dp_fn_L(drift_space)
    gpc_approx_raised_cosine_drift_space = gpc_approx_dp_raised_cosine_fn_L(drift_space)
    gpc_approx_lanczos_drift_space = gpc_approx_dp_lanczos_fn_L(drift_space)

    #####
    # Print errors between expansion of (function of) and
    # true distribution function of prob BM with drift greater than L
    if printerrors:
        first_row = [['Method \\textbackslash{{}} errors.',
                      'Max. abs.',
                      'Mean abs.',
                      'Mean square',
                     ]]
        table_arr = []
        #  table_arr.append(list(('gPC',) +
        table_arr.append(list(('gPC $M = K = {}$'.format(M),) +
            calc_three_char_dists(true_model_drift_space,
                                     gpc_approx_drift_space,
                                     )))
        #  table_arr.append(list(('Raised cosine gPC',) +
        table_arr.append(list(('Raised cosine gPC $M = K = {}$'.format(M),) +
            calc_three_char_dists(true_model_drift_space,
                                     gpc_approx_raised_cosine_drift_space,
                                     )))
        #  table_arr.append(list(('Lanczos gPC',) +
        table_arr.append(list(('Lanczos gPC $M = K = {}$'.format(M),) +
            calc_three_char_dists(true_model_drift_space,
                                     gpc_approx_lanczos_drift_space,
                                     )))
        print(tabulate(first_row, tablefmt="latex_raw"))
        print(tabulate(table_arr, tablefmt="latex_raw", floatfmt=".4g"))
        print("\\caption{{Errors for filtered gPC expansions"
              "}}"
              #  " $M = {0}, K = {1}$}}".format(M, K)
              )

    #####
    # Plot true and approx using both naive gPC and func gPC approx
    fig1, ax1 = plt.subplots()

    figname = 'filter'

    ax1.plot(drift_space, true_model_drift_space, 'r--', label='True')
    ax1.plot(drift_space, gpc_approx_drift_space, 'midnightblue',
             linewidth=.75, label='gPC')
    ax1.plot(drift_space, gpc_approx_raised_cosine_drift_space, 'k',
             linewidth=.75, label='Raised cosine filtered gPC')
    ax1.plot(drift_space, gpc_approx_lanczos_drift_space, 'k',
             linewidth=.75, label='Lanczos filtered gPC')

    xlabel = '$\\alpha$'
    ylabel = '$\\gamma_{{\\scriptscriptstyle T, L}}' \
        '(\\alpha); T = {}, L = {}$'.format(T, L)
    set_axis_labels_endaxes(xlabel, ylabel, ax1)

    ax1.legend()
    ax1.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    ax1.axhline(1, linestyle=loosely_dotted, color='k', linewidth=.25)

    fig1.savefig(
        './figs/fit_alpha_gamma_gpc_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf',
        #  bbox_inches='tight',
        )

    ##

    fig_err, ax_err = plt.subplots()

    figname = 'filter_errors'

    ax_err.plot(drift_space,
                 true_model_drift_space - gpc_approx_drift_space,
                 'r--', linewidth=.75, label='gPC error')
    ax_err.plot(drift_space,
                 true_model_drift_space - gpc_approx_raised_cosine_drift_space,
                 'g--', linewidth=.75, label='Raised cosine gPC error')
    ax_err.plot(drift_space,
                 true_model_drift_space - gpc_approx_lanczos_drift_space,
                 'b--', linewidth=.75, label='Lanczos gPC error')

    xlabel = '$\\alpha$'
    ylabel = '$\\gamma_{{\\scriptscriptstyle T, L}}' \
        '(\\alpha); T = {}, L = {}$'.format(T, L)
    set_axis_labels_endaxes(xlabel, ylabel, ax_err)

    ax_err.legend()
    ax_err.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)

    fig_err.savefig(
        './figs/fit_alpha_gamma_gpc_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf',
        #  bbox_inches='tight',
        )

    ##

    fig_logplus, ax_logplus = plt.subplots()

    figname = 'filter_logplus_abs'

    ax_logplus.plot(drift_space,
                 np.log(1 + np.abs(true_model_drift_space - gpc_approx_drift_space)),
                 'r--', linewidth=.75, label='gPC logp error')
    ax_logplus.plot(drift_space,
                 np.log(1 + np.abs(true_model_drift_space - gpc_approx_raised_cosine_drift_space)),
                 'g--', linewidth=.75, label='Raised cosine gPC logp error')
    ax_logplus.plot(drift_space,
                 np.log(1 + np.abs(true_model_drift_space - gpc_approx_lanczos_drift_space)),
                 'b--', linewidth=.75, label='Lanczos filtered gPC logp error')

    xlabel = '$\\alpha$'
    ylabel = '$\\gamma_{{\\scriptscriptstyle T, L}}' \
        '(\\alpha); T = {}, L = {}$'.format(T, L)
    set_axis_labels_endaxes(xlabel, ylabel, ax_logplus)

    ax_logplus.legend()
    ax_logplus.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)

    fig_logplus.savefig(
        './figs/fit_alpha_gamma_gpc_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf',
        #  bbox_inches='tight',
        )


def gpc_bm_rm_prob_plot_gamma_L_filter(
        T=30, L=75,
        K=10, M=10,
        a=0, b=2,
        sp_mult_L=4
        ):
    """
    Plot gPC and CMC mean/std against L; normal
    """

    # Watch out with indexing against L!
    Lspace = np.linspace(0, L, int(np.ceil(L*sp_mult_L + 1)))
    Targ = -1

    assert a < b, 'a not smaller than b'

    model_fn_param = exact_prob_bm_rm

    # fixme alles omgooien kost ook weer tijd...
    def model_fn_param_new(param_space, gPC_var):
        return exact_prob_bm_rm(param_space, T, gPC_var)

    filter_fn=lambda eta: (1 + np.cos(np.pi * eta)) / 2

    #####
    fcs_dp, norms_sq = gpc_approx_discrete_proj_uniform_process(
        model_fn_param_new, Lspace, a, b,
        gauss_quad_deg=M,
        gpc_deg=K,
        )

    fcs_dp_filtered = fcs_dp * (filter_fn(np.arange(M+1) / (M+1)))

    gpc_dp_mean = fcs_dp[:, 0]
    gpc_dp_std = np.sqrt(np.nansum(
        fcs_dp[:, 1:]**2 * norms_sq[1:], axis=1))

    gpc_dp_filtered_mean = fcs_dp_filtered[:, 0]
    gpc_dp_filtered_std = np.sqrt(np.nansum(
        fcs_dp_filtered[:, 1:]**2 * norms_sq[1:], axis=1))

    fig_L, ax_L = plt.subplots()

    ax_L.plot(Lspace, gpc_dp_mean, label='gPC discrete projection mean')
    ax_L.fill_between(Lspace,
                      gpc_dp_mean - gpc_dp_std,
                      gpc_dp_mean + gpc_dp_std,
                      alpha=0.5, label='gPC discrete projection std')


    ax_L.plot(Lspace, gpc_dp_mean, label='gPC filtered discr. proj. mean')
    ax_L.fill_between(Lspace,
                      gpc_dp_filtered_mean - gpc_dp_filtered_std,
                      gpc_dp_filtered_mean + gpc_dp_filtered_std,
                      alpha=0.5, label='gPC filtered discrete projection std')

    #  ax_L.set_ylim([0, 1])
    ax_L.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    ax_L.axhline(1, linestyle=loosely_dotted, color='k', linewidth=.25)

    xlabel = '$L$'
    ylabel = '$\\gamma_{{\\scriptscriptstyle T, L}}(\\alpha); T = {}; ' \
        '\\alpha \\sim \\mathrm{{Un}}[{}, {}]$'.format(T, a, b)
    set_axis_labels_endaxes(xlabel, ylabel, ax_L)
    ax_L.legend()

    figname = 'normal_un_filtered'
    fig_L.savefig(
        './figs/UQ_L_gamma_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf',)


if __name__ == "__main__":
    for K, M in [(10, 10), (20, 20), (30, 30), (50, 50), (75, 75)]:
        gpc_bm_rm_prob_filter(K=K, M=M)


    gpc_bm_rm_prob_plot_gamma_L_filter()

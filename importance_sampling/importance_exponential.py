import numpy as np
from scipy import stats


def exact_prob_exp(beta, level):
    return np.exp(-level / beta)


def cmc_exp(beta, level, N):
    hits = (np.random.exponential(beta, size=N) >= level).astype(int)

    return np.mean(hits), np.var(hits, ddof=1)


def importance_sampler_exp(beta, level, N):
    # One can see that the condition for the indicator vector is
    # theoretically always true...
    hits = np.exp(-level / beta) * (np.random.exponential(beta, size=N) >= 0).astype(int)

    return np.mean(hits), np.var(hits, ddof=1)


if __name__ == "__main__":
    #  np.random.seed(42)

    beta = 2
    level = 10
    N = 100000
    print(exact_prob_exp(beta, level))
    print(cmc_exp(beta, level, N))
    print(importance_sampler_exp(beta, level, N))

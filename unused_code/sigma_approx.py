def fourier_approx(x, N):
    NN = 2*np.arange(0, N) + 1
    A = np.multiply.outer(NN , x)

    return (4 / np.pi) * np.sum(np.sin(A) / NN[:, np.newaxis], axis=0)


def sigma_approx(x, N):
    NN = 2*np.arange(0, N) + 1
    A = np.multiply.outer(NN , x)

    return (4 / np.pi) * np.sum(np.sin(A) * (np.sin(NN * np.pi / N) / NN**2)[:, np.newaxis], axis=0)


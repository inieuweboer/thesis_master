import numpy as np


def main():
    alpha = 0.4
    tail = 0.6

    Nsims = np.power(10, 6)
    N = np.power(10, 2)
    Nfull = np.full(Nsims, N)

    lln_est = np.random.binomial(Nfull, alpha) / Nfull
    lln_est_tail = np.mean((lln_est > tail).astype(int))

    ld_rate_fn = lambda s: s*np.log(s / alpha) + (1-s)*np.log((1-s)/(1-alpha))

    ld_est_tail = np.exp(-N*ld_rate_fn(tail))

    print(lln_est_tail)
    print(ld_est_tail)


if __name__ == "__main__":
    main()

import os
import shutil
import time

from helper_functions import (
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    rfparams_alpha, rfparams_ucov,
    arg_rmax,
    )
from rainfall_model_cumulative_splitting import *
from UQ_methods import gpc_approx_discrete_proj_uniform_tree_nodes


def main():
    # Get args
    args = rf_argparser()
    (datetimestr, nsims, T, nlevels, rfparams_def,
     gauss_quad_deg,
     node_index,
     a, b,
     Lmin, Lmax, Lindex, Lamt,
     outfolder,
     ) = (args.datetime, args.nsims,
          args.T, args.nlevels, args.rfparams,
          args.gauss_quad_deg,
          args.node_index,
          args.a, args.b,
          args.Lmin, args.Lmax, args.Lindex, args.Lamt,
          args.outfolder,
          )
    assert node_index < gauss_quad_deg

    work_nodes = gpc_approx_discrete_proj_uniform_tree_nodes(
        a=a, b=b,
        gauss_quad_deg=gauss_quad_deg,  # M
        )
    rfparams = rfparams_ucov(rfparams_def, work_nodes, node_index)

    rmax = arg_rmax(Lmin, Lmax, Lindex, Lamt)
    ndays = T
    importance_fn_distance = make_importance_fn_distance(ndays, rmax)

    # c = 0.6275; gamma = 0.001; int(np.round(c * np.abs(np.log(gamma))))

    alg_dict = {
                'CMC': cumulative_rainfall_cmc,
                'FE': cumulative_rainfall_splitting_fixed_effort,
                'FNS': cumulative_rainfall_splitting_fixed_successes,
                'FO': cumulative_rainfall_splitting_fixed_offspring,
                }

    # Prepare filenames
    tofolder = outfolder
    if args.test:
        outfolder = outfolder + 'test/'
        tofolder = tofolder + 'test/'

    if not args.local:
        outfolder = '{}/multi_splitting_{}/{}'.format(
            os.getenv("TMPDIR"),
            datetimestr,
            outfolder)
        tofolder = '{}/{}'.format(os.getenv("HOME"), tofolder)
    else:
        outfolder = './' + outfolder

    os.makedirs(outfolder, exist_ok=True)
    os.makedirs(tofolder, exist_ok=True)

    name = '_rf_'
    filename_out_pre = "".join([outfolder, datetimestr, name])
    filename_to_pre = "".join([tofolder, datetimestr, name])

    for method in ['CMC', 'FE', 'FNS', 'FO', ]:
        for method_params in getattr(args, method):
            if method_params[0] < 0:
                break

            args_key = args_str_maker(
                method, method_params,
                #  nsims=nsims,
                L=rmax, T=ndays, nlevels=nlevels,
                rfparamshash=rf_params_tostring(rfparams),
                )
            outfile = filename_out_pre + args_key
            tofile = filename_to_pre + args_key

            alg_params = (nlevels, *method_params, importance_fn_distance)

            with open(outfile, 'a+') as fp_meth:
                t = time.process_time()

                for _ in range(nsims):
                    print(alg_dict[method](params=rfparams, rmax=rmax, ndays=ndays,
                                        alg_params=alg_params,
                        ),
                        file=fp_meth)

                elapsed = time.process_time() - t

            if not args.local:
                shutil.copy(outfile, tofile)

            print('Dictionary entry node {}: {}'.format(node_index, {args_key: elapsed}))


if __name__ == "__main__":
    np.random.seed()
    np.seterr(divide='ignore')

    main()

import matplotlib.pyplot as plt
import numpy as np
from scipy.special import erf

from itertools import cycle

from importance_functions import (
    plot_levels,
    make_importance_fn_brownian_motion,
    make_importance_fn_distance,
    make_importance_fn_large_deviation,
    )

from UQ_bm_drift_rm import gpc_approx_discrete_proj_process

from scipy.special import erf, expit, logit
from optimize_levels import optimize_levels
from text_render_latex import *
latexify(columns=2)


def Phi(z):
    return (1 + erf(z / np.sqrt(2))) / 2


def exact_prob_bm_rm(L, T, drift):
    r"""
    $\xprob{ M_T^{(\alpha)} > L}$ where
    $M_T^{(\\alpha)} := \sup_{0 \le t \le T} X_t$ and
    $X_t := W_t + \alpha t$

    https://math.stackexchange.com/questions/1053294/density-of-first-hitting-time-of-brownian-motion-with-drift
    """
    return Phi((drift*T - L) / np.sqrt(T)) + \
        np.exp(2*L*drift) * Phi(-(L + drift*T) / np.sqrt(T))


def bm_drift_splitting_fixed_successes(L, T, drift,
                                 alg_params=(None, 4, 40, None),
                                 optimize=False,
                                 #  show=False, save=False,
                                 ax=plt,
                                 ):
    """
    nlevels is no of levels
    success is no of succeeded particles per level required
    """
    #  stepsize, nlevels, success, max_bootstraps, importance_fn = alg_params
    stepsize, nlevels, success, importance_fn = alg_params

    # Step size semi-agnostic of threshold L and amount of levels. Dividing
    # by 100 means a standard deviation factor of 10 between two levels,
    # which makes the probability of multiple level-skipping quite low
    if stepsize is None:
        #  stepsize = 0.01
        stepsize = L / ((nlevels + 1) * 100)

    if importance_fn is None:
        importance_fn = make_importance_fn_distance(T, L)

    # Breadth-first algorithm
    def breadth_first(plot=True):
        # Start dry, total pos, entry time;
        particles = [(0, 0)]

        for lvl_index in range(nlevels):
            survived_particles = []

            # Iterator for generating random cycled picks
            it = cycle(np.random.permutation(len(particles)))

            # Go on until success level reached or max bootstraps reached
            #  for _ in range(max_bootstraps):
            #      if len(survived_particles) == success:
            #          break
            while len(survived_particles) < success:

                #  index = np.random.randint(len(particles))
                index = next(it)

                pos, entry_time = particles[index]
                trajectory = [pos]

                # Skip current particle if entry time after end time
                if entry_time >= T:
                    continue

                particle_amounts[lvl_index] += 1

                # Pre-sample normally distributed steps
                nsteps = int((T - entry_time) / stepsize) + 1
                samples = iter(np.random.normal(scale=np.sqrt(stepsize),
                                                size=nsteps) + drift*stepsize)

                split_color = 'red'

                for t in np.arange(entry_time, T, stepsize):
                    pos += next(samples)
                    trajectory.append(pos)

                    # Save current particle if survived
                    if importance_fn(t, pos) >= levels[lvl_index]:
                        survival_amounts[lvl_index] += 1

                        # Add at next time step
                        survived_particles.append((pos, t + stepsize))

                        split_color = 'green'

                        break

                if plot:
                    # Note that particle trajectories overlap!
                    ax.plot(
                        np.linspace(
                            entry_time,
                            entry_time + (len(trajectory) - 1)*stepsize,
                            len(trajectory)),
                        trajectory,
                        #  color=(0, 0, lvl_index/nlevels),
                        color=((nlevels - lvl_index - 1)*2/(nlevels*3),
                               (nlevels - lvl_index - 1)*2/(nlevels*3),
                               (nlevels - lvl_index - 1)*2/(nlevels*3)),
                        linewidth=.01)

                    # Plot endpoint of trajectory
                    if len(trajectory) > 1:
                        ax.plot(entry_time + (len(trajectory) - 1)*stepsize,
                                 trajectory[-1],
                                 color=split_color, marker='.', zorder=100)

            particles = survived_particles

    # Set standard equidistant levels
    levels = [lvl_index/nlevels for lvl_index in range(1, nlevels+1)]

    if optimize:
        # Set survival amounts to zero and do a test run
        survival_amounts = np.zeros(nlevels, dtype=int)
        particle_amounts = np.zeros(nlevels, dtype=int)
        breadth_first(False)

        # Re-run with optimized levels
        probs = np.array(particle_amounts, dtype=float)
        probs /= particle_amounts
        levels = optimize_levels(probs, levels)
        nlevels = len(levels)

    survival_amounts = np.zeros(nlevels, dtype=int)
    particle_amounts = np.zeros(nlevels, dtype=int)
    breadth_first()

    # Plot the levels
    plot_levels(T, L, importance_fn, levels, ax=ax, xlabel='', ylabel='')

    xticks = list(map(int, ax.get_xticks().tolist()))
    xticks[-2] = 'T={}'.format(xticks[-2])
    ax.set_xticklabels(xticks)

    yticks = list(map(int, ax.get_yticks().tolist()))
    yticks[-2] = 'L={}'.format(yticks[-2])
    ax.set_yticklabels(yticks)

    # Calc prob (unneeded for plot)
    probs = np.array(survival_amounts, dtype=float)
    probs /= particle_amounts

    #  # eq 5.14 in Wadman
    #  sre_bound = -1 + np.prod(1 / (survival_amounts - 2) + 1)
    est = np.prod(probs)
    #  var_bound = sre_bound * est * est

    #  fig.canvas.draw()

    return est
    #  return est, var_bound, np.sqrt(sre_bound)


def gpc_bm_rm_prob_plot_gamma_L_custom(
        T=30, L=75,
        K=10, M=11,
        #  a=-1, b=1,
        a=0, b=2,
        sp_mult_L=24,
        #  biases=False,
        ax=plt,
        vlineL=None,
        prob_est=None,
        ):
    """
    Plot gPC and CMC mean/std against L; normal/log/logit
    """

    # Watch out with indexing against L!
    Lspace = np.linspace(0, L, int(np.ceil(L*sp_mult_L + 1)))
    Targ = -1

    assert a < b, 'a not smaller than b'

    model_fn_full = exact_prob_bm_rm

    fcs_dp, norms_sq = gpc_approx_discrete_proj_process(
        model_fn_full,
        Lspace, T, a, b,
        gauss_quad_deg=M,
        gpc_deg=K,
        )

    gpc_dp_mean = fcs_dp[:, Targ, 0]
    gpc_dp_std = np.sqrt(np.nansum(
        fcs_dp[:, Targ, 1:]**2 * norms_sq[1:], axis=1))

    ax.plot(Lspace, gpc_dp_mean, label='gPC dp mean')
    ax.fill_between(Lspace,
                    gpc_dp_mean - gpc_dp_std,
                    gpc_dp_mean + gpc_dp_std,
                    alpha=0.2, label='gPC dp std')

    if vlineL is not None:
        ax.axvline(vlineL, linestyle=loosely_dotted, color='k', linewidth=.5)
        print('Mean prob at L = {}: {}'.format(vlineL, gpc_dp_mean[Lspace == vlineL][0]))

    if prob_est is not None:
        ax.plot(vlineL, prob_est, 'r.')

    ylabel = '$\\gamma_{{\scriptscriptstyle T={0}, L}}(\\alpha) = \\mathbb{{P}}[\\sup_{{0 \\le t \\le T}} W_t + \\alpha t > L]$'.format(T)

    set_axis_labels_endaxes('$L$', ylabel, ax)
    #  ax.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    #  ax.axhline(1, linestyle=loosely_dotted, color='k', linewidth=.25)


if __name__ == "__main__":
    import time

    #  rand = np.random.randint(1000)
    #  print(rand)
    #  np.random.seed(rand)
    np.random.seed(7)
    np.seterr(divide='ignore')

    L = 5
    T = 30
    drift = 0.5

    #  print('L = ', L)
    #  print('T = ', T)
    #  print('drift = ', drift)

    #  print(exact_prob_bm_rm(L, T, drift))

    stepsize = 0.01

    nlevels = 3
    success = 5

    figname = 'frontpage'

    fig, axes = plt.subplots(nrows=1, ncols=2)
    fig.set_figheight(fig.get_figheight() / 3 * 2)

    prob_est = bm_drift_splitting_fixed_successes(
        L=L, T=T, drift=drift,
        alg_params=(stepsize, nlevels,
                    success,
                    make_importance_fn_brownian_motion(T, L)
                    ),
        #  alg_params=(stepsize, nlevels,
        #              success, None),
        ax=axes[0]
        )

    K, M, a, b = 10, 10, 0, 1

    gpc_bm_rm_prob_plot_gamma_L_custom(T=T, L=5*L, K=K, M=M, a=a, b=b, ax=axes[1], vlineL=L, prob_est=None)
    #  gpc_bm_rm_prob_plot_gamma_L_custom(T=T, L=5*L, K=K, M=M, a=a, b=b, ax=axes[1], vlineL=L, prob_est=prob_est)

    fig.savefig(
        './figs/{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf',
        )

import numpy as np

from itertools import permutations

from opt_einsum import contract
from orthopy.line_segment import tree_jacobi, tree_legendre

from scipy.integrate import quad
from scipy.optimize import lsq_linear
from scipy.stats import beta as beta_dist
from scipy.special import factorial
from scipy.special import gamma as gamma_fn
from scipy.special import ndtr as normal_cdf
from scipy.special import ndtri as normal_cdf_inv
from scipy.special import roots_legendre, roots_sh_jacobi, roots_chebyt
from quadpy import quad as quad_vect

from helper_functions import make_affine_transf


def gpc_approx_interp(model_fn, a=-1, b=1,
                      gpc_deg=10,  # K
                      nnodes=None,  # M
                      #  nnodes=10,  # M
                      returnnodes=False,
                      weights=None,
                      ):
    gpc_deg += 1  # Include K itself

    if nnodes is None:
        nnodes = gpc_deg
    elif nnodes < gpc_deg:
        print('Warning: M = {}, K = {}, must have M >= K + 1 to prevent overfitting/underdetermination'.format(nnodes, gpc_deg - 1))

    # Transformations to support of Legendre polynomials and back
    affine_fn = make_affine_transf(-1, 1, a, b)
    affine_fn_inv = make_affine_transf(a, b, -1, 1)

    orthopolval = np.polynomial.legendre.legval

    # Calculate interpolation nodes
    orthopolroots = np.polynomial.legendre.legroots
    nodes = orthopolroots(np.eye(nnodes + 1)[-1])
    #  orthopolroots = lambda n: roots_chebyt(n)[0]  # better? worse? TODO
    #  nodes = orthopolroots(gpc_deg)

    norms_sq = 1 / (2 * np.arange(gpc_deg) + 1)

    # Vandermonde-like matrix
    Phi_km = orthopolval(nodes, np.eye(gpc_deg))

    # Calculate values of model on transformed nodes
    model_fn_vec = model_fn(affine_fn(nodes))

    # Get coefs through solving
    #  fourier_coefs = np.linalg.solve(Phi_km.T, model_fn_vec)
    if weights is None:
        fourier_coefs = lsq_linear(Phi_km.T, model_fn_vec).x
    elif isinstance(weights, bool) and weights:
        _, weights, wsum = roots_legendre(nnodes, mu=True)
        weights /= wsum
        weights = np.sqrt(weights)
        W = np.diag(weights)
        fourier_coefs = lsq_linear(W @ Phi_km.T, weights * model_fn_vec).x
    else:
        weights = np.sqrt(weights)
        W = np.diag(weights)
        fourier_coefs = lsq_linear(W @ Phi_km.T, weights * model_fn_vec).x

    # Function that transforms back and evaluates Fourier/gPC expansion
    def gpc_approx_fn(z):
        return orthopolval(affine_fn_inv(z), fourier_coefs)

    if returnnodes:
        return gpc_approx_fn, fourier_coefs, norms_sq, affine_fn(nodes)

    return gpc_approx_fn, fourier_coefs, norms_sq


def gpc_approx_interp_process(model_fn_param, Lspace, Tspace, a=-1, b=1,
                              gpc_deg=10,  # K
                              #  nnodes=None,  # M
                              #  nnodes=10,  # M
                              ):
    gpc_deg += 1  # Include K itself

    #  if nnodes is None:
    nnodes = gpc_deg

    # Transformation to support of Legendre polynomials
    affine_fn = make_affine_transf(-1, 1, a, b)

    orthopolval = np.polynomial.legendre.legval

    orthopolroots = np.polynomial.legendre.legroots
    nodes = orthopolroots(np.eye(nnodes + 1)[-1])
    #  orthopolroots = lambda n: roots_chebyt(n)[0]
    #  nodes = orthopolroots(gpc_deg)

    norms_sq = 1 / (2 * np.arange(gpc_deg) + 1)

    # Vandermonde-like matrix
    Phi_km = orthopolval(nodes, np.eye(gpc_deg))

    LL, AA, TT = np.meshgrid(Lspace, affine_fn(nodes),
                             Tspace, indexing='ij')
    model_fn_vec = model_fn_param(LL, TT, AA)

    # Get coefs through solving
    fourier_coefs = np.linalg.solve(Phi_km.T, model_fn_vec)
    #  fourier_coefs = np.linalg.lstsq(Phi_km.T, model_fn_vec, rcond=None)[0]
    fourier_coefs = np.swapaxes(fourier_coefs, 1, 2)  # To l, t, m

    return fourier_coefs, norms_sq


def gpc_approx_discrete_proj_uniform(
        model_fn, a=-1, b=1,
        gauss_quad_deg=10, gpc_deg=10,  # M, K
        #  gauss_quad_deg=100, gpc_deg=100,  # M, K
        assert_norms=False,
        returnnodes=False,
        ):
    """
    gauss_quad_deg: amount of nodes where Gauss quadrature corresponding to
        gPC projection is applied
    gpc_deg: deg of gPC expansion, i.e. amt of terms - 1 (note zeroth degree)
    """
    gpc_deg += 1  # Include K itself

    if 2*gauss_quad_deg - gpc_deg - 1 < 3:
        print('Warning: M = {}, K = {}: must have deg(f) <= 2M - K - 1 to prevent aliasing errors'.format(gauss_quad_deg, gpc_deg - 1))

    # Transformation to support of Legendre polynomials and back
    affine_fn = make_affine_transf(-1, 1, a, b)
    affine_fn_inv = make_affine_transf(a, b, -1, 1)

    orthopolval = np.polynomial.legendre.legval
    orthopolgauss = roots_legendre

    # Legendre-Gauss quad against uniform (lebesgue) measure
    # Gaussian quad has to be of same type as the gPC expansion to get
    # the correct mean, variance etc, i.e. the correct coefficients
    nodes, weights, wsum = orthopolgauss(gauss_quad_deg, mu=True)

    # Get the norms and norms multiplied by sum of weights (weights have
    # to be normalized)
    norms_sq = 1 / (2 * np.arange(gpc_deg) + 1)
    norms_sq_wsum = wsum * norms_sq

    if assert_norms:
        norms_quad = quad_vect(lambda u: orthopolval(u, np.eye(gpc_deg))**2,
                               -1, 1)[0]
        assert np.allclose(norms_sq_wsum, norms_quad)

    Phi_km = orthopolval(nodes, np.eye(gpc_deg))
    model_fn_vec = model_fn(affine_fn(nodes))
    fourier_coefs = Phi_km @ (model_fn_vec * weights) / norms_sq_wsum

    def gpc_approx_fn(z): return orthopolval(affine_fn_inv(z), fourier_coefs)

    if returnnodes:
        return gpc_approx_fn, fourier_coefs, norms_sq, affine_fn(nodes)

    return gpc_approx_fn, fourier_coefs, norms_sq


# Generalize all other functions when time allows?
def gpc_approx_discrete_proj_uniform_process(model_fn_param,
        param_space, a=-1, b=1,
        gauss_quad_deg=10, gpc_deg=10,
        ):
    gpc_deg += 1  # Include K itself

    # Transformation to support of Legendre polynomials
    affine_fn = make_affine_transf(-1, 1, a, b)

    orthopolval = np.polynomial.legendre.legval
    orthopolgauss = roots_legendre

    # Legendre-Gauss quad against a uniform (lebesgue) measure
    nodes, weights, wsum = orthopolgauss(gauss_quad_deg, mu=True)

    # Legendre polynomial expansion
    norms_sq = 1 / (2 * np.arange(gpc_deg) + 1)
    norms_sq_wsum = wsum * norms_sq

    Phi_km = orthopolval(nodes, np.eye(gpc_deg))

    PP, NN = np.meshgrid(param_space, affine_fn(nodes), indexing='ij')
    model_fn_vec = model_fn_param(PP, NN)

    fourier_coefs = contract(
        'km,lm->lk', Phi_km / norms_sq_wsum[:, np.newaxis],
        model_fn_vec * weights)

    return fourier_coefs, norms_sq


# Same as previous but not generalized from BM in Lspace and Tspace!
def gpc_approx_discrete_proj_process(model_fn_param, Lspace, Tspace, a=-1, b=1,
                                     gauss_quad_deg=100, gpc_deg=100
                                     ):
    gpc_deg += 1  # Include K itself

    # Transformation to support of Legendre polynomials
    affine_fn = make_affine_transf(-1, 1, a, b)

    orthopolval = np.polynomial.legendre.legval
    orthopolgauss = roots_legendre

    # Legendre-Gauss quad against a uniform (lebesgue) measure
    nodes, weights, wsum = orthopolgauss(gauss_quad_deg, mu=True)

    # Legendre polynomial expansion
    norms_sq = 1 / (2 * np.arange(gpc_deg) + 1)
    norms_sq_wsum = wsum * norms_sq

    Phi_km = orthopolval(nodes, np.eye(gpc_deg))

    LL, TT, AA = np.meshgrid(Lspace, Tspace, affine_fn(nodes), indexing='ij')
    model_fn_vec = model_fn_param(LL, TT, AA)

    fourier_coefs = contract(
        'km,ltm->ltk', Phi_km / norms_sq_wsum[:, np.newaxis],
        model_fn_vec * weights)

    return fourier_coefs, norms_sq


#####

def gpc_approx_discrete_proj_uniform_tree(
        model_fn,
        a=-1, b=1,
        gauss_quad_deg=10, gpc_deg=10,  # M, K
        model_fn_vectorized=True,
        filter_fn=None,
        ):

    # Transformation to support of Legendre polynomials and back
    affine_fn = make_affine_transf(-1, 1, a, b)
    affine_fn_inv = make_affine_transf(a, b, -1, 1)

    def orthopolval(x):
        return np.array(tree_legendre(
            X=x, n=gpc_deg,
            standardization="p(1)=(n+alpha over n)"))

    orthopolgauss = roots_legendre

    # Legendre-Gauss quad against a uniform (lebesgue) measure
    nodes, weights, wsum = orthopolgauss(gauss_quad_deg, mu=True)

    # Legendre polynomial expansion
    norms_sq = 1 / (2 * np.arange(gpc_deg + 1) + 1)
    norms_sq_wsum = wsum * norms_sq

    Phi_km = orthopolval(nodes)

    if model_fn_vectorized:
        model_fn_vec = model_fn(affine_fn(nodes))
    else:
        model_fn_vec = []

        for node in nodes:
            model_fn_vec.append(model_fn(affine_fn(node)))

        model_fn_vec = np.array(model_fn_vec)

    fourier_coefs = (Phi_km @ (model_fn_vec * weights)) / norms_sq_wsum

    if filter_fn is None:
        def gpc_approx_fn(z):
            return fourier_coefs @ orthopolval(affine_fn_inv(z))
    else:
        def gpc_approx_fn(z):
            return (fourier_coefs * filter_fn(np.arange(gauss_quad_deg+1) / (gauss_quad_deg+1))) @ orthopolval(affine_fn_inv(z))
            #  return (fourier_coefs * np.sinc((np.arange(gauss_quad_deg+1) * np.pi) / (np.pi * gauss_quad_deg))) @ orthopolval(affine_fn_inv(z))

    return gpc_approx_fn, fourier_coefs, norms_sq


def gpc_approx_discrete_proj_uniform_tree_normal_transf(
        model_fn,
        #  mu, sigma,
        gauss_quad_deg=10, gpc_deg=10,  # M, K
        model_fn_vectorized=True,
        filter_fn=None,
        ):

    # Transformation to support of Legendre polynomials and back
    affine_fn = make_affine_transf(-1, 1, 0, 1)
    affine_fn_inv = make_affine_transf(0, 1, -1, 1)

    transf_fn = lambda y: normal_cdf_inv(affine_fn(y))
    transf_fn_inv = lambda z: affine_fn_inv(normal_cdf(z))

    def orthopolval(x):
        return np.array(tree_legendre(
            X=x, n=gpc_deg,
            standardization="p(1)=(n+alpha over n)"))

    orthopolgauss = roots_legendre

    # Legendre-Gauss quad against a uniform (lebesgue) measure
    nodes, weights, wsum = orthopolgauss(gauss_quad_deg, mu=True)

    # Legendre polynomial expansion
    norms_sq = 1 / (2 * np.arange(gpc_deg + 1) + 1)
    norms_sq_wsum = wsum * norms_sq

    Phi_km = orthopolval(nodes)

    if model_fn_vectorized:
        model_fn_vec = model_fn(transf_fn(nodes))
    else:
        model_fn_vec = []

        for node in nodes:
            model_fn_vec.append(model_fn(transf_fn(node)))

        model_fn_vec = np.array(model_fn_vec)

    fourier_coefs = (Phi_km @ (model_fn_vec * weights)) / norms_sq_wsum

    if filter_fn is None:
        def gpc_approx_fn(z):
            return fourier_coefs @ orthopolval(transf_fn_inv(z))
    else:
        def gpc_approx_fn(z):
            return (fourier_coefs * filter_fn(np.arange(gauss_quad_deg+1) / (gauss_quad_deg+1))) @ orthopolval(transf_fn_inv(z))
            #  return (fourier_coefs * np.sinc((np.arange(gauss_quad_deg+1) * np.pi) / (np.pi * gauss_quad_deg))) @ orthopolval(transf_fn_inv(z))

    return gpc_approx_fn, fourier_coefs, norms_sq


def gpc_approx_discrete_proj_uniform_tree_nodes(
        a=-1, b=1,
        gauss_quad_deg=8,  # M
        ):

    # Transformation from support of Legendre polynomials
    affine_fn = make_affine_transf(-1, 1, a, b)

    orthopolgauss = roots_legendre

    # Legendre-Gauss quad against a uniform (lebesgue) measure
    nodes, _ = orthopolgauss(gauss_quad_deg)

    return affine_fn(nodes)


def gpc_approx_discrete_proj_uniform_tree_from_data(
        model_fn_vec,
        a=-1, b=1,
        gauss_quad_deg=10, gpc_deg=10,  # M, K
        ):

    # Transformation to support of Legendre polynomials
    affine_fn_inv = make_affine_transf(a, b, -1, 1)

    def orthopolval(x):
        return np.array(tree_legendre(
            X=x, n=gpc_deg,
            standardization="p(1)=(n+alpha over n)"))

    orthopolgauss = roots_legendre

    # Legendre-Gauss quad against a uniform (lebesgue) measure
    nodes, weights, wsum = orthopolgauss(gauss_quad_deg, mu=True)

    # Legendre polynomial expansion
    norms_sq = 1 / (2 * np.arange(gpc_deg + 1) + 1)
    norms_sq_wsum = wsum * norms_sq

    Phi_km = orthopolval(nodes)
    fourier_coefs = (Phi_km @ (model_fn_vec * weights)) / norms_sq_wsum

    def gpc_approx_fn(z):
        return fourier_coefs @ orthopolval(affine_fn_inv(z))

    return gpc_approx_fn, fourier_coefs, norms_sq


def gpc_approx_discrete_proj_param_uniform_tree_from_data(
        model_fn_arr,
        a=-1, b=1,
        gauss_quad_deg=10, gpc_deg=10,  # M, K
        ):

    # Transformation to support of Legendre polynomials
    affine_fn_inv = make_affine_transf(a, b, -1, 1)

    def orthopolval(x):
        return np.array(tree_legendre(
            X=x, n=gpc_deg,
            standardization="p(1)=(n+alpha over n)"))

    orthopolgauss = roots_legendre

    # Legendre-Gauss quad against a uniform (lebesgue) measure
    nodes, weights, wsum = orthopolgauss(gauss_quad_deg, mu=True)

    # Legendre polynomial expansion
    norms_sq = 1 / (2 * np.arange(gpc_deg + 1) + 1)
    norms_sq_wsum = wsum * norms_sq

    Phi_km = orthopolval(nodes)

    fourier_coefs = contract(
        'km,lm->lk', Phi_km / norms_sq_wsum[:, np.newaxis],
        model_fn_arr * weights)

    return fourier_coefs, norms_sq


def gpc_approx_discrete_proj_beta(model_fn,
                                  a=0, b=1,
                                  calpha=1, cbeta=1,
                                  gauss_quad_deg=10, gpc_deg=10,  # M, K
                                  assert_norms=False,
                                  ):
    """
    calpha and cbeta > 0 from Beta convention
    """
    assert calpha > 0, 'calpha should be positive'
    assert cbeta > 0, 'cbeta should be positive'

    # Transformation from support of shifted Jacobi polynomials and back
    affine_fn_jac = make_affine_transf(0, 1, 1, -1)

    affine_fn = make_affine_transf(0, 1, a, b)
    affine_fn_inv = make_affine_transf(a, b, 0, 1)

    def orthopolval(u):
        return np.array(tree_jacobi(
            X=affine_fn_jac(u), n=gpc_deg,
            alpha=calpha - 1, beta=cbeta - 1,
            standardization="p(1)=(n+alpha over n)"))

    c = calpha + cbeta - 1

    def orthopolgauss(deg):
        return roots_sh_jacobi(deg, c, calpha, mu=True)

    z_nodes, weights, wsum = orthopolgauss(gauss_quad_deg)
    Phi_km = orthopolval(z_nodes)
    model_fn_vec = model_fn(affine_fn(z_nodes))

    N = np.arange(1, gpc_deg + 1)

    # Gamma fn has singularity in 0, and know that first coefficient is equal
    # to B(alpha, beta) = wsum
    norms_sq_wsum = gamma_fn(N + calpha) * gamma_fn(N + cbeta) \
        / ((2*N + c) * gamma_fn(N + c) * factorial(N))
    norms_sq_wsum = np.append(wsum, norms_sq_wsum)
    norms_sq = norms_sq_wsum / wsum

    if assert_norms:
        norms_quad = quad_vect(lambda u: orthopolval(u)**2 *
                               beta_dist.pdf(u, a=calpha, b=cbeta), 0, 1)[0]
        assert np.allclose(norms_sq, norms_quad)

    fourier_coefs = Phi_km @ (model_fn_vec * weights) / norms_sq_wsum

    def gpc_approx_fn(z):
        return fourier_coefs @ orthopolval(affine_fn_inv(z))

    return gpc_approx_fn, fourier_coefs, norms_sq


#####


def gpc_approx_from_fc(fcs, a=-1, b=1):
    affine_fn = make_affine_transf(a, b, -1, 1)

    return lambda z: np.polynomial.legendre.legval(affine_fn(z), fcs)


def cmc_approx_process(model_fn_param,
                       Lspace, Tspace, a=-1, b=1, nsamples=100):
    Aspace = np.random.uniform(a, b, nsamples)

    LL, TT = np.meshgrid(Lspace, Tspace, indexing='ij')

    # Due to memory limitations...
    samples = []
    for alpha in Aspace:
        samples.append(model_fn_param(LL, TT, alpha))

    return np.array(samples)


def make_three_tensor_legendre(K):
    def orthopolval(u, deg):
        return np.polynomial.legendre.legval(u, np.eye(deg)[-1])

    return make_three_tensor(orthopolval=orthopolval, K=K, wsum=2, a=-1, b=1)


def make_three_tensor(orthopolval, K, wsum=2, a=-1, b=1):
    indices = [(i, j, k) for i in range(K+1) for j in range(i+1)
               for k in range(j+1) if i >= abs(j - k) and j + k >= i]
    three_tensor = np.zeros((K+1, K+1, K+1))

    for i, j, k in indices:
        # can this be further optimized through vectorization?
        quad_out = quad(
            lambda u: orthopolval(u, i+1) * orthopolval(u, j+1) * orthopolval(u, k+1),
            a, b)[0] / wsum

        for perm in set(permutations((i, j, k))):
            three_tensor[perm] = quad_out

    assert np.allclose(three_tensor, np.swapaxes(three_tensor, 0, 1))
    assert np.allclose(three_tensor, np.swapaxes(three_tensor, 0, 2))

    return three_tensor


def make_four_tensor_legendre_old(K):
    def orthopolval(u, deg):
        # Include deg itself in eye matrix, counting from 0 to K
        return np.polynomial.legendre.legval(u, np.eye(deg + 1)[-1])

    return make_four_tensor(orthopolval=orthopolval, K=K, wsum=2, a=-1, b=1)


def make_four_tensor(orthopolval, K, wsum=2, a=-1, b=1):
    indices = [(i, j, k, l) for i in range(K+1) for j in range(i+1)
               for k in range(j+1) for l in range(k+1)
               if j + k >= abs(i - l) and i + l >= abs(j - k)
               ]
    four_tensor = np.zeros((K+1, K+1, K+1, K+1))

    for i, j, k, l in indices:
        quad_out = quad(
            lambda u: orthopolval(u, i) * orthopolval(u, j) * \
                      orthopolval(u, k) * orthopolval(u, l),
            a, b)[0] / wsum

        for perm in set(permutations((i, j, k, l))):
            four_tensor[perm] = quad_out

    assert np.allclose(four_tensor, np.swapaxes(four_tensor, 0, 1))
    assert np.allclose(four_tensor, np.swapaxes(four_tensor, 0, 2))
    assert np.allclose(four_tensor, np.swapaxes(four_tensor, 0, 3))

    return four_tensor


def make_four_tensor_legendre(K):
    # Faster

    def orthopolval(u, deg):
        # Include deg itself in eye matrix, counting from 0 to K
        return np.polynomial.legendre.legval(u, np.eye(deg + 1)[-1])

    # Legendre pols of even order are even and of odd order are odd!
    indices = [(i, j, k, l) for i in range(K+1) for j in range(i+1)
               for k in range(j+1) for l in range(k+1)
               if j + k >= abs(i - l) and i + l >= abs(j - k) and
               (i % 2 + j % 2 + k % 2 + l % 2) % 2 == 0  # no odd number of odd polynomials
               ]
    four_tensor = np.zeros((K+1, K+1, K+1, K+1))

    quad_out_arr = np.array([quad(
        lambda u: orthopolval(u, i) * orthopolval(u, j) * \
        orthopolval(u, k) * orthopolval(u, l),
        a=0, b=1)[0] for i, j, k, l in indices])

    for (i, j, k, l), quad_out in zip(indices, quad_out_arr):
        for perm in set(permutations((i, j, k, l))):
            four_tensor[perm] = quad_out

    assert np.allclose(four_tensor, np.swapaxes(four_tensor, 0, 1))
    assert np.allclose(four_tensor, np.swapaxes(four_tensor, 0, 2))
    assert np.allclose(four_tensor, np.swapaxes(four_tensor, 0, 3))

    return four_tensor


def make_four_tensor_from_three(
        orthopolval=None, K=None,
        norms_sq=None, three_tensor=None,
        wsum=2, a=-1, b=1):
    """
    Much slower if three_tensor is not pre-calculated!
    """

    D = 2*K  # Need up to this dimension...

    if norms_sq is None:
        norms_sq = np.append(1, np.array([quad(lambda u: orthopolval(
            u, i+1)**2, a, b)[0] / wsum for i in range(1, D+1)]))

    if three_tensor is None:
        three_tensor = make_three_tensor(orthopolval, D, wsum, a, b)

    assert three_tensor.shape[0] == norms_sq.size

    four_tensor = contract(
        'nij,nkl->ijkl', three_tensor / norms_sq[:, np.newaxis, np.newaxis], three_tensor)

    assert np.allclose(four_tensor, np.swapaxes(four_tensor, 0, 1))
    assert np.allclose(four_tensor, np.swapaxes(four_tensor, 0, 2))
    assert np.allclose(four_tensor, np.swapaxes(four_tensor, 0, 3))

    return four_tensor[:K+1, :K+1, :K+1, :K+1]


def gpc_skewness(fcs, three_tensor, norms_sq=None, std=None):
    assert norms_sq is not None or std is not None

    if std is None:
        std_power_minus_three = np.power(np.nansum(fcs[1:]**2 * norms_sq[1:]), -1.5)
    else:
        assert isinstance(std, (int, float))
        std_power_minus_three = np.power(std, -3)

    v = fcs[1:]
    K = fcs.size

    return std_power_minus_three * contract(
            'i, j, k, ijk', v, v, v, three_tensor[1:, 1:, 1:])


def gpc_kurtosis(fcs, four_tensor, norms_sq=None, std=None, var=None):
    assert norms_sq is not None or std is not None or var is not None

    if std is None and var is None:
        std_power_minus_four = np.power(np.nansum(fcs[1:]**2 * norms_sq[1:]), -2)
    elif std is not None:
        assert isinstance(std, (int, float))
        std_power_minus_four = np.power(std, -4)
    elif var is not None:
        assert isinstance(var, (int, float))
        std_power_minus_four = np.power(var, -2)

    v = fcs[1:]
    K = fcs.size

    return std_power_minus_four * contract(
            'i, j, k, l, ijkl', v, v, v, v, four_tensor[1:, 1:, 1:, 1:])


if __name__ == "__main__":
    T1 = make_four_tensor_legendre_old(5)
    T2 = make_four_tensor_legendre(5)

    assert np.allclose(T1, T2)

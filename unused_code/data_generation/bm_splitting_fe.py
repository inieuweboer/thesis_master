from bm_splitting import *

if __name__ == "__main__":
    import datetime
    print('Datetime: ', datetime.datetime.now())

    fp = open("./out/bm_splitting_fe", 'a')

    np.random.seed(42)
    np.seterr(divide='ignore')

    params = (0, 1)

    nruns = int(1e5)

    L = 5
    T = 30

    print('nruns = ', '{:,}'.format(nruns).replace(',', ' '))
    print('L = ', L)
    print('T = ', T)
    print()

    optimize = False
    show = False

    for _ in range(nruns):
        print(bm_splitting_fixed_effort(params=params, L=L, T=T,
            optimize=optimize, show=show), file=fp)

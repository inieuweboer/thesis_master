#!/bin/bash
#SBATCH --job-name=BMSP5
#SBATCH --output=%x-%j.out

#SBATCH -N 1
#SBATCH --tasks-per-node 9
#SBATCH --time=48:00:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: BMSP5"

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

case $HOSTNAME in
  (antergos-sopropo)
    pyth="python";
    work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl)
    echo "should not run on ${HOSTNAME} without sbatch, exit";
    exit 1;;
  (*)
    # py3 and latest versions of numpy, scipy
    # pyth="$HOME/.virtualenvs/math/bin/python";
    pyth="python3";
    work_dir="$HOME/thesis_rainfall";;
esac

# params="--L 5 --CMC 10000 100000 1000000 --FE 100 1000 10000 --FNS 100 1000 10000 --FO 100 2 100 3 100 5"
params="--L 5 --FO 100 5"

time "${pyth}" "${work_dir}/bm_splitting_runall.py"   --datetime "$dt" $params && \
time "${pyth}" "${work_dir}/bm_splitting_validate.py" --datetime "$dt" $params

echo end of job

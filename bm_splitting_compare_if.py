import multiprocessing
import os
import shutil
import time

from bm_splitting import *
from helper_functions import (
    bm_splitting_argparser, args_str_maker)


def worker(work_args, times_dict):
    outfile, nsims, alg_dict, method, L, T, alg_meth_params, args_key = work_args

    with open(outfile, 'a+') as fp_meth:
        t = time.process_time()

        for _ in range(nsims):
            print(alg_dict[method](L=L, T=T, alg_params=alg_meth_params),
                  file=fp_meth)

        elapsed = time.process_time() - t

    print('Imp fn:')
    print(alg_meth_params[3])
    print('nsims:')
    print('{:,}'.format(nsims).replace(',', ' '))
    print('Process time:')
    print(elapsed)

    times_dict[args_key] = elapsed


def main():
    # Prepare multiprocessing
    manager = multiprocessing.Manager()
    times_dict = manager.dict()
    jobs = []

    # Get args
    args = bm_splitting_argparser()
    datetimestr, nsims, nlevels, L, T = \
        args.datetime, args.nsims, args.nlevels, args.L, args.T


    # c = 0.6275; gamma = exact_prob_bm(L, T); int(np.round(c * np.abs(np.log(gamma))))
    stepsize = 0.01

    print('stepsize = ', '{:,}'.format(stepsize).replace(',', ' '))
    print('nlevels = ', '{:,}'.format(nlevels).replace(',', ' '))
    print()
    print('nsims = ', '{:,}'.format(nsims).replace(',', ' '))
    print('L = ', L)
    print('T = ', T)
    print()

    #  optimize = True
    alg_dict = {
                'FE': bm_splitting_fixed_effort,
                'FNS': bm_splitting_fixed_successes,
                'FO': bm_splitting_fixed_offspring,
                }
    imp_fn_dict = {
                   'dist': make_importance_fn_distance(T, L),
                   'BM': make_importance_fn_brownian_motion(T, L),
                   'LD': make_importance_fn_large_deviation(T, L),
                   }

    # Prepare filenames
    outfolder = 'out/bm_if/'
    tofolder = 'out/bm_if/'
    if args.test:
        outfolder = outfolder + 'test/'
        tofolder = tofolder + 'test/'
    if not args.local:
        outfolder = '{}/multi_splitting_inb_{}/{}'.format(os.getenv("TMPDIR"), datetimestr, outfolder)
        tofolder = '/home/ismani/{}'.format(tofolder)

    os.makedirs(outfolder, exist_ok=True)
    os.makedirs(tofolder, exist_ok=True)

    name = '_bm_sp_'
    filename_out_pre = "".join([outfolder, datetimestr, name])
    filename_to_pre = "".join([tofolder, datetimestr, name])

    # Times printed to local file to parse them
    filename_times = "".join([tofolder, datetimestr, name + "times"])

    method = 'FNS'
    with open(filename_times, 'a+') as fp_time:
        for imp_fn in ['dist', 'LD', 'BM']:
            for method_params in getattr(args, method):
                if method_params[0] < 0:
                    print('Skipping ', method)
                    break

                args_key = args_str_maker(nsims, L, T, nlevels, imp_fn, method_params)
                outfile = filename_out_pre + args_key
                work_args = outfile, nsims, alg_dict, method, L, T, \
                    (stepsize, nlevels, *method_params, imp_fn_dict[imp_fn]), \
                    args_key

                p = multiprocessing.Process(
                        target=worker, args=(work_args, times_dict))
                p.name = args_key
                jobs.append(p)
                p.start()

        for p in jobs:
            args_key = p.name
            outfile = filename_out_pre + args_key
            tofile = filename_to_pre + args_key

            p.join()

            # Move from scratch to local
            shutil.move(outfile, tofile)

            # Save partial time results
            print(times_dict, file=fp_time)
            print(args_key, 'joined!')
            print()


if __name__ == "__main__":
    np.random.seed()
    np.seterr(divide='ignore')

    main()

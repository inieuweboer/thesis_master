import matplotlib.pyplot as plt
import numpy as np

from scipy.special import expit, logit

from bm_splitting import Phi
from helper_functions import (
    make_affine_transf,
    calc_three_char_dists,
    calc_three_char_dists_re,
    qq_plotter)
from UQ_methods import *

from tabulate import tabulate
from text_render_latex import *
latexify(columns=2)


###

# Use axis convention L, T, A/k/m,
# i.e. level, end time, gPC deg / amt of drift nodes

###


def exact_prob_bm_rm(L, T, drift):
    r"""
    $\xprob{ M_T^{(\alpha)} > L}$ where
    $M_T^{(\\alpha)} := \sup_{0 \le t \le T} X_t$ and
    $X_t := W_t + \alpha t$

    https://math.stackexchange.com/questions/1053294/density-of-first-hitting-time-of-brownian-motion-with-drift
    """
    return Phi((-L + drift*T) / np.sqrt(T)) + \
        np.exp(2*L*drift) * Phi((-L - drift*T) / np.sqrt(T))


def gpc_bm_rm_prob(
        T=30, L=26,
        K=10, M=11,
        #  a=-1, b=1,
        a=0, b=2,
        sp_mult=50,
        ):

    # Params
    #  L = 0.1  # No Gibbs phenomenon
    #  L = 5
    #  L = 18  # 0.001 for zero drift
    #  L = 26  # 2e-6 for zero drift

    assert a < b, 'a not smaller than b'

    model_fn = lambda alpha: exact_prob_bm_rm(L, T, alpha)

    #####
    gpc_approx_dp_fn, _, _ = gpc_approx_discrete_proj_uniform(
        model_fn,
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )

    drift_space = np.linspace(a, b, int(np.ceil(b - a))*sp_mult + 1)
    true_model_drift_space = model_fn(drift_space)

    gpc_approx_drift_space = gpc_approx_dp_fn(drift_space)

    # Plot true and approx using both naive gPC and func gPC approx
    fig1, ax1 = plt.subplots()
    fig1.set_figheight(fig1.get_figheight() * figfactor)
    fig1.set_figwidth(fig1.get_figwidth() * figfactor)

    figname = 'bm_drift_rm'

    ax1.plot(drift_space, true_model_drift_space, 'k--', label='True')
    ax1.plot(drift_space, gpc_approx_drift_space, 'r',
             linewidth=.5, label='gPC')

    xlabel = '$\\alpha$'
    ylabel = '$\\gamma_{{\\scriptscriptstyle T, L}}' \
        '(\\alpha); T = {}, L = {}$'.format(T, L)
    set_axis_labels_endaxes(xlabel, ylabel, ax1)

    ax1.legend()
    ax1.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    ax1.axhline(1, linestyle=loosely_dotted, color='k', linewidth=.25)

    fig1.savefig(
        './figs/fit_alpha_gamma_gpc_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf', transparent=True,
        )


def gpc_bm_rm_prob_more(
        model_fn=None,
        T=30, L=26,
        K=10, M=11,
        #  a=-1, b=1,
        a=0, b=2,
        sp_mult=50,
        biases=False,
        ):

    assert a < b, 'a not smaller than b'

    if model_fn is None:
        model_fn = lambda alpha: exact_prob_bm_rm(L, T, alpha)

    #####
    gpc_approx_dp_fn_L, fcs_dp_L, norms_sq_L = gpc_approx_discrete_proj_uniform(
        model_fn,
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )

    log_gpc_approx_dp_fn_L, _, _ = gpc_approx_discrete_proj_uniform(
        lambda alpha: np.log(exact_prob_bm_rm(L, T, alpha)),
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )

    logit_gpc_approx_dp_fn_L, _, _ = gpc_approx_discrete_proj_uniform(
        lambda alpha: logit(exact_prob_bm_rm(L, T, alpha)),
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )

    drift_space = np.linspace(a, b, int(np.ceil(b - a))*sp_mult + 1)
    true_model_drift_space = model_fn(drift_space)

    gpc_approx_drift_space = gpc_approx_dp_fn_L(drift_space)

    log_gpc_approx_drift_space = log_gpc_approx_dp_fn_L(drift_space)
    mapped_log_gpc_approx_drift_space = np.exp(log_gpc_approx_drift_space)

    logit_gpc_approx_drift_space = logit_gpc_approx_dp_fn_L(drift_space)
    mapped_logit_gpc_approx_drift_space = expit(logit_gpc_approx_drift_space)

    # Plot true and approx using both naive gPC and func gPC approx
    fig1, ax1 = plt.subplots()
    fig1.set_figheight(fig1.get_figheight() * figfactor)
    fig1.set_figwidth(fig1.get_figwidth() * figfactor)

    figname = 'compare'

    ax1.plot(drift_space, true_model_drift_space, 'k--', label='True')
    ax1.plot(drift_space, gpc_approx_drift_space, 'r',
             linewidth=.5, label='gPC')
    ax1.plot(drift_space, mapped_log_gpc_approx_drift_space, 'g',
             linewidth=.5, label='exp of log-gPC')
    ax1.plot(drift_space, mapped_logit_gpc_approx_drift_space, 'b',
             linewidth=.5, label='expit of logit-gPC')

    xlabel = '$\\alpha$'
    ylabel = '$\\gamma_{{\\scriptscriptstyle T, L}}' \
        '(\\alpha); T = {}, L = {}$'.format(T, L)
    set_axis_labels_endaxes(xlabel, ylabel, ax1)

    ax1.legend()
    ax1.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    ax1.axhline(1, linestyle=loosely_dotted, color='k', linewidth=.25)

    fig1.savefig(
        './figs/fit_alpha_gamma_gpc_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf', transparent=True,
        )

    fig_func, axes_func = plt.subplots(nrows=1, ncols=2)
    fig_func.set_figheight(fig_func.get_figheight() / 2)

    for i, (func, gpc_space, funcname) in enumerate([
        (np.log, log_gpc_approx_drift_space, 'log'),
        (logit, logit_gpc_approx_drift_space, 'logit')
        ]):

        axes_func[i].plot(drift_space, func(true_model_drift_space),
                    'k--', label='True')
        axes_func[i].plot(drift_space, gpc_space,
                    'r', linewidth=.5, label=funcname + '-gPC')

        axes_func[i].legend()

        xlabel = '$\\alpha$'
        ylabel = '$\\mathrm{{{}}}(\\gamma_{{\\scriptscriptstyle T, L}}' \
            '(\\alpha))$'.format(funcname)
        set_axis_labels_endaxes(xlabel, ylabel, axes_func[i])

    figname = 'log_logit'

    fig_func.savefig(
        './figs/fit_alpha_gamma_gpc_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf', transparent=True,
        )

    if not biases:
        return

    #####
    # Print errors between expansion of (function of) and
    # true distribution function of prob BM with drift greater than L
    first_row = [['Method \\textbackslash{{}} Error',
                    'Max. abs.',
                    'Mean abs.',
                    'Mean square',
                    ]]
    table_arr = []
    table_arr.append(list(('gPC',) +
        calc_three_char_dists(true_model_drift_space,
                                gpc_approx_drift_space,
                                )))
    table_arr.append(list(('exp of log-gPC',) +
        calc_three_char_dists(true_model_drift_space,
                                mapped_log_gpc_approx_drift_space,
                                )))
    table_arr.append(list(('expit of logit-gPC',) +
        calc_three_char_dists(true_model_drift_space,
                                mapped_logit_gpc_approx_drift_space,
                                )))
    print(tabulate(first_row, tablefmt="latex_raw"))
    print(tabulate(table_arr, tablefmt="latex_raw", floatfmt=".4g"))

    figname = 'biases'
    fig_bias, ax_bias = plt.subplots()
    fig_bias.set_figheight(fig_bias.get_figheight() * figfactor)
    fig_bias.set_figwidth(fig_bias.get_figwidth() * figfactor)

    ax_bias.plot(drift_space,
                 true_model_drift_space - gpc_approx_drift_space,
                 'r--', linewidth=.5, label='gPC')
    ax_bias.plot(drift_space,
                 true_model_drift_space - mapped_log_gpc_approx_drift_space,
                 'g--', linewidth=.5, label='log-gPC')
    ax_bias.plot(drift_space,
                 true_model_drift_space - mapped_logit_gpc_approx_drift_space,
                 'b--', linewidth=.5, label='logit-gPC')

    xlabel = '$\\alpha$'
    ylabel = 'Approximation error'
    set_axis_labels_endaxes(xlabel, ylabel, ax_bias)

    ax_bias.legend()
    ax_bias.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)

    fig_bias.savefig(
        './figs/fit_alpha_gamma_gpc_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf', transparent=True,
        )


def gpc_bm_rm_prob_more_oneplot(
        model_fn=None,
        T=30, L=26,
        K=10, M=11,
        #  a=-1, b=1,
        a=0, b=2,
        sp_mult=50,
        print_errors=False,
        ):

    assert a < b, 'a not smaller than b'

    if model_fn is None:
        model_fn = lambda alpha: exact_prob_bm_rm(L, T, alpha)

    #####
    gpc_approx_dp_fn_L, fcs_dp_L, norms_sq_L = gpc_approx_discrete_proj_uniform(
        model_fn,
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )

    log_gpc_approx_dp_fn_L, _, _ = gpc_approx_discrete_proj_uniform(
        lambda alpha: np.log(exact_prob_bm_rm(L, T, alpha)),
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )

    logit_gpc_approx_dp_fn_L, _, _ = gpc_approx_discrete_proj_uniform(
        lambda alpha: logit(exact_prob_bm_rm(L, T, alpha)),
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )

    drift_space = np.linspace(a, b, int(np.ceil(b - a))*sp_mult + 1)
    true_model_drift_space = model_fn(drift_space)

    gpc_approx_drift_space = gpc_approx_dp_fn_L(drift_space)

    log_gpc_approx_drift_space = log_gpc_approx_dp_fn_L(drift_space)
    mapped_log_gpc_approx_drift_space = np.exp(log_gpc_approx_drift_space)

    logit_gpc_approx_drift_space = logit_gpc_approx_dp_fn_L(drift_space)
    mapped_logit_gpc_approx_drift_space = expit(logit_gpc_approx_drift_space)

    # Plot true and approx using both naive gPC and func gPC approx
    fig, axes = plt.subplots(nrows=1, ncols=2)
    fig.set_figheight(fig.get_figheight() / 2)

    figname = 'compare_bias'

    axes[0].plot(drift_space, true_model_drift_space, 'k--', label='True')
    axes[0].plot(drift_space, gpc_approx_drift_space, 'r',
             linewidth=.5, label='gPC')
    axes[0].plot(drift_space, mapped_log_gpc_approx_drift_space, 'g',
             linewidth=.5, label='exp of log-gPC')
    axes[0].plot(drift_space, mapped_logit_gpc_approx_drift_space, 'b',
             linewidth=.5, label='expit of logit-gPC')

    xlabel = '$\\alpha$'
    ylabel = '$\\gamma_{{\\scriptscriptstyle T, L}}' \
        '(\\alpha); T = {}, L = {}$'.format(T, L)
    set_axis_labels_endaxes(xlabel, ylabel, axes[0])

    axes[0].legend()
    axes[0].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    axes[0].axhline(1, linestyle=loosely_dotted, color='k', linewidth=.25)

    axes[1].plot(drift_space,
                 true_model_drift_space - gpc_approx_drift_space,
                 'r--', linewidth=.5, label='gPC')
    axes[1].plot(drift_space,
                 true_model_drift_space - mapped_log_gpc_approx_drift_space,
                 'g--', linewidth=.5, label='log-gPC')
    axes[1].plot(drift_space,
                 true_model_drift_space - mapped_logit_gpc_approx_drift_space,
                 'b--', linewidth=.5, label='logit-gPC')

    xlabel = '$\\alpha$'
    ylabel = 'Approximation error'
    set_axis_labels_endaxes(xlabel, ylabel, axes[1])

    axes[1].legend()
    axes[1].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)

    fig.savefig(
        './figs/fit_alpha_gamma_gpc_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf', transparent=True,
        )

    if print_errors:
        #####
        # Print errors between expansion of (function of) and
        # true distribution function of prob BM with drift greater than L
        first_row = [['Method \\textbackslash{{}} Error',
                        'Max. abs.',
                        'Mean abs.',
                        'Mean square',
                        ]]
        table_arr = []
        table_arr.append(list(('gPC',) +
            calc_three_char_dists(true_model_drift_space,
                                    gpc_approx_drift_space,
                                    )))
        table_arr.append(list(('exp of log-gPC',) +
            calc_three_char_dists(true_model_drift_space,
                                    mapped_log_gpc_approx_drift_space,
                                    )))
        table_arr.append(list(('expit of logit-gPC',) +
            calc_three_char_dists(true_model_drift_space,
                                    mapped_logit_gpc_approx_drift_space,
                                    )))
        print(tabulate(first_row, tablefmt="latex_raw"))
        print(tabulate(table_arr, tablefmt="latex_raw", floatfmt=".4g"))


def gpc_bm_rm_prob_plot_gamma_L(
        T=30, L=75,
        K=10, M=11,
        #  a=-1, b=1,
        a=0, b=2,
        sp_mult_L=4,
        biases=False,
        ):
    """
    Plot gPC and CMC mean/std against L
    """

    rv = '\\gamma_{{\\scriptscriptstyle T, L}}(\\alpha)'

    # Watch out with indexing against L!
    Lspace = np.linspace(0, L, int(np.ceil(L*sp_mult_L + 1)))
    Targ = -1

    assert a < b, 'a not smaller than b'

    model_fn_param = exact_prob_bm_rm

    # fixme alles omgooien kost ook weer tijd...
    def model_fn_param_new(param_space, gPC_var):
        return exact_prob_bm_rm(param_space, T, gPC_var)

    #####
    #  cmc_samples = cmc_approx_process(model_fn_param, Lspace, Tspace, a, b)
    cmc_samples = cmc_approx_process(
        model_fn_param, Lspace, T, a, b,
        nsamples=np.power(10, 5))

    fcs_interp, _ = gpc_approx_interp_process(
        #  model_fn_param, Lspace, Tspace, a, b,
        model_fn_param, Lspace, T, a, b,
        gpc_deg=K,
        )
    fcs_dp, norms_sq = gpc_approx_discrete_proj_uniform_process(
        model_fn_param_new, Lspace, a, b,
        gauss_quad_deg=M,
        gpc_deg=K,
        )

    cmc_mean = np.mean(cmc_samples[:, :, Targ], axis=0)
    cmc_std = np.std(cmc_samples[:, :, Targ], axis=0, ddof=1.5)

    gpc_interp_mean = fcs_interp[:, Targ, 0]
    gpc_interp_std = np.sqrt(np.nansum(
        fcs_interp[:, Targ, 1:]**2 * norms_sq[1:], axis=1))

    gpc_dp_mean = fcs_dp[:, 0]
    gpc_dp_std = np.sqrt(np.nansum(
        fcs_dp[:, 1:]**2 * norms_sq[1:], axis=1))

    figname = 'normal_un'
    fig_L, ax_L = plt.subplots()
    fig_L.set_figheight(fig_L.get_figheight() * figfactor)
    fig_L.set_figwidth(fig_L.get_figwidth() * figfactor)

    ax_L.plot(Lspace, cmc_mean, 'k--', label='CMC mean')  # indices n, t, l
    ax_L.fill_between(Lspace,
                      cmc_mean - cmc_std,
                      cmc_mean + cmc_std,
                      #  np.maximum(0, cmc_mean - cmc_std),
                      #  np.minimum(1, cmc_mean + cmc_std),
                      alpha=0.5, label='CMC std')

    ax_L.plot(Lspace, gpc_interp_mean,
              linewidth=.5,
              label='gPC interp mean')
    ax_L.fill_between(Lspace,
                      gpc_interp_mean - gpc_interp_std,
                      gpc_interp_mean + gpc_interp_std,
                      alpha=0.5, label='gPC interp std')

    ax_L.plot(Lspace, gpc_dp_mean,
              linewidth=.5,
              label='gPC discrete projection mean')
    ax_L.fill_between(Lspace,
                      gpc_dp_mean - gpc_dp_std,
                      gpc_dp_mean + gpc_dp_std,
                      alpha=0.5, label='gPC discrete projection std')

    #  ax_L.set_ylim([0, 1])
    ax_L.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    ax_L.axhline(1, linestyle=loosely_dotted, color='k', linewidth=.25)

    xlabel = '$L$'
    ylabel = '${} \\pm {}$'.format(
    # ; T = {}; \\alpha \\sim \\mathrm{{Un}}[{}, {}]$'.format(
            xe(rv), xstd(rv),
            #  T, a, b
            )
    set_axis_labels_endaxes(xlabel, ylabel, ax_L)
    ax_L.legend(loc='upper right')  # This one manually

    fig_L.savefig(
        './figs/UQ_L_gamma_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf', transparent=True,
        )

    if not biases:  # Done
        return

    #####
    # Print errors between CMC and gPC for mean
    first_row = [['Method \\textbackslash{{}} Error',
                    'Max. abs.',
                    'Mean abs.',
                    'Mean square',
                    ]]
    table_arr = []
    table_arr.append(list(('gPC interp',) +
                        calc_three_char_dists(cmc_mean, gpc_interp_mean)))
    table_arr.append(list(('gPC dp',) +
                        calc_three_char_dists(cmc_mean, gpc_dp_mean)))
    print(tabulate(first_row, tablefmt="latex_raw"))
    print(tabulate(table_arr, tablefmt="latex_raw", floatfmt=".4g"))

    ##

    figname = 'normal_un_biases'
    fig_biases, ax_biases = plt.subplots(nrows=1, ncols=2)
    fig_biases.set_figheight(fig_biases.get_figheight() / 2)

    ax_biases[0].plot(Lspace,
                 cmc_mean - gpc_dp_mean,
                 'r--', linewidth=.5, label='Disc. proj.')
    ax_biases[0].plot(Lspace,
                 cmc_mean - gpc_interp_mean,
                 'b--', linewidth=.5, label='Interp.')

    xlabel = '$L$'
    ylabel = 'Bias est. of ${}$'.format(xe(rv))
    set_axis_labels_endaxes(xlabel, ylabel, ax_biases[0])

    ax_biases[0].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)

    ax_biases[1].plot(Lspace,
                 cmc_std - gpc_dp_std,
                 'r--', linewidth=.5, label='Disc. proj.')
    ax_biases[1].plot(Lspace,
                 cmc_std - gpc_interp_std,
                 'b--', linewidth=.5, label='Interp.')

    xlabel = '$L$'
    ylabel = 'Bias est. of ${}$'.format(xstd(rv))
    set_axis_labels_endaxes(xlabel, ylabel, ax_biases[1])

    ax_biases[1].legend()
    ax_biases[1].axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)

    fig_biases.savefig(
        './figs/UQ_L_gamma_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf', transparent=True,
        )


def gpc_bm_rm_prob_plot_gamma_L_multiple_intervals(
        T=30,
        K=10, M=11,
        #  a=-1, b=1,
        a=0, b=2,
        list_params=None,
        sp_mult_L=4,
        ):
    """
    Four plots for different uniform dists of alpha, gamma mean/std against L
    """
    figname = 'un_intervals_dp'

    model_fn = exact_prob_bm_rm
    model_fn_param = lambda L, alpha: exact_prob_bm_rm(L, T, alpha)

    if list_params is None:
        list_params = [(30, 0, 0.5), (45, -1, 1), (15, -0.1, 0.1), (20, -1, 0)]

    fig_L, axes_L = plt.subplots(nrows=2, ncols=2)

    for i, (L, a, b) in enumerate(list_params):
        # Watch out with indexing against L!
        Lspace = np.linspace(0, L, int(np.ceil(L*sp_mult_L + 1)))

        ax_L = axes_L[i % 2, int(i / 2)]

        fcs_dp, norms_sq = gpc_approx_discrete_proj_uniform_process(
            model_fn_param, Lspace,
            a, b,
            gauss_quad_deg=M,
            gpc_deg=K,
            )

        gpc_dp_mean = fcs_dp[:, 0]
        gpc_dp_std = np.sqrt(np.nansum(
            fcs_dp[:, 1:]**2 * norms_sq[1:], axis=1))

        ax_L.plot(Lspace, gpc_dp_mean, label='gPC disc. proj. mean')
        ax_L.fill_between(Lspace,
                          gpc_dp_mean - gpc_dp_std,
                          gpc_dp_mean + gpc_dp_std,
                          alpha=0.5, label='gPC disc. proj. std')

        ax_L.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
        ax_L.axhline(1, linestyle=loosely_dotted, color='k', linewidth=.25)

        ylabel = '$\\gamma_{{\\scriptscriptstyle T, L}}(\\alpha)$ ' \
            'for $\\alpha \\sim \\mathrm{{Un}}[{}, {}]$'.format(a, b)

        set_axis_labels_endaxes('$L$', ylabel, ax_L)

        if i == 3:
            ax_L.legend()

    fig_L.savefig(
        './figs/UQ_L_gamma_{}_T_{}_'
        'K_{}_M_{}.pdf'.format(figname,
                                         T, K, M),
        format='pdf', transparent=True,
        )


def gpc_bm_rm_prob_plot_gamma_L_multiple_fns(
        T=30, L=75,
        K=10, M=11,
        a=0, b=2,
        sp_mult_L=4,
        biases=False,
        show=False,
        ):
    """
    Two plots side by side, gamma mean/std against L, log- and logit-gPC
    """

    rv = '\\gamma_{{\\scriptscriptstyle T, L}}(\\alpha)'

    # Watch out with indexing against L!
    Lspace = np.linspace(0, L, int(np.ceil(L*sp_mult_L + 1)))
    Targ = -1

    assert a < b, 'a not smaller than b'

    list_fns = []

    model_fn_param = lambda L, T, alpha: np.log(exact_prob_bm_rm(L, T, alpha))
    fn = '\\log'
    list_fns.append([model_fn_param, fn])

    model_fn_param = lambda L, T, alpha: logit(exact_prob_bm_rm(L, T, alpha))
    fn = '\\mathrm{{logit}}'
    list_fns.append([model_fn_param, fn])

    if biases:
        figname = 'log_logit_biases'
        fig_L, axes_L = plt.subplots(nrows=3, ncols=2)
        fig_L.set_figheight(fig_L.get_figheight() * 1.5)
    else:
        figname = 'log_logit'
        fig_L, axes_L = plt.subplots(nrows=1, ncols=2)

    for i, (model_fn_param, fn) in enumerate(list_fns):
        if biases:
            ax_L = axes_L[0, i]
            ax_L_bias_mean = axes_L[1, i]
            ax_L_bias_std = axes_L[2, i]
        else:
            ax_L = axes_L[i]

        fcs_interp, _ = gpc_approx_interp_process(
            model_fn_param,
            Lspace, T, a, b,
            gpc_deg=K
            )
        fcs_dp, norms_sq = gpc_approx_discrete_proj_process(
            model_fn_param,
            Lspace, T, a, b,
            gauss_quad_deg=M,
            gpc_deg=K
            )

        cmc_samples = cmc_approx_process(model_fn_param, Lspace, T, a, b,
                                              nsamples=np.power(10, 5))
        cmc_mean = np.mean(cmc_samples[:, :, Targ], axis=0)
        cmc_std = np.std(cmc_samples[:, :, Targ], axis=0, ddof=1.5)

        gpc_interp_mean = fcs_interp[:, Targ, 0]
        gpc_interp_std = np.sqrt(np.nansum(
            fcs_interp[:, Targ, 1:]**2 * norms_sq[1:], axis=1))

        gpc_dp_mean = fcs_dp[:, Targ, 0]
        gpc_dp_std = np.sqrt(np.nansum(
            fcs_dp[:, Targ, 1:]**2 * norms_sq[1:], axis=1))

        ax_L.plot(Lspace, cmc_mean, 'k--',
                  label='CMC mean')  # indices n, t, l
        ax_L.fill_between(Lspace,
                          cmc_mean - cmc_std,
                          cmc_mean + cmc_std,
                          #  np.maximum(0, cmc_mean - cmc_std),
                          #  np.minimum(1, cmc_mean + cmc_std),
                          alpha=0.5, label='CMC std')

        ax_L.plot(Lspace, gpc_interp_mean,
                  linewidth=.5,
                  label='gPC interp mean')
        ax_L.fill_between(Lspace,
                          gpc_interp_mean - gpc_interp_std,
                          gpc_interp_mean + gpc_interp_std,
                          alpha=0.5, label='gPC interp std')

        ax_L.plot(Lspace, gpc_dp_mean,
                  linewidth=.5,
                  label='gPC disc. proj. mean')
        ax_L.fill_between(Lspace,
                          gpc_dp_mean - gpc_dp_std,
                          gpc_dp_mean + gpc_dp_std,
                          alpha=0.5, label='gPC disc. proj. std')

        if fn:
            ylabel = '${}({})$'.format(fn, rv)
        else:
            ylabel = '${}$'.format(rv)

        set_axis_labels_endaxes('$L$', ylabel, ax_L)

        if i == 0:
            ax_L.legend(frameon=False)

        if not biases:
            continue

        ##### biases
        #  ax_L_bias_mean.plot(Lspace,
        #              cmc_mean - gpc_interp_mean,
        #              'b--', linewidth=.5, label='Interp.')
        ax_L_bias_mean.plot(Lspace,
                    cmc_mean - gpc_dp_mean,
                    'r--', linewidth=.5, label='Disc. proj.')

        xlabel = '$L$'
        ylabel = 'Bias est. of ${}$'.format(xe('{}({})'.format(fn, rv)))
        set_axis_labels_endaxes(xlabel, ylabel, ax_L_bias_mean)

        ax_L_bias_mean.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)

        #  ax_L_bias_std.plot(Lspace,
        #              cmc_std - gpc_interp_std,
        #              'b--', linewidth=.5, label='Interp.')
        ax_L_bias_std.plot(Lspace,
                    cmc_std - gpc_dp_std,
                    'r--', linewidth=.5, label='Disc. proj.')

        xlabel = '$L$'
        ylabel = 'Bias est. of ${}$'.format(xstd('{}({})'.format(fn, rv)))
        set_axis_labels_endaxes(xlabel, ylabel, ax_L_bias_std)

        ax_L_bias_std.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)

        if i == 0:
            ax_L_bias_std.legend(frameon=False)

    if show:
        plt.show()
    else:
        fig_L.savefig(
            './figs/UQ_L_gamma_{}_T_{}_L_{}_'
            'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                            T, L, K, M, a, b),
            format='pdf', transparent=True,
            )


def gpc_bm_rm_prob_plot_gamma_L_compare(
        T=30, L=75,
        K=10, M=11,
        a=0, b=2,
        sp_mult_L=4,
        ):
    """Plot gPC and CMC mean/std against L; normal/log/logit"""

    rv = '\\gamma_{{\\scriptscriptstyle T, L}}(\\alpha)'

    # Watch out with indexing against L!
    Lspace = np.linspace(0, L, int(np.ceil(L*sp_mult_L + 1)))
    Targ = -1

    assert a < b, 'a not smaller than b'

    #  figname = "normal_log"
    figname = "normal_log_logit"

    model_fn_param = exact_prob_bm_rm

    #####
    fcs_dp, norms_sq = gpc_approx_discrete_proj_process(
        model_fn_param,
        Lspace, T, a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )
    fcs_dp_log, norms_sq_log = gpc_approx_discrete_proj_process(
        lambda L, T, alpha: np.log(exact_prob_bm_rm(L, T, alpha)),
        Lspace, T, a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )
    fcs_dp_logit, norms_sq_logit = gpc_approx_discrete_proj_process(
        lambda L, T, alpha: logit(exact_prob_bm_rm(L, T, alpha)),
        Lspace, T, a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )

    #  cmc_samples = cmc_approx_process(model_fn_param, Lspace, T, a, b,
    #                                        nsamples=np.power(10, 5))
    #  cmc_mean = np.mean(cmc_samples[:, :, Targ], axis=0)
    #  cmc_std = np.std(cmc_samples[:, :, Targ], axis=0, ddof=1.5)

    gpc_dp_mean = fcs_dp[:, Targ, 0]
    gpc_dp_std = np.sqrt(np.nansum(fcs_dp[:, Targ, 1:]**2 * norms_sq[1:], axis=1))

    gpc_log_dp_mean = fcs_dp_log[:, Targ, 0]
    gpc_log_dp_std = np.sqrt(np.nansum(
        fcs_dp_log[:, Targ, 1:]**2 * norms_sq_log[1:], axis=1))

    gpc_logit_dp_mean = fcs_dp_logit[:, Targ, 0]
    gpc_logit_dp_std = np.sqrt(np.nansum(
        fcs_dp_logit[:, Targ, 1:]**2 * norms_sq_logit[1:], axis=1))

    #  gpc_interp_mean = fcs_interp[:, Targ, 0]
    #  gpc_interp_std = np.sqrt(np.nansum(
    #      fcs_interp[:, Targ, 1:]**2 * norms_sq[1:], axis=1))

    fig_L, ax_L = plt.subplots()
    fig_L.set_figheight(fig_L.get_figheight() * figfactor)
    fig_L.set_figwidth(fig_L.get_figwidth() * figfactor)

    #  ax_L.plot(Lspace, cmc_mean, 'k--', label='CMC mean')  # indices n, t, l
    #  ax_L.fill_between(Lspace,
    #                    cmc_mean - cmc_std,
    #                    cmc_mean + cmc_std,
    #                    #  np.maximum(0, cmc_mean - cmc_std),
    #                    #  np.minimum(1, cmc_mean + cmc_std),
    #                    alpha=0.5, label='CMC std')

    #  ax_L.plot(Lspace, gpc_interp_mean, label='gPC interpolation mean')
    #  ax_L.fill_between(Lspace,
    #                    gpc_interp_mean - gpc_interp_std,
    #                    gpc_interp_mean + gpc_interp_std,
    #                    alpha=0.5, label='gPC std')

    ax_L.plot(Lspace, gpc_dp_mean, label='gPC disc. proj. mean')
    ax_L.fill_between(Lspace,
                      gpc_dp_mean - gpc_dp_std,
                      gpc_dp_mean + gpc_dp_std,
                      alpha=0.5, label='gPC disc. proj. std')

    ax_L.plot(Lspace, np.exp(gpc_log_dp_mean), label='exp of log-gPC disc. proj. mean')
    ax_L.fill_between(Lspace,
                      np.exp(gpc_log_dp_mean - gpc_log_dp_std),
                      np.exp(gpc_log_dp_mean + gpc_log_dp_std),
                      alpha=0.5, label='exp of log-gPC disc. proj. std')

    ax_L.plot(Lspace, expit(gpc_logit_dp_mean),
              label='expit of logit-gPC mean')
    ax_L.fill_between(Lspace,
                      expit(gpc_logit_dp_mean - gpc_log_dp_std),
                      expit(gpc_logit_dp_mean + gpc_log_dp_std),
                      alpha=0.5, label='expit of logit-gPC std')

    ax_L.axhline(0, linestyle=loosely_dotted, color='k', linewidth=.25)
    ax_L.axhline(1, linestyle=loosely_dotted, color='k', linewidth=.25)

    xlabel = '$L$'
    ylabel = '${}$'.format(
    #  ylabel = '${} \\pm {}$'.format(
            #  xe(rv), xstd(rv),
            rv,
            )
    set_axis_labels_endaxes(xlabel, ylabel, ax_L)

    ax_L.legend()
    fig_L.savefig(
        './figs/UQ_L_gamma_{}_T_{}_L_{}_'
        'K_{}_M_{}_a_{}_b_{}.pdf'.format(figname,
                                         T, L, K, M, a, b),
        format='pdf', transparent=True,
        )


def gpc_bm_rm_prob_calc_stats(
        T=30, L=26,
        K=10, M=11,
        #  a=-1, b=1,
        a=0, b=2,
        sp_mult=50,
        ):

    # Params
    #  L = 0.1  # No Gibbs phenomenon
    #  L = 5
    #  L = 18  # 0.001 for zero drift
    #  L = 26  # 2e-6 for zero drift

    assert a < b, 'a not smaller than b'

    model_fn = lambda alpha: exact_prob_bm_rm(L, T, alpha)

    #####
    _, fcs, norms_sq = gpc_approx_discrete_proj_uniform(
        model_fn,
        a, b,
        gauss_quad_deg=M,
        gpc_deg=K
        )

    gpc_mean = fcs[0]
    gpc_var = np.nansum(fcs[1:]**2 * norms_sq[1:])
    gpc_std = np.sqrt(gpc_var)

    three_tensor_legendre = make_three_tensor_legendre(K)
    gpc_skew = gpc_skewness(fcs=fcs, three_tensor=three_tensor_legendre, std=gpc_std)

    four_tensor_legendre = make_four_tensor_legendre(K=K)
    gpc_kurt = gpc_kurtosis(fcs=fcs, four_tensor=four_tensor_legendre, std=gpc_std)


    print('Mean:', gpc_mean)
    print('std:', gpc_std)
    print('Variance:', gpc_var)
    print('Skewness:', gpc_skew)
    print('Kurtosis:', gpc_kurt)
    print()


if __name__ == "__main__":
    gpc_bm_rm_prob()
    gpc_bm_rm_prob_more(biases=True)
    #  gpc_bm_rm_prob_more_oneplot()

    #  gpc_bm_rm_prob_plot_gamma_L()
    #  gpc_bm_rm_prob_plot_gamma_L(biases=True)
    #  for L, a, b in [(75, 0, 2), (30, 0, 0.5), (45, -1, 1), (30, -0.1, 0.1)]:
    #      gpc_bm_rm_prob_plot_gamma_L(T=30, L=L, a=a, b=b, sp_mult_L=20)

    #  gpc_bm_rm_prob_plot_gamma_L_multiple_intervals()

    #  gpc_bm_rm_prob_plot_gamma_L_multiple_fns()
    #  gpc_bm_rm_prob_plot_gamma_L_multiple_fns(L=17, biases=True)
    #  gpc_bm_rm_prob_plot_gamma_L_compare()
    #  gpc_bm_rm_prob_calc_stats()

import numpy as np

import argparse, datetime

parser = argparse.ArgumentParser()
parser.add_argument('--datetime', type=str, default=datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
parser.add_argument('--L', type=int, default=5)
parser.add_argument('--T', type=int, default=30)
#  parser.add_argument('--CMC', nargs='+', type=int, default=(np.power(10, 4), np.power(10, 5), np.power(10, 6)))
#  parser.add_argument('--FE', nargs='+', type=int, default=(np.power(10, 2), np.power(10, 3), np.power(10, 4)))
#  parser.add_argument('--FNS', nargs='+', type=int, default=(np.power(10, 2), np.power(10, 3), np.power(10, 4)))
#  parser.add_argument('--FO', nargs='+', type=int, default=(100, 5, 100, 3, 100, 2))
parser.add_argument('--CMC', nargs='?', type=int, default=(np.power(10, 4), np.power(10, 5), np.power(10, 6)), const=(-1, ))
parser.add_argument('--FE',  nargs='?', type=int, default=(np.power(10, 2), np.power(10, 3), np.power(10, 4)), const=(-1, ))
parser.add_argument('--FNS', nargs='?', type=int, default=(np.power(10, 2), np.power(10, 3), np.power(10, 4)), const=(-1, ))
parser.add_argument('--FO',  nargs='?', type=int, default=(100, 5, 100, 3, 100, 2), const=(-1, -1))
parser.add_argument('--nsims', type=int, default=100)
parser.add_argument('--test', action='store_true')
parser.add_argument('--local', action='store_true')

args = parser.parse_args()
assert len(args.FO) % 2 == 0, 'Need even amount of arguments for fixed amount of offspring'

args.CMC = [(el, ) for el in args.CMC]
args.FE = [(el, ) for el in args.FE]
args.FNS = [(el, ) for el in args.FNS]
args.FO = list(map(tuple, (args.FO[2*i: 2*i+2] for i in range(len(args.FO)//2))))


print(args)

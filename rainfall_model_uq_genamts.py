import os
import shutil

from helper_functions import (
    args_str_maker,
    rf_argparser,
    rf_params_tostring,
    rfparams_alpha, rfparams_ucov,
    )

from rainfall_model import *
from UQ_methods import gpc_approx_discrete_proj_uniform_tree_nodes


def main():
    # Get args
    args = rf_argparser()
    (datetimestr, nsims, T, nlevels, rfparams_def,
     gauss_quad_deg,
     node_index,
     a, b,
     outfolder,
     ) = (args.datetime, args.nsims,
          args.T, args.nlevels, args.rfparams,
          args.gauss_quad_deg,
          args.node_index,
          args.a, args.b,
          args.outfolder,
          )
    assert node_index < gauss_quad_deg

    work_nodes = gpc_approx_discrete_proj_uniform_tree_nodes(
        a=a, b=b,
        gauss_quad_deg=gauss_quad_deg,  # M
        )
    rfparams = rfparams_ucov(rfparams_def, work_nodes, node_index)

    # Prepare filenames
    tofolder = outfolder
    if args.test:
        outfolder = outfolder + 'test/'
        tofolder = tofolder + 'test/'

    if not args.local:
        outfolder = '{}/{}'.format(os.getenv("TMPDIR"), outfolder)
        tofolder = '{}/{}'.format(os.getenv("HOME"), tofolder)
    else:
        outfolder = './' + outfolder

    os.makedirs(outfolder, exist_ok=True)
    os.makedirs(tofolder, exist_ok=True)

    args_key = args_str_maker(
        T=T,
        rfparamshash=rf_params_tostring(rfparams),
        )
    outfile = '{}{}_{}'.format(outfolder, datetimestr, args_key)
    tofile = '{}{}_{}'.format(tofolder, datetimestr, args_key)

    with open(outfile, 'a+') as fp:
        cumulative_rainfall_gen_outcomes(rfparams, ndays=T, c=np.ones(nstations),
                                         nruns=nsims,
                                         fp=fp)

    if not args.local:
        # This overwrites any currently existing file! Should be ok with timestamps
        shutil.copy(outfile, tofile)

        # Doesn't work with parallel generation
        #  with open(outfile, 'rb') as fpout, open(tofile, 'ab') as fpto:
        #      shutil.copyfileobj(fpout, fpto)


if __name__ == "__main__":
    np.random.seed()
    main()

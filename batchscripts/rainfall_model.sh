#!/bin/bash
#SBATCH --job-name=RFfnsLast
#SBATCH --output=%x-%j.out
#SBATCH -N 1
#SBATCH --tasks-per-node 2
#SBATCH --time=48:00:00
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=ismani.nieuweboer@student.uva.nl
echo "start of job: RFfnsLast"

dt=$(date '+%Y%m%d_%H%M%S');
echo "$dt"

# mkdir -p "$TMPDIR"/multi_splitting/out/rf
# mkdir -p "$TMPDIR"/multi_splitting_inb_"$dt"/out/rf

case $HOSTNAME in
  (antergos-sopropo) work_dir="./thesis_rainfall";;
  (*lisa.surfsara.nl) echo "should not run on ${HOSTNAME} without sbatch, exit"; exit 1;;
  (*) module load 2019; module unload Python; module load Python/3.6.6-intel-2018b; work_dir="$HOME/thesis_rainfall";;
esac
echo `which python3`

# params="--CMC 100 1000 10000"
# params="--FE 10 100 1000"
# params="--FNS 10 100 1000"
# params="--FO 100 2 100 3 100 5"
# params=" --CMC 100 1000 10000 --FE 10 100 1000 --FO 50 2 50 3 50 5"
# params="--FO 10 2 100 2 1000 2"
params="--FNS 10 10000"

time python3 "${work_dir}/rainfall_model_run.py" --datetime "$dt" $params

# time python3 "${work_dir}/rainfall_model_validate.py" $params  # Do this manually afterwards!

# Already do this in Python
# cp "$TMPDIR"/multi_splitting/out/rf/* "$HOME"/out/rf/ -v

echo end of job
